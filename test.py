#! /usr/bin/env python
# -*- coding: utf-8 -*- 

import httplib, urllib
headers = {"Content-type": "application/json","Accept": "application/json"}
#conn = httplib.HTTPConnection("api.driveme.dev.tfs.im")
conn = httplib.HTTPConnection("www.driveme.com")

#####################
#Url for USER Resource
####################

####### CREATE ########
#params = "{\"user\":{\"user_id\": \"XTqA8JrRowM7gZYVQSscRFKInqyRjQjQEPgZYVQSscRFKIeHdxjHedgZYVQSscSER\", \"user_type\":\"driver\", \"user_priority\":\"normal\", \"user_device\": {\"os_name\": \"Android\", \"os_version\": \"4.2.2\", \"notification_id\": \"SKHKDF2354sqd\"}}}";
#params ="{\"user\":{\"user_firstname\":\"test_prenom_client\",\"user_lastname\":\"test_nom_client\",\"user_type\":\"passenger\",\"user_photo\":\"441668_Far-Cry-3_.jpg\",\"user_gender\":\"Masculin\",\"user_country\":\"Madagascar\",\"user_id\":\"test_client\"}}"

#conn.request("POST", "/regulator/user", params, headers)

###### UPDATE #########
# params = "{\"user\":{\"user_name\":\"driveme regulator\", \"user_priority\":\"low\"}}";
# conn.request("GET", "/user/test_user_greg/EmDqkisDS5Sh3HvYLlpV9jPh4aLF2NEwRn96AgZ8rlxDjvoDxGUpfhLZw2ya1Qu", params, headers)

#params = "{\"user\":{\"user_name\":\"greg\",\"user_phone\":\"0184177334\",\"user_email\":\"greg@mail.com\",\"subscription_code\":\"123456789\",\"code_pin\":\"12345\"}}"
#conn.request("PUT", "/user/test_user_greg/TZ6TE50XJUNhah1XgbEy9A1v63lZnhKxRp0erPwOLVknjm0cMj5lxzB3pPftHNP", params, headers)
############ READ #########
# params = "";
# conn.request("GET", "/user/test_regul/quvoHm66WWP4yvGTwZDtTahSMRKcqKov9EON84i1QapTrFwBk3L7Go9UkMAxbjs", params, headers)

#######################
# AUTHENTICATE 		  
#######################
# params = "{\"user\":{\"user_id\": \"test_user_seb\", \"user_type\":\"passenger\", \"user_priority\":\"high\", \"user_device\": {\"os_name\": \"iOS\", \"os_version\": \"7.0.2\", \"notification_id\": \"test_notif_id\"}}}";
# conn.request("POST", "/authenticate", params, headers)

########## LOGOUT ###################
# params = "";
# conn.request("DELETE", "/logout/quvoHm66WWP4yvGTwZDtTahSMRKcqKov9EON84i1QapTrFwBk3L7Go9UkMAxbjs", params, headers)



########################
#Url for CREDIT Resource
########################

############ CREATE #########
#params = "{\"credit\": {\"credit_amount\": \"1200\"}}"
#conn.request("POST","/credit/u6ed0Avlkl5pqxMT4G9ur2V5WBbeCM6yK2sXhK9paSA0ZDCoRjT8qCvZkLOKfGw", params, headers);

########## READ ##########
#params = ""
#conn.request("GET","/credit/XTqA8JrRowM7gZYVQSscRFKInqyRjQjQEPgZYVQSscRFKIeHdxjHedgZYVQSscRX/u6ed0Avlkl5pqxMT4G9ur2V5WBbeCM6yK2sXhK9paSA0ZDCoRjT8qCvZkLOKfGw", params, headers);



#################################
#Url for CREDIT MOVEMENT Resource
#################################

############ CREATE #########
#params = "{\"account_mvt\": {\"account_mvt_debit\":\"500\",\"account_mvt_op_id\":\"1\",\"account_mvt_op_type\":\"Credit\"}}"
#conn.request("POST","/account_movement/u6ed0Avlkl5pqxMT4G9ur2V5WBbeCM6yK2sXhK9paSA0ZDCoRjT8qCvZkLOKfGw", params, headers);

############ READ #########
#params = ""
#conn.request("GET","/account_movement/XTqA8JrRowM7gZYVQSscRFKInqyRjQjQEPgZYVQSscRFKIeHdxjHedgZYVQSscRX/u6ed0Avlkl5pqxMT4G9ur2V5WBbeCM6yK2sXhK9paSA0ZDCoRjT8qCvZkLOKfGw", params, headers);



#####################
#Url for CAR Resource
####################

####### CREATE #########

#params = "{\"car\": {\"car_id\": \"DD99933\", \"car_description\": \"desc test\", \"car_status\": \"available\",\"car_level\": \"basic\", \"car_price\": \"5Euro/min\", \"car_position\": {\"longitude\": 10.32544, \"latitude\": 22.2154478}, \"driver\": {\"driver_firstname\": \"mahery\",\"driver_lastname\": \"antenaina\"}}}"
#params = "{\"car\":{\"car_id\":\"DF456Y\",\"car_description\":\"fgf fhgh fgsdgs\",\"car_status\":\"gsdfgsd\",\"car_level\":\"basic\",\"car_price\":\"3 Eur\/min\"}}"
#print params
#conn.request("POST", "/regulator/car/5iNX9VxNYa26tWPwUvRezELWlnNL8VuDCbEHZg4vFYr7gTmyQvtaNfVODBloc9m", params, headers)

####### UPDATE #########
#params = ""
#params = "{\"car\": {\"car_status\": \"pick-up\", \"car_position\": {\"longitude\": 10.32544, \"latitude\": 22.2154478}}}"
#conn.request("PUT", "/driver/car/CG786976/miZUasDtCACFPmJujXLnycTGa7Pkw5DdMz5yOR0wp2tHbNNH2p8FNrVHvjbX310", params, headers)

########## READ ###########

#params = ""
#conn.request("GET", "/car/AME197974CD/u6ed0Avlkl5pqxMT4G9ur2V5WBbeCM6yK2sXhK9paSA0ZDCoRjT8qCvZkLOKfGw", params, headers)

#params = ""
#conn.request("GET", "/car", params, headers)

#####################
#Url for RIDE Resource
####################

##### CREATE #########

#params =  "{\"ride\":{\"user_id\" : \"test_user_seb\", \"car_id\" : \"AG476Q\", \"start_date\": \"\", \"subscription_code\": \"\", \"start_position\": { \"longitude\": \"2.360039\", \"latitude\": \"48.858026\", \"address\": \"Rue des Francs Bourgeois 75003 Paris France\", \"zipcode\" : 75003}, \"end_position\": { \"longitude\": \"2.361502\", \"latitude\": \"48.856452\", \"address\": \"22 Rue Malher 75004 Paris France\", \"zipcode\" : 75004}}}"
#conn.request("POST", "/ride/nwxRLZP1Wk3jH1QaBnxt9avqgQrtp9lM4728dETV74U0GVU2ARfJutyFBE8gNGe", params, headers)

########## READ #########
#params = ""
#conn.request("GET", "/regulator/ride/63/FRdAo0ApstOvTjAIzrqqP7o4yILFhrabzW1Ag3luTcCSLXl5x8vs4vlm0oacmYO", params, headers)


########## UPDATE #########
# params = "{\"ride\": {\"ride_status\": \"finished\"}}"
# conn.request("PUT", "/ride/37/rS9XLn39gD46Tf01Pq7KyHbozu87YEcCtAkKo5WnTAGwhHuU18esc2T8b0szjwk", params, headers)

############### DELETE ###########
# params = ""
# conn.request("DELETE", "/ride/67/quvoHm66WWP4yvGTwZDtTahSMRKcqKov9EON84i1QapTrFwBk3L7Go9UkMAxbjs", params, headers)



#####################
#Url for COMPANY Resource
####################

###### CREATE #########
#params = "{\"company\":{" +\
#        "\"company_name\": \"test_name\"," +\
#        "\"company_address\": \"22 Rue du Debarcadere\"," +\
#        "\"company_address2\": \"\"," +\
#        "\"company_code_postal\": \"75017\"," +\
#        "\"company_ville\": \"paris\"," +\
#        "\"company_pays\": \"France\"," +\
#        "\"company_siret\": \"73282932000074\"," +\
#        "\"company_tva\": \"20\"" +\
#    "}" +\
#"}"
#conn.request("POST", "/company", params, headers)

###### READ #########

#params = ""
#conn.request("GET", "/company/1/nwxRLZP1Wk3jH1QaBnxt9avqgQrtp9lM4728dETV74U0GVU2ARfJutyFBE8gNGe", params, headers)

####### UPDATE #########

#params = "{\"company\":{" +\
#        "\"company_name\": \"company_test_name\"," +\
#        "\"company_address\": \"22 Rue du Debarcadere\"," +\
#        "\"company_address2\": \"\"," +\
#        "\"company_code_postal\": \"75017\"," +\
#        "\"company_ville\": \"paris\"," +\
#        "\"company_pays\": \"France\"," +\
#        "\"company_siret\": \"73282932000074\"," +\
#        "\"company_tva\": \"20\"" +\
#    "}" +\
#"}"
#conn.request("PUT", "/company/1/u6ed0Avlkl5pqxMT4G9ur2V5WBbeCM6yK2sXhK9paSA0ZDCoRjT8qCvZkLOKfGw", params, headers)



#####################
#Url for RESERVATION Resource
####################

###### CREATE #########

#params = "{" +\
#      "\"reservation\": {"+\
#          "\"driver_identity\": \"70646\","+\
#		  		"\"reservation_date\": \"2013-10-30\","+\
#		  		"\"reservation_time\": \"12:30:00\","+\
#          "\"start_position\": {"+\
#              "\"longitude\": \"-2.232424\","+\
#              "\"latitude\": \"48.594206\","+\
#              "\"address\": \"Allee Nicolas - Sainte Brigitte - 22380 Saint Cast le Guildo\""+\
#          "},"+\
#          "\"end_position\": {"+\
#              "\"longitude\": \"2.326375\","+\
#              "\"latitude\": \"48.834653\","+\
#              "\"address\": \"30bis, rue Gassendi - 75014 Paris\""+\
#          "}"+\
#      "}"+\
#  "}"
##print params
#conn.request("POST", "/reservation/89iVwYQhM0mvTWJZaAdfm0wo6blXFM5qtp31Ww4ViYazyApnkNhRSjaJyXib5Dz", params, headers)

# params = "{\"reservation\":{\"reservation_status\":\"riding\"}}"
# conn.request("PUT", "/reservation/2/89iVwYQhM0mvTWJZaAdfm0wo6blXFM5qtp31Ww4ViYazyApnkNhRSjaJyXib5Dz", params, headers)

####### READ ##########
#params = ""
#conn.request("GET", "/regulator/reservation/4/FRdAo0ApstOvTjAIzrqqP7o4yILFhrabzW1Ag3luTcCSLXl5x8vs4vlm0oacmYO", params, headers)


####### USER HELPER ########

#params=""
#conn.request("POST", "/regulator/helper/users/5iNX9VxNYa26tWPwUvRezELWlnNL8VuDCbEHZg4vFYr7gTmyQvtaNfVODBloc9m", params, headers)

####### RIDE HELPER ########
# params=""
# conn.request("POST", "/helper/rides/TkHtduSf1lxjwsg0Bqz6B8kdGQvf3tbGMTDoCQXV7b60NIdqg5BXpJmKIG4uEtI", params, headers)

######### CAR HELPER ############

# params = ""
# conn.request("POST", "/regulator/helper/cars/nWlcA1GGyohrjMPYIl5rvNyOaFLn4LB3dCKCpVRHBOzwhOEMYHoxxNFNpbU0Ahy", params, headers)

######### RESERVATION HELPER ############
# params = ""
# conn.request("GET", "/driver/helper/reservations/ayaGLpUisHhYpAveTxgZWARVPUWmdoPxCRdrBZkbR1iMfLB0Pyew0QrGrTlto9w", params, headers)

params = ""
conn.request("POST", "/regulator/helper/reservations/jHQKzSmXPL7uy09UTf8AZWCMCxZdF4c2uqSdxg4FljnR0lUnAVTMpZooSJOQWby", params, headers)


################ RESOURCE DRIVER  ###################
################# CREATE ###############
#params = "{\"driver\": {\"driver_firstname\":\"test_first_name\",\"driver_lastname\":\"test_driver_last_name\",\"driver_phone\":\"132213\",\"driver_email\":\"mail@asareo.fr\",\"driver_identity\": \"54321\",\"driver_photos\":\"test_photos\",\"No_VTC_Card\":\"hjghklh\",\"code_pin\": \"1234\",\"notification_id\": \"123456789\"}}"
#params = "{\"driver_firstname\":\"mahery\",\"driver_lastname\":\"Antenaina\",\"driver_photos\":\"998555_422266187874855_1428218776_n.jpg\",\"No_VTC_card\":\"4646\",\"driver_identity\":\"135436\"}"
#print params;
#conn.request("POST","/regulator/driver/XnXeivE4O2ocC5YmwUyahyJmkemNJTAfacWY27rW9lo2ylo23ADb7xyr9REwVcf",params,headers)

################# AUTHENTICATION ###############

# params = "{\"driver\":{\"driver_identity\": \"70646\", \"code_pin\":\"1234\", \"car_id\": \"CG786976\", \"notification_id\": \"123456789\"}}";
# conn.request("POST", "/driver/authenticate", params, headers)

################# DECONNECT #################
# params = "";
# conn.request("DELETE", "/driver/logout/eMkGzB3zYfhVD2Hbil49L2aOHiuhwwXJ96wijGmjx2sd8SBxgFKmz1tTNUC8qeG", params, headers)

# #################### PAYMENT ##############

#payment ride
#params = "{\"ride\":{\"ride_id\": \"37\"}, \"payment\": {\"payment_type\" : \"credit-minute\"}, \"card\" : {\"porteur\": \"1111222233334444\", \"dateval\": \"1214\", \"cvv\": \"123\", \"owner\": \"Jean Jacques Goldman\", \"title\": \"Carte PRO 1\"}}";
#conn.request("POST", "/payment/check/yzKDqTdf922IIEvEP9it1tKo8vrg4LN3B06GXlcgfjl5ymfJ6MRghKNpc2lOAfp", params, headers)

#payment credit minutes

#params = "{\"credit\": {\"credit_amount\": 30}, \"payment\": {\"amount\" : 50}, \"card\" : {\"porteur\": \"1111222233334444\", \"dateval\": \"1214\", \"cvv\": \"123\", \"owner\": \"Jean Jacques Goldman\", \"title\": \"Carte PRO 1\"}}";
#conn.request("POST", "/payment/credit/djYXQW1pPZJk5ngDAN5SEtUt2jAMbqPqfShBeNDIwHoc8JnmK5LchiDGCirzBKz", params, headers)


# params = "";
# conn.request("POST", "/driver/helper/rides/wsIsuzNYFagLQg7ykNPC7SKnoLV2V5kBUkG0pRWL0yrYtCk9vlcAj3hSZ9s7hGp", params, headers)

#params = "";
#conn.request("GET", "/helper/users/nLDaKdMs5dML5Aggv0oWYqbVQGYSNLrJkPhr44kG4ED1f6fn1aj9xi6S3YZEI3v", params, headers)

# #################### CREDIT ##############
# params = "{\"credit\": {\"credit_amount\": 20}}"
# conn.request("POST", "/credit/5mx8Wq3CfuHa8WKL6dBu188ZxU6D9AQWNesh9h2yh24VMVswdTDKxCCdGj65qHm", params, headers)

#################### HELPER DRIVER ######################
# params = "";
# conn.request("POST", "/regulator/helper/drivers/quvoHm66WWP4yvGTwZDtTahSMRKcqKov9EON84i1QapTrFwBk3L7Go9UkMAxbjs", params, headers)

response = conn.getresponse()
print response.status, response.reason
data = response.read()
print data
