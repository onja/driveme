Drive Me Rest Server
====================

API REST JSON
-------------

# Rest Server URL: 

Dev: http://api.driveme.dev.tfs.im
Prod: http://api.driveme-app.com

# Resources:
- [/driver/authenticate](https://github.com/thefloatingstone/driveme_server_rest#driver-authenticate-driverauthenticate)
- [/driver/logout](https://github.com/thefloatingstone/driveme_server_rest#driver-logout)
- [/authenticate](https://github.com/thefloatingstone/driveme_server_rest#authenticate-authenticate)
- [/car (CRU)](https://github.com/thefloatingstone/driveme_server_rest#car-car)
- [/ride (CRUD)](https://github.com/thefloatingstone/driveme_server_rest#ride-ride)
- [/user (CRU)](https://github.com/thefloatingstone/driveme_server_rest#user-user)
- [/company (CRU)](https://github.com/thefloatingstone/driveme_server_rest#company-company)
- [/payment (R)](https://github.com/thefloatingstone/driveme_server_rest#payment-payment)
- [/credit (R)](https://github.com/thefloatingstone/driveme_server_rest#cre-payment)
- [/reservation (CRU)](https://github.com/thefloatingstone/driveme_server_rest#reservation-reservation)

# Helpers:

- [/helper/cars (R)](https://github.com/thefloatingstone/driveme_server_rest#list-cars-helpercars)
- [/helper/rides (R)](https://github.com/thefloatingstone/driveme_server_rest#list-rides-helperrides)
- [/helper/users (R)](https://github.com/thefloatingstone/driveme_server_rest#list-users-helperusers)

# Driver App methods and examples

- [Authentication](https://github.com/thefloatingstone/driveme_server_rest#authenticate-authenticate)
    - [Example 1: driver authentication with no error](https://github.com/thefloatingstone/driveme_server_rest#example-1-driver-authentication-with-no-error)
- [Logout](https://github.com/thefloatingstone/driveme_server_rest#driver-logout)
    - [Example 1: driver logout with no error](https://github.com/thefloatingstone/driveme_server_rest#example-1-driver-logout-with-no-error)
- [Update car position and status](https://github.com/thefloatingstone/driveme_server_rest#update-car-position-and-status)
    - [Example 1: Update car's position only](https://github.com/thefloatingstone/driveme_server_rest#example-1-update-cars-position-only)
    - [Example 2: Update status and car's position](https://github.com/thefloatingstone/driveme_server_rest#example-2-update-status-and-cars-position)

# Passenger App methods and examples

- [Authentication](https://github.com/thefloatingstone/driveme_server_rest#authenticate-authenticate)
    - [Example 2: passenger authentication with no error](https://github.com/thefloatingstone/driveme_server_rest#example-2-passenger-authentication-with-no-error)
- [Get Cars by position and status](https://github.com/thefloatingstone/driveme_server_rest#get-cars-by-position-and-status)
    - [Example 1: Get available cars for a normal ride](https://github.com/thefloatingstone/driveme_server_rest#example-1-get-available-cars-for-a-normal-ride)

# API methods

## Driver Authenticate

### Specifications

`HTTP POST /driver/authenticate`
```json
{
    "driver":{
        "driver_identity": <driver_identity>,
        "code_pin": <code_pin>,
        "car_id": <car_id>,
        "notification_id"
    }
}
```
with the following:
 - `driver_identity`, driver identity a 6 generated characters (MANDATORY),
 - `code_pin`, driver number code pin (MANDATORY),
 - `car_id`, id of the car, the number registration (MANDATORY),
 - `notification_id`, Id for the notifications (push) (MANDATORY),

 `HTTP RESPONSE [HTTP code]`

```json
{
    "error": {
        "error_code": <error_code>,
        "error_message": <error_message>
    }
}
```

with the following:
 - `error_code`, an error code, 0 if no error, negative otherwise,
 - `error_message`, an error message, empty if no error,

 ### Error codes

HTTP Code | Error code  | Error message                             | Error description
:-------- | ----------: | :---------------------------------------- | :----------------
200       | 0           |                                           | No error
200       | -3          | Authentication failed                     | The driver_identity or the code_pin or the car_id are not correct
200       | -50         | Missing driver:driver_identity            | The field 'driver_identity' of the object 'driver' is missing, and this field is mandatory
200       | -51         | Missing driver:code_pin                   | The field 'code_pin' of the object 'driver' is missing, and this field is mandatory
200       | -52         | Missing driver:car_id                     | The field 'car_id' of the object 'driver' is missing, and this field is mandatory
200       | -53         | Missing driver:notification_id            | The field 'notification_id' of the object 'driver' is missing, and this field is mandatory

### Examples

#### Example 1: driver authentication with no error

`HTTP POST /driver/authenticate`
```json
{
    "driver": {
        "driver_identity": "64972",
        "code_pin": "1234",
        "car_id": "AME197974CD",
        "notification_id": "VQSscRFKInqyR"
    }
}
```
`HTTP RESPONSE [200]`
```json
{
    "session": {
        "session_id": "yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz",
        "session_priority": "normal"
    },
    "error": {
        "error_code": "0",
        "error_message": ""
    }
}
```

#### Example 2: driver authentication with error

`HTTP POST /driver/authenticate`
```json
{
    "driver": {
        "driver_identity": "64972",
        "car_id": "AME197974CD",
        "notification_id": "VQSscRFKInqyR"
    }
}
```
`HTTP RESPONSE [200]`
```json
{
    "error": {
        "error_code": "-51",
        "error_message": "Missing driver:code_pin"
    }
}
```

## Driver Logout

### Specifications

`HTTP DELETE /driver/logout/<session_id>`


with the following:
 - `session_id`, the session id retrieved with the `authenticate` method (MANDATORY),
 

 `HTTP RESPONSE [HTTP code]`

```json
{
    "error": {
        "error_code": <error_code>,
        "error_message": <error_message>
    }
}
```

with the following:
 - `error_code`, an error code, 0 if no error, negative otherwise,
 - `error_message`, an error message, empty if no error,

 ### Error codes

HTTP Code | Error code  | Error message                             | Error description
:-------- | ----------: | :---------------------------------------- | :----------------
200       | 0           |                                           | No error
200       | -1          | Not found                                 | The driver with the session_id passed doesn't exists
200       | -9          | Missing session_id                        | The 'session_id ' is missing in the URL

### Examples

#### Example 1: driver logout with no error

`HTTP DELETE /driver/logout/yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz`

`HTTP RESPONSE [200]`
```json
{
    "error": {
        "error_code": "0",
        "error_message": ""
    }
}
```

## Authenticate `/authenticate`

### Specifications

`HTTP POST /authenticate`
```json
{
    "user":{
        "user_id": <user_id>,
        "user_type": <user_type>,
        "user_priority": <user_priority>,
        "subscription_code"; <subscription_code>,
        "user_device": {
            "os_name": <os_name>,
            "os_version": <os_version>,
            "notification_id": <notification_id>
        }
    }
}
```

with the following:
 - `user_id`, a 64 characters string generated by the client application (MANDATORY),
 - `user_type`, one of the following : 'passenger', 'driver', 'regulator' (MANDATORY),
 - `user_priority`, one of the following : 'high', 'normal', 'low' (OPTIONAL, the default value is 'normal'),
 - `subscription_code`, random number : length 10 (MANDATORY)
 - `user_device`, object with the following fields:
    - `os_name`, one of the following : 'iOS', 'Androïd' (MANDATORY),
    - `os_version`, for instance '6.0' (MANDATORY),
    - `notification_id`, Id for the notifications (push) (MANDATORY),

`HTTP RESPONSE [HTTP code]`

```json
{
    "session": {
        "session_id": <session_id>,
        "session_priority": <session_priority>
    },
    "error": {
        "error_code": <error_code>,
        "error_message": <error_message>
    }
}
```


with the following:
 - `session_id`, a 64 characters string generated by the rest server, which has to be present in every request,
 - `session_priority`, one of the following : 'high', 'normal', 'low',
 - `error_code`, an error code, 0 if no error, negative otherwise,
 - `error_message`, an error message, empty if no error,

### Error codes

HTTP Code | Error code  | Error message                             | Error description
:-------- | ----------: | :---------------------------------------- | :----------------
200       | 0           |                                           | No error
409       | -10         | Missing user:user_id                      | The field 'user_id' of the object 'user' is missing, and this field is mandatory
409       | -11         | Missing user:user_type                    | The field 'user_type' of the object 'user' is missing, and this field is mandatory
409       | -12         | Missing user:user_device:os_name          | The field 'os_name' of the object 'user':'user_device' is missing, and this field is mandatory
409       | -13         | Missing user:user_device:os_version       | The field 'os_version' of the object 'user':'user_device' is missing, and this field is mandatory
409       | -14         | Missing user:user_device:notification_id  | The field 'notification_id' of the object 'user':'user_device' is missing, and this field is mandatory
409       | -15         | Wrong value for user:user_type            | The only values authorized for user:user_type are : 'passenger', 'driver', 'regulator'
409       | -16         | Wrong value for user:user_priority        | The only values authorized for user:user_priority are : 'high', 'normal', 'low'
409       | -17         | Wrong value for user:user_device:os_name  | The only values authorized for user:user_device:os_name are : 'iOS', 'Androïd',
409       | -18         | Missing user:subscription_code            | The field 'subscription_code' of the object 'user' is missing and this field is mandatory 

### Examples

#### Example 2: passenger authentication with no error

`HTTP POST /authenticate`
```json
{
    "user": {
        "user_id": "XTqA9JrRowM7gZYVQSscRFKInqyRjQjQEPgZYVQSscRFKIeHdxjHedgZYVQSscRF",
        "user_type": "passenger",
        "subscription_code": "1205478965",
        "user_device": {
            "os_name": "iOS",
            "os_version":"6",
            "notification_id":"VQSscRFKInqyR"
        }
    }
}
```
`HTTP RESPONSE [200]`
```json
{
    "session": {
        "session_id": "yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz",
        "session_priority": "normal"
    },
    "error": {
        "error_code": "0",
        "error_message": ""
    }
}
```

#### Example 2: authentication with one error

`HTTP POST /authenticate`
```json
{
    "user": {
        "user_id": "XTqA9JrRowM7gZYVQSscRFKInqyRjQjQEPgZYVQSscRFKIeHdxjHedgZYVQSscRF",
        "user_type": "wrong_value",
        "subscription_code": "1205478965",
        "user_device": {
            "os_name": "iOS",
            "os_version":"6",
            "notification_id":"VQSscRFKInqyR"
        }
    }
}
```
`HTTP RESPONSE [409]`
```json
{
    "error": {
        "error_code": "-11",
        "error_message": "Missing user:user_type"
    }
}
```

## Car `/car`

### Create Car

`HTTP POST /car/<session_id>`
```json
{    
    "car":{
        "car_id": <car_id>,
        "car_photos": <car_photos>,
        "car_description": <car_description>,
        "car_status": <car_status>,
        "car_level": <car_level>,
        "car_price": <car_price>,
        "car_estimated_price": <car_estimated_price>,
        "car_position": {
            "longitude": <longitude>,
            "latitude": <latitude>
        },
        "driver":{
            "driver_identity": <driver_identity>,
            "driver_name": <driver_name>
        }        
    }
}
```
with the following:
 - `session_id`, the session id retrieved with the `authenticate` method (MANDATORY),
 - `car_id`, car registration number (MANDATORY),
 - `longitude`, longitude of the car position (MANDATORY),
 - `latitude`, latitude of the car position (MANDATORY),
 - `car_photos`, photos of the car,
 - `car_status`, status of the car with one the following values (OPTIONAL, if not present, the car status does not change, the default status, right after authentication is 'not-available'):
    - 'available': the car is available for a new ride,
    - 'not-available': the car is not available for ride, probably because the driver is doing a break,    
    following values will be induced automatically by the server thanks to the ride transactions (DO NOT USE THEM DIRECTLY)
    - 'ride': the car is riding a passenger,
    - 'pick-up': the car is going to pickup a passenger,
    - 'waiting-passenger', the driver is ready at the ride start place
- `car_level`, one of the following values (MANDATORY):
    - 'basic': standard car,
    - 'premium': premium car, like 'jaguar',
 - `car_price`, string representation of the price to be displayed in the App, for instance : 5€/min or 5€/km
 - `car_estimated_price`, estimated price of the ride in Euro, for instance : 15.00
 - `driver_name`, name of the driver, for instance : 'Alex' (MANDATORY)
 - `driver_identity`, identity of the driver : '123456' (MANDATORY)
 
 `HTTP RESPONSE [HTTP code]`

```json
{    
    "error": {
        "error_code": <error_code>,
        "error_message": <error_message>
    }
}
```
with the following:
 - `error_code`, an error code, 0 if no error, negative otherwise,
 - `error_message`, an error message, empty if no error

### Examples

#### Example 1: Create a new car

`HTTP POST /car/XTqA9JrRowM7gZYVQSscRFKInqyRjQjQEPgZYVQSscRFKIeHdxjHedgZYVQSscRF`
```json
{
    
    "car":{
        "car_id": "DSC9874AD",
        "car_description": "berline super luxe",
        "car_status": "available",
        "car_level": "premium",
        "car_price": "5€/min",
        "car_position": {
            "longitude": "-2.232424",
            "latitude": "48.594206"
        },
        "driver":{
            "driver_identity": "123456",
            "driver_name": "onja rachid"
        }        
    }
}
```
`HTTP RESPONSE [201]`
```json
{
    "error": {
        "error_code": "0",
        "error_message": ""
    }
}
```

#### Example 1: Create a new car with error

`HTTP POST /car/XTqA9JrRowM7gZYVQSscRFKInqyRjQjQEPgZYVQSscRFKIeHdxjHedgZYVQSscRF`
```json
{
    
    "car":{
        "car_id": "DSC9874AD",
        "car_description": "berline super luxe",
        "car_status": "available",
        "car_level": "premium",
        "car_price": "5€/min",
        "car_position": {
            "latitude": "48.594206"
        },
        "driver":{
            "driver_identity": "123456",
            "driver_name": "onja rachid"
        }        
    }
}
```
`HTTP RESPONSE [409]`
```json
{
    "error": {
        "error_code": "-20",
        "error_message": "Missing car:car_position:longitude"
    }
}
```

### Update Car position and status

`HTTP PUT /driver/car/<car_id>/<session_id>`
```json
{    
    "car":{
        "car_photos": <car_photos>,
        "car_description": <car_description>,
        "car_status": <car_status>,
        "car_level": <car_level>,
        "car_price": <car_price>,
        "car_estimated_price": <car_estimated_price>,
        "car_position": {
            "longitude": <longitude>,
            "latitude": <latitude>
        },
        "driver":{
            "driver_identity": <drive_identity>,
            "driver_name": <driver_name>
        }        
    }
}
```

with the following:
 - `car_id`, user_id of the driver (MANDATORY),
 - `session_id`, the session id retrieved with the `authenticate` method (MANDATORY),
 - `longitude`, longitude of the car position (MANDATORY),
 - `latitude`, latitude of the car position (MANDATORY),
 - `car_status`, status of the car with one the following values (OPTIONAL, if not present, the car status does not change, the default status, right after authentication is 'not-available'):
    - 'available': the car is available for a new ride,
    - 'not-available': the car is not available for ride, probably because the driver is doing a break,
    The following values will be induced automatically by the server thanks to the ride transactions (DO NOT USE THEM DIRECTLY)
    - 'ride': the car is riding a passenger,
    - 'pick-up': the car is going to pickup a passenger,
    - 'waiting-passenger', the driver is ready at the ride start place
 - `car_level`, one of the following values (MANDATORY):
    - 'basic': standard car,
    - 'premium': premium car, like 'jaguar',
 - `car_price`, string representation of the price to be displayed in the App, for instance : 5€/min or 5€/km
 - `car_estimated_price`, estimated price of the ride in Euro, for instance : 15.00
 - `driver_name`, name of the driver, for instance : 'Alex' (MANDATORY)
 - `driver_identity`, identity of the driver : '123456' (MANDATORY)


`HTTP RESPONSE [HTTP code]`

```json
{
    "car": {
        "car_status": <car_status>
    },
    "ride": {
        "ride_status": <ride_status>,
    },
    "error": {
        "error_code": <error_code>,
        "error_message": <error_message>
    }
}
```

with the following:
- `car_status`, status of the car with one the following values :
    - 'available': the car is available for a new ride,
    - 'not-available': the car is not available for ride, probably because the driver is doing a break,
    The following values will be induced automatically by the server thanks to the ride transactions (DO NOT USE THEM DIRECTLY)
    - 'ride': the car is riding a passenger,
    - 'pick-up': the car is going to pickup a passenger,
    - 'waiting-passenger', the driver is ready at the ride start place
- `ride_status`, status of the ride, one of the following values :
    - 'tmp': start and end position finded,
    - 'pending': waiting for the driver to confirm,
    - 'accepted': the driver has accepted,
    - 'ride-pack': the driver start the ride,
    - 'ride-minute': the passenger change location on ride,
- `error_code`, an error code, 0 if no error, negative otherwise,
- `error_message`, an error message, empty if no error,

### Examples

#### Example 1: Update car's position only

`HTTP PUT /driver/car/AME197974CD/yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz`
```json
{
    
    "car":{
        "car_position": {
            "longitude": "-2.232424",
            "latitude": "48.594206"
        }
    }
}
```
`HTTP RESPONSE [200]`
```json
{
    "car": {
        "car_status": "ride"
    },
    "error": {
        "error_code": "0",
        "error_message": ""
    }
}
```

#### Example 2: Update status and car's position

`HTTP PUT /driver/car/AME197974CD/yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz`
```json
{
    
    "car":{
        "car_position": {
            "longitude": "-2.232424",
            "latitude": "48.594206"
        },
        "car_status": "not-available"
    }
}
```
`HTTP RESPONSE [200]`
```json
{
    "car": {
        "car_status": "not-available"
    },
    "error": {
        "error_code": "0",
        "error_message": ""
    }
}
```

#### Example 3: Update car's position with an active ride

`HTTP PUT /driver/car/AME197974CD/yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz`
```json
{
    
    "car":{
        "car_position": {
            "longitude": "-2.232424",
            "latitude": "48.594206"
        }
    }
}
```
`HTTP RESPONSE [200]`
```json
{
    "car": {
        "car_status": "available"
    },
    "ride": {
        "ride_status": "accepted"
    },
    "error": {
        "error_code": "0",
        "error_message": ""
    }
}
```

#### Example 4: Update car's position with an error : missing latitude

`HTTP PUT /driver/car/AME197974CD/yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz`
```json
{
    
    "car":{
        "car_position": {
            "longitude": "-2.232424",
            "latitude": ""
        }
    }
}
```
`HTTP RESPONSE [409]`
```json
{
    "error": {
        "error_code": "-21",
        "error_message": "Missing car:car_position:latitude"
    }
}
```

### Get Car informations

`HTTP GET /car/<car_id>/<session_id>`

with the following:
 - `car_id`, registration number of the car (MANDATORY),
 - `session_id`, the session id retrieved with the `authenticate` method (MANDATORY)

`HTTP RESPONSE [HTTP CODE]` 

```json
{
    
    "car": {
        "car_id": <car_id>,
        "car_status": <car_status>,
        "car_photos": <car_photos>,
        "car_description": <car_description>,
        "car_status": <car_status>,
        "car_position": {
            "longitude": <longitude>,
            "latitude": <latitude>,
            "ETA": <ETA>
        },
        "car_level": <car_level>,
        "car_price": <car_price>,
        "car_estimated_price": <car_estimated_price>
    },
    "driver": {
        "driver_identity": <driver_identity>,
        "driver_name": <driver_name>,
        "driver_photos": <driver_photos>
    },
    "error": {
        "error_code": <error_code>,
        "error_message": <error_message>
    }
}
```

with the following:
 - `car_id`, user_id of the driver,
 - `longitude`, longitude of the car position,
 - `latitude`, latitude of the car position,
 - `ETA`, Estimate Time of Arrival of the Car in minutes,
 - `car_status`, status of the car with one the following values:
    - 'available': the car is available for a new ride,
    - 'not-available': the car is not available for ride, probably because the driver is doing a break,
    - 'ride': the car is riding a passenger,
    - 'pick-up': the car is going to pickup a passenger,
    - 'waiting-passenger', the driver is ready at the ride start place
 - `car_level`, one of the following values:
    - 'basic': standard car,
    - 'premium': premium car, like 'jaguar',
 - `car_price`, string representation of the price to be displayed in the App, for instance : 5€/min or 5€/km
 - `car_estimated_price`, estimated price of the ride in Euro, for instance : 15.00
 - `driver_name`, name of the driver, for instance : 'Alex'
 - `driver_identity`, identity of the driver : '123456'
 - `driver_photos`, path of the driver photos,
 - `error_code`, an error code, 0 if no error, negative otherwise,
 - `error_message`, an error message, empty if no error

### Exemples

### Exemple 1: Retrieve car without error

`HTTP GET /car/AME197974CD/yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz`

`HTTP RESPONSE [200]`
```json
{
    "car": {
        "car_id": "AME197974CD",
        "car_status": "available",                
        "car_position": {
            "longitude": "-2.232424",
            "latitude": "48.594206", 
            "ETA": "10"
        },
        "car_level": "basic",
        "car_price": "1.5€/mn",
        "car_estimated_price": "15.00"
    },
    "driver": {
        "driver_identity": "123456",
        "driver_name": "andria",
        "driver_photos": "http://api.driveme.dev.tfs.im/images/driver/img-driver.jpeg"

    },
    "error": {
        "error_code": "0",
        "error_message": ""
    }
}
```

### Exemple 2 : Get car with error
`HTTP GET /car/AME197974CD/yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz`


`HTTP RERSPONSE [404]`
```json
{
    "error": {
        "error_code": "-1",
        "error_message": "Car Not found"
    }
}
```

HTTP Code | Error code  | Error message                             | Error description
:-------- | ----------: | :---------------------------------------- | :----------------
200       | 0           |                                           | No error
404       | -1          | Not found                                 | The car with the id passed doesn't exists
405       | -2          | Method not allowed                        | the method 'Put' is not allowed on the resource /car, you probably missed the car_id in the URL
409       | -9          | Missing session:session_id                | The field 'session_id ' of the object 'session' is missing, and this field is mandatory
409       | -20         | Missing car:car_position:longitude        | The field 'longitude' of the object 'car:car_position' is missing, and this field is mandatory
409       | -21         | Missing car:car_position:latitude         | The field 'latitude' of the object 'car:car_position' is missing, and this field is mandatory
409       | -22         | Missing car:car_level                     | The field 'car_level' of the object 'car' is missing, and this field is mandatory
409       | -23         | Wrong value for car:car_status            | The only values authorized for car:car_status are : 'available', 'not-available'
409       | -24         | Wrong value for car:car_level             | The only values authorized for car:car_level are : 'basic', 'premium'
409       | -25         | Wrong value for car:driver:driver_name    | The field 'driver_name' of the object 'car:driver' is missing, and this field is mandatory


## Ride `/ride`

### Create a Ride

`HTTP POST /ride/<session_id>`
```json
{
    
    "ride": {
        "car_id": <car_id>,
        "start_date": <start_date>,
        "subscription_code"; <subscription_code>,
        "start_position": {
            "longitude": <start_position:longitude>,
            "latitude": <start_position:latitude>,
            "address": <address>
        },
        "end_position": {
            "longitude": <end_position:longitude>,
            "latitude": <end_position:latitude>,
            "address": <address>
        }
    },
}
```

with the following:
 - `session_id`, the session id retrieved with the `authenticate` method (MANDATORY),
 - `car_id`, user_id of the driver (MANDATORY), 
 - `start_date`, date of the ride's start, can be 'now' or a timestamp in miliseconds (OPTIONAL, default value is 'now'),
 - `subscription_code`, random number : length 10 (MANDATORY),
 - `start_position:longitude`, longitude of the ride starts' position (MANDATORY),
 - `start_position:latitude`, latitude of the ride starts' position (MANDATORY),
 - `start_position:address`, address of the ride starts' position (MANDATORY),
 - `end_position:longitude`, longitude of the ride end's position (MANDATORY),
 - `end_position:latitude`, latitude of the ride end's position (MANDATORY),
 - `end_position:address`, address of the ride end's position (MANDATORY),

`HTTP RESPONSE [HTTP code]`

```json
{
    "ride": {
        "ride_id": <ride_id>,
        "ride_status": <ride_status>,
        "start_position": {
            "longitude": <longitude>,
            "latitude": <latitude>,
            "address": <address>
        },
        "end_position": {
            "longitude": <longitude>,
            "latitude": <latitude>,
            "address": <address>
        },
        "ride_pack_price": <ride_pack_price>,
        "ride_minute_price": <ride_minute_price>,
        "ride_total_price": <ride_total_price>,
        "ride_estimated_time": <ride_estimated_time>,
        "ride_estimated_length": <ride_estimated_length>
    },
    "car": {
        "car_id": <car_id>
        "car_name": <car_name>,
        "car_description": <car_description>,
        "car_estimated_price": <car_estimated_price>,
        "eta": <eta>
    },
    "driver": {
        "driver_name": <driver_name>,
        "driver_photos": <driver_photos>
    },
    "error": {
        "error_code": <error_code>,
        "error_message": <error_message>
    }
}
```

with the following:
 - `ride_id`, id of the ride created,
 - `ride_status`, status of the ride, one of the following values :
    - 'tmp': start and end position finded,
    - 'pending': waiting for the driver to confirm,
    - 'accepted': the driver has accepted,
    - 'refused': the driver has refused,
    - 'canceled': the driver has canceled,
    - 'ride-pack': the driver start the ride,
    - 'ride-minute': the passenger change location on ride,
    - 'finished': the ride has finished
 - `start_position:longitude`, longitude of the ride starts' position,
 - `start_position:latitude`, latitude of the ride starts' position,
 - `start_position:address`, address of the ride starts' position,
 - `end_position:longitude`, longitude of the ride end's position,
 - `end_position:latitude`, latitude of the ride end's position,
 - `end_position:address`, address of the ride end's position,
 - `ride_estimated_time`, estimated time of the ride in minutes,
 - `ride_estimated_length`, estimated length of the ride in km,
 - `car_id`, car registration number,
 - `car_estimated_price`, estimated price of the ride in Euro, for instance : 15.00
 - `driver_name`, name of the driver, for instance : 'Alex' (MANDATORY)
 - `driver_identity`, identity of the driver : '123456' (MANDATORY)
 - `driver_name`, name of driver,
 - `driver_photos`, photos of the driver,
 - `error_code`, an error code, 0 if no error, negative otherwise,
 - `error_message`, an error message, empty if no error,

### Examples

#### Example 1: Create a normal ride with a 'basic' car

`HTTP POST /ride/yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz`
```json
{
    
    "ride": {
        "car_id": "XTqA9JrRowM7gZYVQSscRFKInqyRjQjQEPgZYVQSscRFKIeHdxjHedgZYVQSscRF",
        "start_date": "now",
        "subscription_code": "1265987546",
        "start_position": {
            "longitude": "-2.232424",
            "latitude": "48.594206",
            "address": "Allée Nicolas - Sainte Brigitte - 22380 Saint Cast le Guildo"
        },
        "end_position": {
            "longitude": "2.326375",
            "latitude": "48.834653",
            "address": "30bis, rue Gassendi - 75014 Paris"
        }        
    },
    
    "error": {
        "error_code": "0",
        "error_message": ""
    }
}
```
`HTTP RESPONSE [201]`
```json
{
    "ride": {
        "ride_id": "UX3kQhqfC7ExwAqyAKJkuG57OpNdsTFOMkjrlGBmFbH8aaBjDwi0GUwXjlwQH6z2",
        "ride_status": "pending",
        "start_position": {
            "longitude": "-2.232424",
            "latitude": "48.594206",
            "address": "Allée Nicolas - Sainte Brigitte - 22380 Saint Cast le Guildo"
        },
        "end_position": {
            "longitude": "2.326375",
            "latitude": "48.834653",
            "address": "30bis, rue Gassendi - 75014 Paris"
        },
        "ride_estimated_time": "5 min",
        "ride_estimated_length": "16 km"
    },
    "car": {
        "car_name": "porsche cayenne",
        "car_description": "300 ch",
        "car_estimated_price": "15",
        "eta": "5 min"
    },
    "driver": {
        "driver_name": "sebasien leob",
        "driver_photos": "http://api.driveme.dev.tfs.im/images/driver/img-driver.jpeg"
    },
    "error": {
        "error_code": "0",
        "error_message": ""
    }
}
```

#### Example 2: Create a normal ride with a 'premium' car

`HTTP POST /ride/yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz`
```json
{
    "ride": {
        "car_id": "XTqA9JrRowM7gZYVQSscRFKInqyRjQjQEPgZYVQSscRFKIeHdxjHedgZYVQSscRF",
        "car_level": "premium",
        "start_date": "now",
        "subscription_code": "1265987546",
        "start_position": {
            "longitude": "-2.232424",
            "latitude": "48.594206",
            "address": "Allée Nicolas - Sainte Brigitte - 22380 Saint Cast le Guildo"
        },
        "end_position": {
            "longitude": "2.326375",
            "latitude": "48.834653",
            "address": "30bis, rue Gassendi - 75014 Paris"
        }
    },
}
```
`HTTP RESPONSE [201]`
```json
{
    "ride": {
        "ride_id": "UX3kQhqfC7ExwAqyAKJkuG57OpNdsTFOMkjrlGBmFbH8aaBjDwi0GUwXjlwQH6z2",
        "ride_status": "pending",
        "start_position": {
            "longitude": "-2.232424",
            "latitude": "48.594206",
            "address": "Allée Nicolas - Sainte Brigitte - 22380 Saint Cast le Guildo"
        },
        "end_position": {
            "longitude": "2.326375",
            "latitude": "48.834653",
            "address": "30bis, rue Gassendi - 75014 Paris"
        },
        "ride_estimated_time": "5 min",
        "ride_estimated_length": "16 km"
    },
    "car": {
        "car_name": "porsche cayenne",
        "car_description": "300 ch",
        "car_estimated_price": "15",
        "eta": "5"
    },
    "driver": {
        "driver_name": "sebasien leob",
        "driver_photos": "http://api.driveme.dev.tfs.im/images/driver/img-driver.jpeg"
    },
    "error": {
        "error_code": "0",
        "error_message": ""
    }
}
```
## Ride `/ride`

### Get ride informations

`HTTP GET /ride/<ride_id>/<session_id>`

`HTTP RESPONSE [HTTP code]`
```json
{
    "ride": {
        "ride_id" : <ride_id>,
        "ride_status": <ride_status>,
        "subscription_code": <subscription_code>,
        "car_id": <car_id>,
        "car_level": <car_level>,
        "start_date": <start_date>,
        "start_position": {
            "longitude": <start_position:longitude>,
            "latitude": <start_position:latitude>,
            "address": <address>
        },
        "end_position": {
            "longitude": <end_position:longitude>,
            "latitude": <end_position:latitude>,
            "address": <address>
        },
        "ride_estimated_time": <ride_estimated_time>,
        "ride_estimated_length": <ride_estimated_length>
    },
    "car": {
        "car_name": <car_name>,
        "car_description": <car_description>,
        "car_estimated_price": <car_estimated_price>,
        "eta": <eta>
    },
    "driver": {
        "driver_name": <driver_name>,
        "driver_photos": <driver_photos>
    },
    "error": {
        "error_code": "",
        "error_message": ""
    }
}
```
#### Example 1: Get ride informations without error

`HTTP GET /ride/UX3kQhqfC7ExwAqyAKJkuG57OpNdsTFOMkjrlGBmFbH8aaBjDwi0GUwXjlwQH6z2/yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz`

`HTTP RESPONSE [201]`
```json
{
    "ride": {
        "ride_id": "UX3kQhqfC7ExwAqyAKJkuG57OpNdsTFOMkjrlGBmFbH8aaBjDwi0GUwXjlwQH6z2",
        "ride_status": "pending",
        "start_position": {
            "longitude": "-2.232424",
            "latitude": "48.594206",
            "address": "Allée Nicolas - Sainte Brigitte - 22380 Saint Cast le Guildo"
        },
        "end_position": {
            "longitude": "2.326375",
            "latitude": "48.834653",
            "address": "30bis, rue Gassendi - 75014 Paris"
        },
        "ride_estimated_time": "5 min",
        "ride_estimated_length": "16 km"
    },
    "car": {
        "car_name": "porsche cayenne",
        "car_description": "300 ch",
        "car_estimated_price": "15",
        "eta": "5"
    },
    "driver": {
        "driver_name": "sebasien leob",
        "driver_photos": "http://api.driveme.dev.tfs.im/images/driver/img-driver.jpeg"
    },
    "error": {
        "error_code": "0",
        "error_message": ""
    }
}
```
#### Example 2: Get ride informations with error

`HTTP GET /ride/UX3kQhqfC7ExwAqyAKJkuG57OpNdsTFOMkjrlGBmFbH8aaBjDwi0GUwXjlwQH6z2/yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz`

`HTTP RESPONSE [404]`
```json
{
    "error": {
        "error_code": "-1",
        "error_message": "Not found"
    }
}
```

## Ride `/ride`

### Update a Ride

`HTTP PUT /ride/<ride_id>/<session_id>`
```json
{
    
    "ride": {
        "ride_status": <ride_status>,
        "car_id": <car_id>,
        "car_level": <car_level>,
        "start_date": <start_date>,
        "subscription_code": <subscription_code>,
        "start_position": {
            "longitude": <start_position:longitude>,
            "latitude": <start_position:latitude>,
            "address": <address>
        },
        "end_position": {
            "longitude": <end_position:longitude>,
            "latitude": <end_position:latitude>,
            "address": <address>
        }
    },
}
```
`HTTP RESPONSE [HTTP code]`
```json
{
    "ride": {
        "ride_id" : <ride_id>,
        "ride_status": <ride_status>,
        "subscription_code": <subscription_code>,
        "car_id": <car_id>,
        "car_level": <car_level>,
        "start_date": <start_date>,
        "start_position": {
            "longitude": <start_position:longitude>,
            "latitude": <start_position:latitude>,
            "address": <address>
        },
        "end_position": {
            "longitude": <end_position:longitude>,
            "latitude": <end_position:latitude>,
            "address": <address>
        },
        "ride_estimated_time": <ride_estimated_time>,
        "ride_estimated_length": <ride_estimated_length>
    },
    "car": {
        "car_name": <car_name>,
        "car_description": <car_description>,
        "car_estimated_price": <car_estimated_price>,
        "eta": <eta>
    },
    "driver": {
        "driver_name": <driver_name>,
        "driver_photos": <driver_photos>
    },
    "error": {
        "error_code": "",
        "error_message": ""
    }
}
```
#### Example 1: Update ride informations without error

`HTTP PUT /ride/UX3kQhqfC7ExwAqyAKJkuG57OpNdsTFOMkjrlGBmFbH8aaBjDwi0GUwXjlwQH6z2/yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz`
```json
{
    "ride": {
        "ride_status": "pending"
        "car_id": "XTqA9JrRowM7gZYVQSscRFKInqyRjQjQEPgZYVQSscRFKIeHdxjHedgZYVQSscRF",
        "car_level": "premium",
        "start_date": "now",
        "subscription_code": "1265987546",
        "start_position": {
            "longitude": "-2.232424",
            "latitude": "48.594206",
            "address": "Allée Nicolas - Sainte Brigitte - 22380 Saint Cast le Guildo"
        },
        "end_position": {
            "longitude": "2.326375",
            "latitude": "48.834653",
            "address": "30bis, rue Gassendi - 75014 Paris"
        }
    }
}
```

`HTTP RESPONSE [201]`
```json
{
    "ride": {
        "ride_id": "UX3kQhqfC7ExwAqyAKJkuG57OpNdsTFOMkjrlGBmFbH8aaBjDwi0GUwXjlwQH6z2",
        "ride_status": "pending",
        "start_position": {
            "longitude": "-2.232424",
            "latitude": "48.594206",
            "address": "Allée Nicolas - Sainte Brigitte - 22380 Saint Cast le Guildo"
        },
        "end_position": {
            "longitude": "2.326375",
            "latitude": "48.834653",
            "address": "30bis, rue Gassendi - 75014 Paris"
        },
        "ride_estimated_time": "5 min",
        "ride_estimated_length": "16 km"
    },
    "car": {
        "car_name": "porsche cayenne",
        "car_description": "300 ch",
        "car_estimated_price": "15",
        "eta": "5"
    },
    "driver": {
        "driver_name": "sebasien leob",
        "driver_photos": "http://api.driveme.dev.tfs.im/images/driver/img-driver.jpeg"
    },
    "error": {
        "error_code": "0",
        "error_message": ""
    }
}
```

#### Example 2: Update ride informations with error

`HTTP PUT /ride/UX3kQhqfC7ExwAqyAKJkuG57OpNdsTFOMkjrlGBmFbH8aaBjDwi0GUwXjlwQH6z2/yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz`
```json
{
    "ride": {
        "ride_status": "pending"
        "car_id": "XTqA9JrRowM7gZYVQSscRFKInqyRjQjQEPgZYVQSscRFKIeHdxjHedgZYVQSscRF",
        "car_level": "premium",
        "start_date": "now",
        "subscription_code": "1265987546",
        "start_position": {
            "latitude": "48.594206",
            "address": "Allée Nicolas - Sainte Brigitte - 22380 Saint Cast le Guildo"
        },
        "end_position": {
            "longitude": "2.326375",
            "latitude": "48.834653",
            "address": "30bis, rue Gassendi - 75014 Paris"
        }
    }
}
```

`HTTP RESPONSE [404]`
```json
{
    "error": {
        "error_code": "-30",
        "error_message": "Missing ride:start_position:longitude"
    }
}
```
## Ride `/ride`

### Delete a Ride

`HTTP DELETE /ride/<ride_id>/<session_id>`

`HTTP RESPONSE [HTTP code]`
```json
{
    "error": {
        "error_code": "0",
        "error_message": ""
    }
}
```

#### Example 1: Delete ride

`HTTP DELETE /ride/UX3kQhqfC7ExwAqyAKJkuG57OpNdsTFOMkjrlGBmFbH8aaBjDwi0GUwXjlwQH6z2/yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz`

`HTTP RESPONSE [201]`
```json
{
    "error": {
        "error_code": "0",
        "error_message": ""
    }
}
```

#### Example 2: Delete ride with error

`HTTP DELETE /ride/UX3kQhqfC7ExwAqyAKJkuG57OpNdsTFOMkjrlGBmFbH8aaBjDwi0GUwXjlwQH6z2/yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz`

`HTTP RESPONSE [404]`
```json
{
    "error": {
        "error_code": "-1",
        "error_message": "Not found"
    }
}
```

### Error codes

HTTP Code | Error code  | Error message                             | Error description
:-------- | ----------: | :---------------------------------------- | :----------------
201       | 0           |                                           | No error
404       | -1          | Not found                                 | The car with the id passed doesn't exists
405       | -2          | Method not allowed                        | the method 'GET' is not allowed on the resource /ride, you probably missed the ride_id in the URL or wanted to do a POST
409       | -9          | Missing session:session_id                | The field 'session_id ' of the object 'session' is missing, and this field is mandatory
409       | -30         | Missing ride:start_position:longitude     | The field 'longitude' of the object 'ride:start_position' is missing, and this field is mandatory
409       | -31         | Missing ride:start_position:latitude      | The field 'latitude' of the object 'ride:start_position' is missing, and this field is mandatory
409       | -32         | Missing ride:start_position:address       | The field 'address' of the object 'ride:start_position' is missing, and this field is mandatory
409       | -33         | Missing ride:end_position:longitude       | The field 'longitude' of the object 'ride:end_position' is missing, and this field is mandatory
409       | -34         | Missing ride:end_position:latitude        | The field 'latitude' of the object 'ride:end_position' is missing, and this field is mandatory
409       | -35         | Missing ride:end_position:address         | The field 'address' of the object 'ride:end_position' is missing, and this field is mandatory
409       | -37         | Wrong value for car_level                 | The only values authorized for ride:car_level are : 'basic', 'premium'
409       | -40         | Missing for car_id                        | The field 'car_id' of the object 'ride' is missing, and this field is mandatory
409       | -41         | Missing for car_level                     | The field 'car_level' of the object 'ride' is missing, and this field is mandatory
409       | -42         | Missing ride:subscription_code               | The field 'subscription_code' of the object 'ride' is missing, and this field is mandatory


## User `/user`

### Get user informations

`HTTP GET /user/<user_id>/<session_id>`

`HTTP RESPONSE [HTTP code]`
```json
{

    "user":{
        "user_id": <user_id>,
        "user_name": <user_name>,
        "user_phone": <user_phone>,
        "user_email": <user_email>,
        "code_pin": <code_pin>,
        "subscription_code": <subscription_code>,
        "user_device": {
            "os_name": <os_name>,
            "os_version": <os_version>,
            "notification_id": <notification_id>
        }
    },
    "error": {
        "error_code": "",
        "error_message": ""
    }
}
```
with the following:
 - `user_id`, a 64 characters string generated by the client application,
 - `user_name`, user name,
 - `user_phone`, user phone,
 - `user_email`, user email,
 - `code_pin`, code pin for the user,
 - `subscription_code`, random number : length 10,
 - `user_device`, object with the following fields:
    - `os_name`, one of the following : 'iOS', 'Androïd',
    - `os_version`, for instance '6.0',
    - `notification_id`, Id for the notifications (push),

#### Example 1: Get user informations without error

`HTTP GET /user/XTqA9JrRowM7gZYVQSscRFKInqyRjQjQEPgZYVQSscRFKIeHdxjHedgZYVQSscRF/yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz`

`HTTP RESPONSE [201]`
```json
{
    "user": {
        "user_id": "XTqA9JrRowM7gZYVQSscRFKInqyRjQjQEPgZYVQSscRFKIeHdxjHedgZYVQSscRF",
        "user_name": "rachid",
        "user_phone": "061549875",
        "user_email": "driveme@gmail.com",
        "subscription_code": "1205478965",
        "code_pin": "12054",
        "user_device": {
            "os_name": "iOS",
            "os_version":"6",
            "notification_id":"VQSscRFKInqyR"
        }
    },
    "error": {
        "error_code": "0",
        "error_message": ""
    }
}
```

#### Example 1: Get user informations with error

`HTTP GET /user/XTqA9JrRowM7gZYVQSscRFKInqyRjQjQEPgZYVQSscRFKIeHdxjHedgZYVQSscRF/yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz`

`HTTP RESPONSE [200]`
```json
{
    "error": {
        "error_code": "-1",
        "error_message": "Not found"
    }
}
```
### Create user

`HTTP POST /user`
```json
{
    "user":{
        "user_id" : <user_id>,
        "user_name": <user_name>,
        "user_phone": <user_phone>,
        "user_email": <user_email>,
        "code_pin": <code_pin>,
        "user_type": <user_type>,
        "user_priority": <user_priority>,
        "user_device": {
            "os_name": <os_name>,
            "os_version": <os_version>,
            "notification_id": <notification_id>
        }
    }
}
```

`HTTP RESPONSE [HTTP code]`
```json
{
    "error": {
        "error_code": "",
        "error_message": ""
    }
}
```

#### Example 1: Create user without error

`HTTP POST /user`
```json
{
    "user": {
        "user_type": "passenger",
        "user_phone": "061549875",
        "user_email": "driveme@gmail.com",
        "code_pin": "12054",
        "user_priority": "normal",
        "user_device": {
            "os_name": "iOS",
            "os_version":"6",
            "notification_id":"VQSscRFKInqyR"
        }
    }
}
```
`HTTP RESPONSE [201]`
```json
{
    "error": {
        "error_code": "0",
        "error_message": ""
    }
}
```
#### Example 2: Create user with error

`HTTP POST /user`
```json
{
    "user": {
        "user_type": "passenger",
        "user_device": {
            "os_name": "iOS",
            "os_version":"6",
            "notification_id":"VQSscRFKInqyR"
        }
    }
}
```
`HTTP RESPONSE [200]`
```json
{
    "error": {
        "error_code": "-11",
        "error_message": "Missing user:user_priority"
    }
}
```
### Update user informations

`HTTP PUT /user/<user_id>/<session_id>`
```json
{
    "user":{
        "user_firstname": <user_firstname>,
        "user_lastname": <user_lastname>,
        "user_phone": <user_phone>,
        "user_email": <user_email>,
        "code_pin": <code_pin>,
        "subscription_code": <subscription_code>
    }
}
```
`HTTP RESPONSE [HTTP code]`
```json
{
    "error": {
        "error_code": "",
        "error_message": ""
    }
}
```
#### Example 1: Update user without error

`HTTP PUT /user/yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz/XTqA9JrRowM7gZYVQSscRFKInqyRjQjQEPgZYVQSscRFKIeHdxjHedgZYVQSscRF`
```json
{
    "user": {
            "user_type": "passenger",
            "user_priority": "low",
            "user_status": "waiting-car"
        }
    }
}
```
`HTTP RESPONSE [200]`
```json
{
    "error": {
        "error_code": "0",
        "error_message": ""
    }
}
```
#### Example 2: Update user with error

`HTTP PUT /user/yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz/XTqA9JrRowM7gZYVQSscRFKInqyRjQjQEPgZYVQSscRFKIeHdxjHedgZYVQSscRF`
```json
{
    "user": {
        "user_type": "passenger"
        }
    }
}
```
`HTTP RESPONSE [200]`
```json
{
    "error": {
        "error_code": "-1",
        "error_message": "Not found"
    }
}
```

### Error codes


HTTP Code | Error code  | Error message                             | Error description
:-------- | ----------: | :---------------------------------------- | :----------------
200       | 0           |                                           | No error
409       | -10         | Missing user:user_id                      | The field 'user_id' of the object 'user' is missing, and this field is mandatory
409       | -12         | Wrong user:user_priority                  | The only values authorized for user:user_priority are : 'high', 'normal', 'low'


## Company `/company`

### Get company informations

`HTTP GET /company/<company_id>/<session_id>`

`HTTP RESPONSE [HTTP code]`
```json
{

    "company":{
        "company_address": <company_address>,
        "company_address2": <company_address2>,
        "subscription_code": <subscription_code>,
        "company_code_postal": <company_code_postal>,
        "company_ville": <company_ville>,
        "company_pays": <company_pays>,
        "company_siret": <company_siret>,
        "company_tva": <company_tva>
    },
    "error": {
        "error_code": "",
        "error_message": ""
    }
}
```
with the following:
 - `company_id`, company ID: identity field,
 - `company_address`, company address,
 - `company_address2`, another address of the company,
 - `subscription_code`, random number : length 10,
 - `company_code_postal`, company costal code,
 - `company_ville`, company ville location,
 - `company_pays`, company pays location,
 - `company_siret`, company siret number,
 - `company_tva`, company tva rates number

 #### Example 1: Get company informations without error

`HTTP GET /company/150/XTqA9JrRowM7gZYVQSscRFKInqyRjQjQEPgZYVQSscRFKIeHdxjHedgZYVQSscRF`

`HTTP RESPONSE [201]`
```json
{
    "company":{
        "company_address": "22 Rue du Débarcadère",
        "company_address2": "",
        "company_code_postal": "75017",
        "company_ville": "paris",
        "company_pays": "France",
        "company_siret": "73282932000074",
        "company_tva": "20"
    }
}
```

#### Example 1: Get company informations with error

`HTTP GET /company/yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz/XTqA9JrRowM7gZYVQSscRFKInqyRjQjQEPgZYVQSscRFKIeHdxjHedgZYVQSscRF`

`HTTP RESPONSE [404]`
```json
{
    "error": {
        "error_code": "-1",
        "error_message": "Not found"
    }
}
```

 ### Create a company

`HTTP POST /company>`
```json
{
    "company":{
        "company_address": <company_address>,
        "company_address2": <company_address2>,
        "company_code_postal": <company_code_postal>,
        "company_ville": <company_ville>,
        "company_pays": <company_pays>,
        "company_siret": <company_siret>,
        "company_tva": <company_tva>
    }
}
```

`HTTP RESPONSE [HTTP code]`
```json
{
    "error": {
        "error_code": "",
        "error_message": ""
    }
}
```

#### Example 1: Create company without error

`HTTP POST /company`
```json
{
    "company":{
        "company_address": "22 Rue du Débarcadère",
        "company_address2": "",
        "company_code_postal": "75017",
        "company_ville": "paris",
        "company_pays": "France",
        "company_siret": "73282932000074",
        "company_tva": "20"
    }
}
```
`HTTP RESPONSE [201]`
```json
{
    "error": {
        "error_code": "0",
        "error_message": ""
    }
}
```
#### Example 2: Create company with error

`HTTP POST /company`
```json
{
    "company":{
        "company_address2": "",
        "company_code_postal": "75017",
        "company_ville": "paris",
        "company_pays": "France",
        "company_siret": "73282932000074",
        "company_tva": "20"
    }
}
```
`HTTP RESPONSE [409]`
```json
{
    "error": {
        "error_code": "-30",
        "error_message": "Missing company:company_address"
    }
}
```

### Update company informations

`HTTP PUT /company/<company_id><session_id>`
```json
{

    "company":{
        "company_address": <company_address>,
        "company_address2": <company_address2>,
        "company_code_postal": <company_code_postal>,
        "company_ville": <company_ville>,
        "company_pays": <company_pays>,
        "company_siret": <company_siret>,
        "company_tva": <company_tva>
    }
}
```

`HTTP RESPONSE [HTTP code]`
```json
{

    "error": {
        "error_code": "",
        "error_message": ""
    }
}
```

#### Example 1: Update company informations without error

`HTTP PUT /company`
```json
{
    "company":{
        "company_address": "22 Rue du Débarcadère",
        "company_address2": "",
        "company_code_postal": "75017",
        "company_ville": "paris",
        "company_pays": "France",
        "company_siret": "73282932000074",
        "company_tva": "20"
    }
}
```
`HTTP RESPONSE [200]`
```json
{
    "error": {
        "error_code": "0",
        "error_message": ""
    }
}
```

#### Example 1: Update company informations with error

`HTTP PUT /company`
```json
{
    "company":{
        "company_address": "22 Rue du Débarcadère",
        "company_address2": "",
        "company_code_postal": "75017",
        "company_pays": "France",
        "company_siret": "73282932000074",
        "company_tva": "20"
    }
}
```
`HTTP RESPONSE [200]`
```json
{
    "error": {
        "error_code": "-33",
        "error_message": "Missing company:company_ville"
    }
}
```

### Error codes

HTTP Code | Error code  | Error message                             | Error description
:-------- | ----------: | :---------------------------------------- | :----------------
200       | 0           |                                           | No error
404       | -1          | Not found                                 | No cars available with the specified filter
405       | -2          | Method not allowed                        | the method 'PUT' or DELETE  are not allowed on the resource /company, you probably wanted to do a 'GET'
409       | -30         | Missing company:company_address           | The field 'company_address' of the object 'company' is missing, and this field is mandatory
409       | -31         | Missing company:company_code_postal       | The field 'company_code_postal' of the object 'company' is missing, and this field is mandatory 
409       | -33         | Missing company:company_ville             | The field 'company_ville' of the object 'company' is missing, and this field is mandatory
409       | -34         | Missing company:company_pays              | The field 'company_pays' of the object 'company' is missing, and this field is mandatory
409       | -36         | Missing company:company_siret             | The field 'company_siret' of the object 'company' is missing, and this field is mandatory
409       | -37         | Missing company:company_tva               | The field 'company_tva' of the object 'company' is missing, and this field is mandatory

## Payment `/payment`

### Check credit 

`HTTP POST /payment/check/<session_id>`
```json
{
    "payment": {
        "payment_type": <payment_type>
    }
}
```
with the following:
 - `session_id`, the session id retrieved with the `authenticate` method (MANDATORY),
 - `payment_type`, the type of the payment with one the following values (MANDATORY):
    - 'credit-minute': credit minute in driveme application
    - 'credit-company': credit company offer to company
    - 'credit-card': blue credit card

`HTTP RESPONSE [HTTP code]`
```json
{
    "payment": {
        "payment_status": <payment_status>,
        "payment_message": <payment_message>
    },
    "error": {
        "error_code": "",
        "error_message": ""
    }
}
```
with the following:
 - `payment_message`, the session id retrieved with the `authenticate` method (MANDATORY),
 - `payment_status`, status result of the credit check:
    - 'OK': credit or payment valid
    - 'failed': credit card invalid or credit minute or credit company insufficient   

### Error codes

HTTP Code | Error code  | Error message                             | Error description
:-------- | ----------: | :---------------------------------------- | :----------------
200       | 0           |                                           | No error
404       | -1          | Not found                                 | No rides available with the specified filter
405       | -2          | Method not allowed                        | the method 'PUT' or DELETE  are not allowed on the resource /company, you probably wanted to do a 'GET'
409       | -70         | Missing payment:payment_type              | The field 'payment_type' of the object 'payment' is missing, and this field is mandatory


## Credit `/credit`

### Check credit 

`HTTP POST /credit/<session_id>`
```json
{
    "credit": {
        "credit_amount": <credit_amount>
    }
}
```
with the following:
 - `session_id`, the session id retrieved with the `authenticate` method (MANDATORY),
 - `credit_amount`, amount of the minute's credit (MANDATORY)   

`HTTP RESPONSE [HTTP code]`
```json
{
    "credit": {
        "solde": <solde>
    },
    "error": {
        "error_code": "",
        "error_message": ""
    }
}
```
with the following:
 - `account_balance`, account balance of the user ,
 - `payment_status`, status result of the credit check:
    - 'OK': credit or payment valid
    - 'failed': credit card invalid or credit minute or credit company insufficient   

### Error codes

HTTP Code | Error code  | Error message                             | Error description
:-------- | ----------: | :---------------------------------------- | :----------------
200       | 0           |                                           | No error
404       | -1          | Not found                                 | No cars available with the specified filter
405       | -2          | Method not allowed                        | the method 'PUT' or DELETE  are not allowed on the resource /company, you probably wanted to do a 'GET'
409       | -80         | Missing credit object param               | The param 'credit' of the object 'credit' is missing, and this field is mandatory
409       | -81         | Missing credit:credit_amount              | The field 'credit_amount' of the object 'credit' is missing, and this field is mandatory
 

### Examples

#### Example 1: Create credit account

`HTTP POST /credit/yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz`
```json
{
    "credit": {
        "credit_amount": "available"        
    }
}
```

`HTTP RESPONSE [200]`
```json
{
    "credit": {
        "account_balance": "10",
    },    
    "error": {
        "error_code": "0",
        "error_message": ""
    }
}
```
## Reservation `/reservation`

### Create reservation 

`HTTP POST /reservation/<session_id>`
```json
{
    "reservation": {
        "driver_identity": <driver_identity>,
        "reservation_date": <reservation_date>,
        "reservation_time": <reservation_time>,
        "start_position": {
            "longitude": <longitude>,
            "latitude": <latitude>,
            "address": <address>
        },
        "end_position": {
            "longitude": <longitude>,
            "latitude": <latitude>,
            "address": <address>
        },
    }
}
```
with the following:
 - `session_id`, the session id retrieved with the `authenticate` method (MANDATORY),
 - `driver_identity`, identity of the driver's car(OPTIONAL),
 - `reservation_date` Date of the reservation (MANDATORY),
 - `reservation_time` Start time of the reservation ride (MANDATORY),
 - `start_position:longitude`, start position longitude of the reservation ride (MANDATORY),
 - `start_position:latitude`, start position latitude of the reservation ride (MANDATORY),
 - `start_position:address`, start position address of the reservation ride (MANDATORY),
 - `end_position:longitude`, end position longitude of the reservation ride (MANDATORY),
 - `end_position:latitude`, end position latitude of the reservation ride (MANDATORY),
 - `end_position:address`, end position address of the reservation ride (MANDATORY),


`HTTP RESPONSE [HTTP code]`
```json
{
    "reservation": {
        "reference": <reference>,
        "reservation_date": <reservation_date>,
        "reservation_time": <reservation_time>,
        "driver": {
            "driver_firstname": <driver_firstname>,
            "driver_lastname": <driver_lastname>
        },
        "ride": {
            "start_position": {
                "longitude": <longitude>,
                "latitude": <latitude>,
                "address": <address>,
            },
            "end_position": {
                "longitude": <longitude>,
                "latitude": <latitude>,
                "address": <address>,
            },
            "ride_estimated_time": <ride_estimated_time>,
            "ride_estimated_length": <ride_estimated_length>,
        }
    },
    "error": {
        "error_code": "",
        "error_message": ""
    }
}
```
with the following:
 - `reference`, reference of the reservation ride ,
 - `reservation_date`, the date of the reservation:
 - `reservation_time`, start time of the reservation ride
 - `start_position:longitude`, longitude of the reservation ride starts' position,
 - `start_position:latitude`, latitude of the reservation ride starts' position,
 - `start_position:address`, address of the reservation ride starts' position,
 - `end_position:longitude`, longitude of the reservation ride end's position,
 - `end_position:latitude`, latitude of the reservation ride end's position,
 - `end_position:address`, address of the reservation ride end's position,
 - `ride_estimated_time` estimated duration of the reservation ride,
 - `ride_estimated_length` estimated length of the reservation ride,

 ### Error codes

HTTP Code | Error code  | Error message                                 | Error description
:-------- | ----------: | :----------------------------------------     | :----------------
200       | 0           |                                               | No error
404       | -1          | Not found                                     | No cars available with the specified filter
405       | -2          | Method not allowed                            | the method 'PUT' or 'POST' are not allowed on the resource /helper/cars, you probably wanted to do a 'GET'
409       | -9          | Missing session:session_id                    | The field 'session_id ' of the object 'session' is missing, and this field is mandatory
409       | -90         | Missing reservation:start_position:address    | The field 'address' of the object 'reservation:start_position' is missing, and this field is mandatory if the object 'reservation' is present
409       | -91         | Missing reservation:end_position:address      | The field 'address' of the object 'reservation:end_position' is missing, and this field is mandatory if the object 'reservation' is present
409       | -92         | missing reservation:reservation_date          | The field `reservation_date` of the object 'reservation' is missing, and this field is mandatory
409       | -93         | missing reservation:reservation_time          | The field `reservation_time` of the object 'reservation' is missing, and this field is mandatory
409       | -94         | Driver not exist                              | Driver of the entering driver_identity not exist
409       | -95         | Driver not available                          | Driver of the entering driver_identity not available on the reservation date

### Examples

#### Example 1: Create reservation without error

`HTTP POST /reservation/yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz`
```json
{
    "reservation": {
        "driver_identity": "123456",
        "reservation_date": "2013-10-30",
        "reservation_time": "12:30",
        "start_position": {
            "longitude": "-2.232424",
            "latitude": "48.594206",
            "address": "Allee Nicolas - Sainte Brigitte - 22380 Saint Cast le Guildo"
        },
        "end_position": {
            "longitude": "2.326375",
            "latitude": "48.834653",
            "address": "30bis, rue Gassendi - 75014 Paris"
        },
    }
}
```
`HTTP RESPONSE [200]`
```json
{
    "reservation": {
        "reference": "Réservation-10/30",
        "reservation_date": "2013-10-30",
        "reservation_time": "12:30",
        "driver": {
            "driver_firstname": "seb",
            "driver_lastname": "test"
        },
        "ride": {
            "start_position": {
                "longitude": "-2.232424",
                "latitude": "48.594206",
                "address": "Allee Nicolas - Sainte Brigitte - 22380 Saint Cast le Guildo"
            },
            "end_position": {
               "longitude": "2.326375",
                "latitude": "48.834653",
                "address": "30bis, rue Gassendi - 75014 Paris"
            },
            "ride_estimated_time": "5" ,
            "ride_estimated_length": "20",
        }
    },
    "error": {
        "error_code": "",
        "error_message": ""
    }
}
```

#### Example 1: Create reservation with error driver not exist

`HTTP POST /reservation/yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz`
```json
{
    "reservation": {
        "driver_identity": "000124",
        "reservation_date": "2013-12-31",
        "reservation_time": "12:30",
        "start_position": {
            "longitude": "-2.232424",
            "latitude": "48.594206",
            "address": "Allee Nicolas - Sainte Brigitte - 22380 Saint Cast le Guildo"
        },
        "end_position": {
            "longitude": "2.326375",
            "latitude": "48.834653",
            "address": "30bis, rue Gassendi - 75014 Paris"
        },
    }
}
```

`HTTP RESPONSE [200]`
```json
{
    "error": {
        "error_code": "-94",
        "error_message": "Le chauffeur n'existe pas"
    }
}
```

#### Example 1: Create reservation with error driver not available

`HTTP POST /reservation/yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz`
```json
{
    "reservation": {
        "driver_identity": "123456",
        "reservation_date": "2013-12-31",
        "reservation_time": "12:30",
        "start_position": {
            "longitude": "-2.232424",
            "latitude": "48.594206",
            "address": "Allee Nicolas - Sainte Brigitte - 22380 Saint Cast le Guildo"
        },
        "end_position": {
            "longitude": "2.326375",
            "latitude": "48.834653",
            "address": "30bis, rue Gassendi - 75014 Paris"
        },
    }
}
```

`HTTP RESPONSE [200]`
```json
{
    "error": {
        "error_code": "-95",
        "error_message": "Chauffeur non disponible"
    }
}
```

### Update reservation 

`HTTP PUT /reservation/<reservation_id><session_id>`
```json
{
    "reservation": {
        "reservation_status": <reservation_status>,
        "driver_identity": <driver_identity>
    }
}
```
with the following:
 - `session_id`, the session id retrieved with the `authenticate` method (MANDATORY),
 - `reservation_status`, status of the car with one the following values (MANDATORY): 
    - 'pending': Created and wait for driver acceptance,
    - 'riding': Accepted and ready for ride,
    - 'finished': Reservation ride finished
- `driver_identity`, identity of the driver's car(OPTIONAL),
`HTTP RESPONSE [HTTP code]`
```json
{
    "error": {
        "error_code": "",
        "error_message": ""
    }
}
```

#### Example 1: Udpdate reservation

`HTTP POST /reservation/10/yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz`
```json
{
    "reservation": {
        "reservation_status": "riding",
        "driver_identity": "176078"
    }
}
```

`HTTP RESPONSE [200]`
```json
{
    "error": {
        "error_code": "0",
        "error_message": ""
    }
}
```

## List Cars `/helper/cars`

### Get Cars by position and status

`HTTP GET /helper/cars/<session_id>`
```json
{
    "car": {
        "car_status": <car_status>,
        "car_level": <car_level>,
        "start_position": {
            "longitude": <start_position:longitude>,
            "latitude": <start_position:latitude>,
            "address": <address>
        },
        "end_position": {
            "longitude": <end_position:longitude>,
            "latitude": <end_position:latitude>,
            "address": <address>
        }
    }
}
```

with the following:
 - `session_id`, the session id retrieved with the `authenticate` method (MANDATORY),
 - the object `ride` is OPTIONAL, but if it's present then the start and end position are MANDATORY
 - `start_position:longitude`, filter start longitude location of the object "car or suppliers",
 - `start_position:latitude`, start latitude location of the object,
 - `start_position:address`,  address of the start position (OPTIONAL),
 - `end_position:longitude`, filter end longitude location of the object "car or suppliers",
 - `end_position:latitude`, end latitude of the object,
 - `end_position:address`, address of the end position (OPTIONAL),
 - `car_status`, status of the car with one the following values (OPTIONAL, default value is 'all'):
    - 'all': all cars,
    - 'available': the car is available for a new ride,
    - 'not-available': the car is not available for ride, probably because the driver is doing a break,
    - 'ride': the car is riding a passenger,
    - 'pick-up': the car is going to pickup a passenger,
 - `car_level`, one of the following values (OPTIONAL, default value is 'all'):
    - 'all': standard and premium cars,
    - 'basic': standard cars,
    - 'premium': premium cars, like 'jaguar',

`HTTP RESPONSE [HTTP code]`

```json
{
    "cars": [
        {
            "car": {
                "car_id": <car_id>,
                "car_status": <car_status>,
                "car_position": {
                    "longitude": <longitude>,
                    "latitude": <latitude>,
                    "ETA": <ETA>
                },
                "car_level": <car_level>,
                "car_price": <car_price>,
                "car_estimated_price": <car_estimated_price>,
            },
            "driver": {
                "driver_name": <driver_name>
            }
        },
        {
            "car": {
                "car_id": <car_id>,
                "car_status": <car_status>,
                "car_position": {
                    "longitude": <longitude>,
                    "latitude": <latitude>,
                    "ETA": <ETA>
                },
                "car_level": <car_level>,
                "car_price": <car_price>,
                "car_estimated_price": <car_estimated_price>,
            },
            "driver": {
                "driver_name": <driver_name>
            }
        },
    ],
    "error": {
        "error_code": <error_code>,
        "error_message": <error_message>
    }
}
```

with the following:
 - `car_id`, user_id of the driver,
 - `longitude`, longitude of the car position,
 - `latitude`, latitude of the car position,
 - `ETA`, Estimate Time of Arrival of the Car in minutes,
 - `car_status`, status of the car with one the following values:
    - 'available': the car is available for a new ride,
    - 'not-available': the car is not available for ride, probably because the driver is doing a break,
    - 'ride': the car is riding a passenger,
    - 'pick-up': the car is going to pickup a passenger,
 - `car_level`, one of the following values:
    - 'basic': standard car,
    - 'premium': premium car, like 'jaguar',
 - `car_price`, string representation of the price to be displayed in the App, for instance : 5€/min or 5€/km
 - `car_estimated_price`, estimated price of the ride in Euro, for instance : 15.00
 - `driver_name`, name of the driver, for instance : 'Alex'
 - `error_code`, an error code, 0 if no error, negative otherwise,
 - `error_message`, an error message, empty if no error,

### Error codes

HTTP Code | Error code  | Error message                             | Error description
:-------- | ----------: | :---------------------------------------- | :----------------
200       | 0           |                                           | No error
404       | -1          | Not found                                 | No cars available with the specified filter
405       | -2          | Method not allowed                        | the method 'PUT' or 'POST' are not allowed on the resource /helper/cars, you probably wanted to do a 'GET'
409       | -9          | Missing session:session_id                | The field 'session_id ' of the object 'session' is missing, and this field is mandatory
409       | -30         | Missing ride:start_position:longitude     | The field 'longitude' of the object 'ride:start_position' is missing, and this field is mandatory if the object 'ride' is present
409       | -31         | Missing ride:start_position:latitude      | The field 'latitude' of the object 'ride:start_position' is missing, and this field is mandatory if the object 'ride' is present
409       | -33         | Missing ride:end_position:longitude       | The field 'longitude' of the object 'ride:end_position' is missing, and this field is mandatory if the object 'ride' is present
409       | -34         | Missing ride:end_position:latitude        | The field 'latitude' of the object 'ride:end_position' is missing, and this field is mandatory if the object 'ride' is present
409       | -36         | Wrong value for car_status                | The only values authorized for car:car_status are : 'all', 'available', 'not-available', 'ride', 'pick-up'
409       | -37         | Wrong value for car_level                 | The only values authorized for car:car_level are : 'all', 'basic', 'premium'


### Examples

#### Example 1: Get available cars for a normal ride

`HTTP GET /helper/cars/yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz`
```json
{
    "car": {
        "car_status": "available",
        "car_level": "all",
        "start_position": {
            "longitude": "-2.232424",
            "latitude": "48.594206"
        },
        "end_position": {
            "longitude": "2.326375",
            "latitude": "48.834653"
        }
    }
}
```
`HTTP RESPONSE [200]`
```json
{
    "ride": {
        "ride_estimated_time": "10",
        "ride_estimated_length": "5",
    },
    "cars": [
        {
            "car": {
                "car_id": "XTqA9JrRowM7gZYVQSscRFKInqyRjQjQEPgZYVQSscRFKIeHdxjHedgZYVQSscRF",
                "car_status": "available",
                "car_position": {
                    "longitude": "-2.232424",
                    "latitude": "48.594206", 
                    "ETA": "10"
                },
                "car_level": "basic",
                "car_price": "1.5€/mn",
                "car_estimated_price": "15.00",
            },
            "driver": {
                "driver_name": "Steve"
            }
        },
        {
            "car": {
                "car_id": "qK9LlHWUPoJklclTSL1lguzBJ0YKgoQjlguzBJPEW4lguzBJQDVBeNMWeLRJkY5e",
                "car_status": "available",
                "car_position": {
                    "longitude": "-2.232424",
                    "latitude": "48.594206", 
                    "ETA": "15"
                },
                "car_level": "premium",
                "car_price": "2.5€/mn",
                "car_estimated_price": "25.00",
            },
            "driver": {
                "driver_name": "Alex"
            }
        },
    ],
    "error": {
        "error_code": "0",
        "error_message": ""
    }
}
```

#### Example 2: Get available cars for a normal ride, but no cars available

`HTTP GET /helper/cars/yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz`
```json
{
    "car": {
        "car_status": "available",
        "car_level": "all",
        "start_position": {
            "longitude": "-2.232424",
            "latitude": "48.594206"
        },
        "end_position": {
            "longitude": "2.326375",
            "latitude": "48.834653"
        }
    }
}
```
`HTTP RESPONSE [404]`
```json
{
    "error": {
        "error_code": "-1",
        "error_message": "Not found"
    }
}
```

#### Example 3: Get available cars for a normal ride, but with one error : wrong car_level

`HTTP GET /helper/cars/yoF3YbYpzWTvHj0eUzeCzsp0Hj0eUzeCzy2sujnBWHj0eUzeCzRqHj0eUzeCzodz`
```json
{
    "ride": {
        "start_position": {
            "longitude": "-2.232424",
            "latitude": "48.594206"
        },
        "end_position": {
            "longitude": "2.326375",
            "latitude": "48.834653"
        }
    },
    "car": {
        "car_status": "available",
        "car_level": "wrong_value"
    }
}
```
`HTTP RESPONSE [409]`
```json
{
    "error": {
        "error_code": "-35",
        "error_message": "Wrong value for car:car_level"
    }
}
```


## List Rides `/helper/rides`

`HTTP GET /helper/rides/<session_id>`
```json
{
    "rides": [
        "ride": {
        "ride_id": <ride_id>,
        "ride_status": <ride_status>,
        "start_position": {
            "longitude": <longitude>,
            "latitude": <latitude>,
            "address": <address>
        },
        "end_position": {
            "longitude": <longitude>,
            "latitude": <latitude>,
            "address": <address>
        },
        "ride_estimated_time": <ride_estimated_time>,
        "ride_estimated_length": <ride_estimated_length>
        },
        "car": {
            "car_id": <car_id>
            "car_name": <car_name>,
            "car_description": <car_description>,
            "car_estimated_price": <car_estimated_price>,
            "eta": <eta>
        },
        "driver": {
            "driver_name": <driver_name>,
            "driver_photos": <driver_photos>
        },
        "ride": {
        "ride_id": <ride_id>,
        "ride_status": <ride_status>,
        "start_position": {
            "longitude": <longitude>,
            "latitude": <latitude>,
            "address": <address>
        },
        "end_position": {
            "longitude": <longitude>,
            "latitude": <latitude>,
            "address": <address>
        },
        "ride_estimated_time": <ride_estimated_time>,
        "ride_estimated_length": <ride_estimated_length>
        },
        "car": {
            "car_id": <car_id>
            "car_name": <car_name>,
            "car_description": <car_description>,
            "car_estimated_price": <car_estimated_price>,
            "eta": <eta>
        },
        "driver": {
            "driver_name": <driver_name>,
            "driver_photos": <driver_photos>
        }
    ]
}
```

with the following:
 - `ride_id`, id of the ride created,
 - `ride_status`, status of the ride, one of the following values :
    - 'tmp': start and end position finded,
    - 'pending': waiting for the driver to confirm,
    - 'accepted': the driver has accepted,
    - 'refused': the driver has refused,
    - 'ride-pack': the driver start the ride,
    - 'ride-minute': the passenger change location on ride,
    - 'canceld': the driver has canceled,
 - `start_position:longitude`, longitude of the ride starts' position,
 - `start_position:latitude`, latitude of the ride starts' position,
 - `start_position:address`, address of the ride starts' position,
 - `end_position:longitude`, longitude of the ride end's position,
 - `end_position:latitude`, latitude of the ride end's position,
 - `end_position:address`, address of the ride end's position,
 - `ride_estimated_time`, estimated time of the ride in minutes,
 - `ride_estimated_length`, estimated length of the ride in km,
 - `car_id`, car registration number,
 - `car_estimated_price`, estimated price of the ride in Euro, for instance : 15.00
 - `driver_name`, name of the driver, for instance : 'Alex' (MANDATORY)
 - `driver_identity`, identity of the driver : '123456' (MANDATORY)
 - `driver_name`, name of driver,
 - `driver_photos`, photos of the driver,
 - `error_code`, an error code, 0 if no error, negative otherwise,
 - `error_message`, an error message, empty if no error,


## List Users `/helper/users`

`HTTP GET /helper/users/<session_id>`
```json
{
    "users": [
        "user":{
            "user_id": <user_id>,
            "user_type": <user_type>,
            "user_priority": <user_priority>,
            "user_status": <user_status>,
            "user_device": {
                "os_name": <os_name>,
                "os_version": <os_version>,
                "notification_id": <notification_id>
            }
        },
        "user":{
            "user_id": <user_id>,
            "user_type": <user_type>,
            "user_priority": <user_priority>,
            "user_device": {
                "os_name": <os_name>,
                "os_version": <os_version>,
                "notification_id": <notification_id>
            }
        }
    ]
}
```

with the following:
 - `user_id`, a 64 characters string generated by the client application,
 - `user_type`, one of the following : 'passenger', 'driver', 'regulator',
 - `user_priority`, one of the following : 'high', 'normal', 'low',
 - `user_status`, one of the following : 'available', 'waiting-car', 'ride',
 - `user_device`, object with the following fields:
    - `os_name`, one of the following : 'iOS', 'Androïd',
    - `os_version`, for instance '6.0',
    - `notification_id`, Id for the notifications (push),
