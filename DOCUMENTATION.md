SCENARIO I : course au forfait (ride-pack)
------------------------------------------

1. Le client cherche une voiture
2. le serveur trouve la voiture la plus proche
3. le client clique sur le bouton rouge qui affiche le temps estimé d'arrivée de la voiture
4. Création du ride (ride_status à tmp) => le serveur met automatiquement le car_status a "reserved"
5. Le client valide avec le gros bouton ROUGE et paye
6. update du ride en ride_status "waiting-car"
7. le chauffer est notifié de la course, il clique sur ACCEPTER
8. UPDATE ride_status à "accepted" (le serveur met automatiquement car_status à "pick-up")
9. Le chauffeur arrive pour prendre le passager
10. UPDATE ride_status à "waiting-passenger"  (le serveur met automatiquement car_status à "waiting-passenger")
11. le client arrive et le chauffeur appui sur DEMARRER la course
12. UPDATE ride_status à "ride-pack"  (le serveur met automatiquement car_status à "ride")
13. le chauffeur arrive à destination il appui sur terminer la course
14. UPDATE ride_status à "finished" (le serveur met automatiquement car_status à "available")

SCENARIO II : course a la minute avant arrivée à destination (ride-pack, puis ride-minute)
------------------------------------------------------------------------------------------

1. Le client cherche une voiture
2. le serveur trouve la voiture la plus proche
3. le client clique sur le bouton rouge qui affiche le temps estimé d'arrivée de la voiture
4. Création du ride (ride_status à tmp) => le serveur met automatiquement le car_status a "reserved"
5. Le client valide avec le gros bouton ROUGE et paye
6. update du ride en ride_status "waiting-car"
7. le chauffer est notifié de la course, il clique sur ACCEPTER
8. UPDATE ride_status à "accepted" (le serveur met automatiquement car_status à "pick-up")
9. Le chauffeur arrive pour prendre le passager
10. UPDATE ride_status à "waiting-passenger"  (le serveur met automatiquement car_status à "waiting-passenger")
11. le client arrive et le chauffeur appui sur DEMARRER la course
12. UPDATE ride_status à "ride-pack"  (le serveur met automatiquement car_status à "ride")
13. En cours de route, le client décide de changer de destination, à ce moment le chauffeur appui sur CONTINUER et UPDATE ride_status à "ride-minute" (en gardant toujours l'historique des status, avec les timestamp)
14. le chauffeur arrive à destination il appui sur terminer la course
15. UPDATE ride_status à "finished" (le serveur met automatiquement car_status à "available")
==> dans ce cas le client sera facturé du nombre réel de minute

URLs des Webviews
----------------- 

* Profile => /html/profile 
* Historique des courses => /html/ride_history
* Condition générales de ventes (CGU) => /html/cgu
* Liste des cartes de crédits => /html/cb_list

UPDATE Car Status
----------------- 
Interdire les updates suivants :

ride -> available
pick-up -> available
waiting-passenger -> available

ride -> not-available
pick-up -> not-available
waiting-passenger -> not-available

