<?php


//error_reporting(E_ALL);
//ini_set('display_errors', 'On');
//require_once( $_SERVER['DOCUMENT_ROOT'] . '/config/config.inc.php' );
//require_once 'init.inc.php';

define('APPLICATION_PATH', dirname(__FILE__));

require_once 'php/classes/SmartLog.php';
require_once 'php/classes/Utils.class.php';

//require and load activerecord lib
require_once 'php/libs/php-activerecord/ActiveRecord.php';
require 'config/database.conf.php';
require 'config/driveme.conf.php';
require 'config/email.conf.php';
require 'config/paybox.conf.php';
require 'php/libs/smarty/Smarty.class.php';

ActiveRecord\Config::initialize(function($cfg) {
    $cfg->set_model_directory('php/classes');
    $cfg->set_connections(array(
        'development' => 'mysql://' . DATABASE_USER . ':' . DATABASE_PASSWORD . '@' . DATABASE_HOST . '/' . DATABASE_NAME . '?charset=utf8'));
});

//require_once 'authenticate.php'; 
require_once 'vendor/peej/tonic/src/Tonic/Autoloader.php';
$app = new Tonic\Application(array(

    'load' => array('php/resources/*.php', 'php/resources/helper/*.php', 'php/resources/payment/*.php', 'php/resources/helper/driver/*.php', 'php/resources/regulator/*.php', 'php/resources/helper/regulator/*.php')

        ));

$request = new Tonic\Request();

$resource = $app->getResource($request);
$response = $resource->exec();

$response->output();
