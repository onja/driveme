-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le : Mer 16 Octobre 2013 à 14:42
-- Version du serveur: 5.5.31
-- Version de PHP: 5.3.10-1ubuntu3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `driveme`
--

DELIMITER $$
--
-- Fonctions
--
DROP FUNCTION IF EXISTS `get_distance_in_miles_between_geo_locations`;
CREATE DEFINER=`root`@`localhost` FUNCTION `get_distance_in_miles_between_geo_locations`(geo1_latitude decimal(18, 12), geo1_longitude decimal(18, 12), geo2_latitude decimal(18, 12), geo2_longitude decimal(18, 12)) RETURNS decimal(10,3)
    DETERMINISTIC
BEGIN
  return ((ACOS(SIN(geo1_latitude * PI() / 180) * SIN(geo2_latitude * PI() / 180) + COS(geo1_latitude * PI() / 180) * COS(geo2_latitude * PI() / 180) * COS((geo1_longitude - geo2_longitude) * PI() / 180)) * 180 / PI()) * 60 * 1.1515);
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `rc_account_movements`
--

CREATE TABLE IF NOT EXISTS `rc_account_movements` (
  `account_mvt_id` int(11) NOT NULL AUTO_INCREMENT,
  `account_mvt_debit` float DEFAULT NULL,
  `account_mvt_credit` float DEFAULT NULL,
  `account_mvt_op_id` int(11) DEFAULT NULL,
  `account_mvt_op_type` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`account_mvt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `rc_cars`
--

CREATE TABLE IF NOT EXISTS `rc_cars` (
  `car_id` varchar(255) NOT NULL,
  `driver_identity` int(11) NOT NULL,
  `position_id` int(11) DEFAULT NULL,
  `car_name` varchar(20) DEFAULT NULL,
  `car_description` text,
  `car_photos` varchar(55) NOT NULL,
  `car_status` varchar(20) DEFAULT 'available',
  `car_level` varchar(20) DEFAULT NULL,
  `car_price` varchar(20) DEFAULT NULL,
  `car_estimated_price` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`car_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `rc_cars`
--

INSERT INTO `rc_cars` (`car_id`, `driver_identity`, `position_id`, `car_name`, `car_description`, `car_photos`, `car_status`, `car_level`, `car_price`, `car_estimated_price`) VALUES
('AME197974CD', 123456, 2, 'porsche cayenne', '300 ch', '', 'not-available', NULL, '0.5€ / min', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `rc_credits`
--

CREATE TABLE IF NOT EXISTS `rc_credits` (
  `credit_id` int(11) NOT NULL AUTO_INCREMENT,
  `credit_amount` float DEFAULT '0',
  PRIMARY KEY (`credit_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `rc_credits`
--

INSERT INTO `rc_credits` (`credit_id`, `credit_amount`) VALUES
(2, 0),
(3, 0);

-- --------------------------------------------------------

--
-- Structure de la table `rc_devices`
--

CREATE TABLE IF NOT EXISTS `rc_devices` (
  `device_id` int(11) NOT NULL AUTO_INCREMENT,
  `os_name` varchar(20) NOT NULL,
  `os_version` varchar(20) NOT NULL,
  `notification_id` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`device_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Contenu de la table `rc_devices`
--

INSERT INTO `rc_devices` (`device_id`, `os_name`, `os_version`, `notification_id`) VALUES
(1, 'iOS', '4.2.2', 'SKHKDF2354sqd'),
(2, 'Android', '4.2.2', 'SKHKDF2354sqd'),
(7, 'Android', '4.2.2', 'SKHKDF2354sqd'),
(8, 'iOS', '7.0.2', 'test_notif_id');

-- --------------------------------------------------------

--
-- Structure de la table `rc_drivers`
--

CREATE TABLE IF NOT EXISTS `rc_drivers` (
  `driver_id` int(11) NOT NULL AUTO_INCREMENT,
  `driver_firstname` varchar(20) DEFAULT NULL,
  `driver_lastname` varchar(20) DEFAULT NULL,
  `driver_identity` varchar(20) NOT NULL DEFAULT '',
  `driver_phone` varchar(20) DEFAULT NULL,
  `driver_email` varchar(255) DEFAULT NULL,
  `code_pin` varchar(255) DEFAULT NULL,
  `driver_photos` varchar(255) DEFAULT NULL,
  `session_id` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`driver_id`,`driver_identity`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Contenu de la table `rc_drivers`
--

INSERT INTO `rc_drivers` (`driver_id`, `driver_firstname`, `driver_lastname`, `driver_identity`, `driver_phone`, `driver_email`, `code_pin`, `driver_photos`, `session_id`) VALUES
(1, 'THIEBAUT', 'christophe', '', '0621872652', 'chris.313@hotmail.fr', NULL, NULL, NULL),
(2, 'PADEL', 'Laurent', '', '0699536868', 'cultureconsulting@gmail.com; parisroyallimousine@gmail.com', NULL, NULL, NULL),
(3, 'MEGHELLI', 'Yacine', '', '0760562577', 'yacine.meghelli@gmail.com', NULL, NULL, NULL),
(4, 'AMANOUZE', 'Lahcen', '', '0781328227', 'infocergy@yahoo.fr', NULL, NULL, NULL),
(5, 'ZIMMERMANN', 'Pierre', '', '0671210763', 'pzim@free.fr', NULL, NULL, NULL),
(6, 'SOLTANI', 'Yassine', '', '0635580142', 'yassyne95@hotmail.fr', NULL, NULL, NULL),
(7, 'CORREIA', 'Alexandre', '', '0633301400', 'alexcorreia.dc@gmail.com', NULL, NULL, NULL),
(8, 'GUILLARD', 'Doniphan, François', '', '0612773689', 'doni-60@hotmail.fr', NULL, NULL, NULL),
(9, 'FERNANDEZ', 'Franck', '', '0634296475', 'franck.fernandez75014@gmail.com', NULL, NULL, NULL),
(10, 'Du JONCHAY', 'Arnaud', '', '0660777105', 'dejonchay@yahoo.fr', NULL, NULL, NULL),
(11, 'ROGRON', 'Florian', '', '0625087878', 'florian2727@hotmail.fr', NULL, NULL, NULL),
(12, 'BERMONT', 'Willy', '', '0669443653', 'willybermont@gmail.com', NULL, NULL, NULL),
(13, 'BENSALEM', 'Ahmed Alain', '', '0680133492', 'aabensalem@gmail.com', NULL, NULL, NULL),
(14, 'test', 'driveme', '123456', '0621872655', 'driveme@test.com', '1111', NULL, 'fNmBN1T0Pd6FofLDXVFNrXCWDjASEHyLKzF3u7FK1tdr4wViaLzssBaGDE6S0Qp');

-- --------------------------------------------------------

--
-- Structure de la table `rc_gratuities`
--

CREATE TABLE IF NOT EXISTS `rc_gratuities` (
  `gratuity_id` int(11) NOT NULL,
  `gratuity_amount` float DEFAULT NULL,
  `driver_identity` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  PRIMARY KEY (`gratuity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `rc_photos`
--

CREATE TABLE IF NOT EXISTS `rc_photos` (
  `photos_id` int(11) NOT NULL AUTO_INCREMENT,
  `photos_name` varchar(120) DEFAULT NULL,
  `photoable_id` int(11) DEFAULT NULL,
  `photoable_type` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`photos_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `rc_positions`
--

CREATE TABLE IF NOT EXISTS `rc_positions` (
  `position_id` int(11) NOT NULL AUTO_INCREMENT,
  `positionable_type` varchar(20) DEFAULT NULL,
  `positionable_id` varchar(255) DEFAULT NULL,
  `longitude` decimal(18,12) DEFAULT NULL,
  `latitude` decimal(18,12) DEFAULT NULL,
  `address` text,
  `eta` int(10) DEFAULT NULL,
  PRIMARY KEY (`position_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `rc_positions`
--

INSERT INTO `rc_positions` (`position_id`, `positionable_type`, `positionable_id`, `longitude`, `latitude`, `address`, `eta`) VALUES
(1, 'Car', 'XTqA9JrRowM7gZYVQSscRFKInqyRjQjQEPgZYVQSscRFKIeHdxjHedgZYVQSscRF', -2.232424000000, 48.594206000000, NULL, NULL),
(2, 'Car', 'AME197974CD', 2.311702000000, 48.881333000000, NULL, 670),
(3, NULL, NULL, 2.361631000000, 48.857346000000, NULL, NULL),
(4, NULL, NULL, 2.353591900000, 48.866839200000, '3 rue papin paris', NULL),
(5, 'Ride', '1', 2.361631000000, 48.857346000000, '22 rue malher paris', NULL),
(6, 'Ride', '1', 2.353591900000, 48.866839200000, '3 rue papin paris', NULL),
(7, 'Ride', '2', 2.361631000000, 48.857346000000, '22 rue malher paris', NULL),
(8, 'Ride', '2', 2.353591900000, 48.866839200000, '3 rue papin paris', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `rc_rides`
--

CREATE TABLE IF NOT EXISTS `rc_rides` (
  `ride_id` int(11) NOT NULL AUTO_INCREMENT,
  `ride_status` varchar(20) DEFAULT NULL,
  `car_id` varchar(255) NOT NULL,
  `car_level` varchar(20) NOT NULL,
  `start_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `subscription_code` varchar(20) DEFAULT NULL,
  `ride_estimated_time` varchar(20) DEFAULT NULL,
  `ride_estimated_length` varchar(20) DEFAULT NULL,
  `start_position_id` int(11) DEFAULT NULL,
  `end_position_id` int(11) DEFAULT NULL,
  `current_position_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`ride_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `rc_rides`
--

INSERT INTO `rc_rides` (`ride_id`, `ride_status`, `car_id`, `car_level`, `start_date`, `subscription_code`, `ride_estimated_time`, `ride_estimated_length`, `start_position_id`, `end_position_id`, `current_position_id`) VALUES
(1, 'tmp', 'AME197974CD', '', '2013-10-15 20:48:29', '0987654321', '6.0333333333333', '2,2 km', 5, 6, NULL),
(2, 'finished', 'AME197974CD', '', '2013-10-15 20:50:48', '0987654321', '6.0333333333333', '2,2 km', 7, 8, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `rc_users`
--

CREATE TABLE IF NOT EXISTS `rc_users` (
  `user_id` varchar(255) NOT NULL,
  `user_type` varchar(20) DEFAULT NULL,
  `user_priority` varchar(20) DEFAULT NULL,
  `subscription_code` varchar(20) DEFAULT NULL,
  `device_id` int(11) NOT NULL,
  `session_id` varchar(64) DEFAULT NULL,
  `credit_id` int(11) DEFAULT NULL,
  `account_mvt_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `rc_users`
--

INSERT INTO `rc_users` (`user_id`, `user_type`, `user_priority`, `subscription_code`, `device_id`, `session_id`, `credit_id`, `account_mvt_id`) VALUES
('test_user_seb', 'passenger', 'normal', '8214755973', 8, 'iw3U0qlUWAA415wrOxXpKENUkUQxWfBPS6sCp1b7H7ijX3E2xRas36IwkGMEzLY', 3, NULL),
('XTqA8JrRowM7gZYVQSscRFKInqyRjQjQEPgZYVQSscRFKIeHdxjHedgZYVQSscRX', 'driver', 'normal', '3723759704', 2, 'BJW34piTFt2vUW9KpJX=uz24bF3vviMzujc0gr3Xqv5kD4QIA=LrnmZAY5eslOj', NULL, NULL),
('XTqA8JrRowM7gZYVQSscRFKInqyRjQjQEPgZYVQSscRFKIeHdxjHedgZYVQSscSER', 'driver', 'normal', '8206754697', 7, NULL, 2, NULL),
('XTqA9JrRowM7gZYVQSscRFKInqyRjQjQEPgZYVQSscRFKIeHdxjHedgZYVQSscRX', 'passenger', 'high', '0449051829', 1, 'nLDaKdMs5dML5Aggv0oWYqbVQGYSNLrJkPhr44kG4ED1f6fn1aj9xi6S3YZEI3v', NULL, NULL),('ABcde15Fg', 'regul', 'high', '0129051829', 1, 'nLDaKdMs5dML5Aggv0oWYqbVQGYSNLrJkPhr44kG4ED1f6fn1aj9xi6S3YZEI3z', NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
