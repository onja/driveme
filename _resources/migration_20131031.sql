DROP TABLE IF EXISTS `rc_paybox_data`;
CREATE TABLE IF NOT EXISTS `rc_paybox_data`(
	`id` INT NOT NULL AUTO_INCREMENT,
	`user_id` VARCHAR(255) NULL,
	`porteur` VARCHAR(20) NULL,
  `dateval` VARCHAR(20) NULL,
  `cvv` VARCHAR(20) NULL,
	`num_question` VARCHAR(20) NULL,
	`num_trans` VARCHAR(20) NULL,
	`num_appel` VARCHAR(20) NULL,
	`reference` VARCHAR(20) NULL,
	`code_response` VARCHAR(20) NULL,
	`authorisation` VARCHAR(20) NULL,
	`amount` DECIMAL(6,2) NULL,
	`paybox_status` VARCHAR(10) NULL,
	`comment` VARCHAR(255) NULL,
	`created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` TIMESTAMP NULL,
	PRIMARY KEY(`id`)
) ENGINE = InnoDB;

ALTER TABLE rc_reservations ADD COLUMN reservation_status VARCHAR(20);
ALTER TABLE rc_cars ADD COLUMN created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE rc_cars ADD COLUMN updated_at timestamp NULL;
ALTER TABLE rc_drivers ADD COLUMN created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE rc_rides ADD COLUMN created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE rc_rides ADD COLUMN updated_at timestamp NULL; 
ALTER TABLE rc_reservations ADD COLUMN created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE rc_reservations ADD COLUMN updated_at timestamp NULL; 
ALTER TABLE rc_reservations MODIFY COLUMN created_at timestamp NULL ;
ALTER TABLE rc_users ADD COLUMN created_at timestamp NULL;
ALTER TABLE rc_users ADD COLUMN updated_at timestamp NULL;  
ALTER TABLE rc_positions ADD COLUMN created_at timestamp NULL;
ALTER TABLE rc_positions ADD COLUMN updated_at timestamp NULL;