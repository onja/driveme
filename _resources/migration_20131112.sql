DROP TABLE IF EXISTS `rc_cards`;
CREATE TABLE IF NOT EXISTS `rc_cards`(
	`card_id` INT NOT NULL AUTO_INCREMENT,
	`user_id` VARCHAR(255) NULL,
	`title` VARCHAR(255) NULL,
	`number` VARCHAR(20) NULL,
	`cvv` VARCHAR(4) NULL,
	`owner` VARCHAR(255) NULL,
  `expired_date` VARCHAR(5) NULL,  
	`created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` TIMESTAMP NULL,
	PRIMARY KEY(`card_id`)
) ENGINE = InnoDB;

ALTER TABLE rc_reservations ADD COLUMN user_id VARCHAR(255) AFTER reservation_id;

DROP TABLE IF EXISTS `rc_driver_planings`;
CREATE TABLE IF NOT EXISTS `rc_driver_planings`(
	`id` INT NOT NULL AUTO_INCREMENT,
	`driver_identity` INT NULL,
	`driver_day` VARCHAR(3) NULL,
	`driver_hour_min` VARCHAR(10) NULL,
	`driver_hour_max` VARCHAR(10) NULL,
	PRIMARY KEY(`id`)
) ENGINE = InnoDB;

ALTER TABLE rc_reservations ADD COLUMN driver_identity VARCHAR(20);
ALTER TABLE rc_reservations MODIFY COLUMN reservation_date DATE;
ALTER TABLE rc_reservations ADD COLUMN reservation_time TIME AFTER reservation_date;
ALTER TABLE rc_reservations ADD COLUMN start_position_id INT(20);
ALTER TABLE rc_reservations ADD COLUMN end_position_id INT(20);
ALTER TABLE rc_reservations ADD COLUMN estimated_length VARCHAR(5);
ALTER TABLE rc_reservations ADD COLUMN estimated_time VARCHAR(5);
ALTER TABLE rc_reservations ADD COLUMN reservation_price DECIMAL(6,2);

ALTER TABLE rc_driver_planings MODIFY COLUMN driver_day DATE;
ALTER TABLE rc_driver_planings MODIFY COLUMN driver_hour_min TIME;
ALTER TABLE rc_driver_planings MODIFY COLUMN driver_hour_max TIME;