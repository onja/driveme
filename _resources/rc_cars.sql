-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le : Lun 14 Octobre 2013 à 15:42
-- Version du serveur: 5.5.31
-- Version de PHP: 5.3.10-1ubuntu3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `driveme`
--

-- --------------------------------------------------------

--
-- Structure de la table `rc_cars`
--
DROP TABLE IF EXISTS `rc_cars`;
CREATE TABLE IF NOT EXISTS `rc_cars` (
  `car_id` varchar(255) NOT NULL,
  `driver_identity` int(11) NOT NULL,
  `position_id` int(11) DEFAULT NULL,
  `car_name` varchar(20) DEFAULT NULL,
  `car_description` text,
  `car_photos` varchar(55) NOT NULL,
  `car_status` varchar(20) DEFAULT 'available',
  `car_level` varchar(20) DEFAULT NULL,
  `car_price` varchar(20) DEFAULT NULL,
  `car_estimated_price` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`car_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `rc_cars`
--

INSERT INTO `rc_cars` (`car_id`, `driver_identity`, `position_id`, `car_name`, `car_description`, `car_photos`, `car_status`, `car_level`, `car_price`, `car_estimated_price`) VALUES
('AME197974CD', 123456, NULL, 'porsche cayenne', '300 ch', '', 'available', NULL, '0.5€ / min', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
