-- ********************* TABLE CREATE 2013/10/20 **********************
-- -------------------------------- rc_ride_histories -------------------------------------
DROP TABLE IF EXISTS  `rc_ride_histories`;
CREATE TABLE IF NOT EXISTS `rc_ride_histories`(
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `ride_id` INT(11),
  `ride_status` VARCHAR(20),
  `created_at` TIMESTAMP,
  `updated_at` TIMESTAMP,
  PRIMARY KEY(`id`) 
) ENGINE = InnoDB;

ALTER TABLE rc_cars ADD COLUMN connection_status VARCHAR(20);
ALTER TABLE rc_cars ADD COLUMN last_request DATETIME;
ALTER TABLE rc_positions ADD COLUMN zipcode VARCHAR(5) AFTER address;
ALTER TABLE rc_rides ADD COLUMN ride_real_time DATETIME;
ALTER TABLE rc_rides MODIFY COLUMN start_date DATETIME NULL DEFAULT NULL;
ALTER TABLE rc_rides ADD COLUMN ride_pack_price DECIMAL(6, 2) NULL;
ALTER TABLE rc_rides ADD COLUMN ride_minute_price DECIMAL(6, 2) NULL;
ALTER TABLE rc_rides ADD COLUMN ride_total_price DECIMAL(6, 2) NULL;
ALTER TABLE rc_positions MODIFY COLUMN zipcode VARCHAR(5) NULL AFTER address;