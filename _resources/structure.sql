--
-- Structure table `rc_rides`
-- ride_id,
-- ride_status,
-- car_id,
--  car_level,
--  start_date,
--  current_position,{
--  	longitude,
--  	latitude
--  },
--  start_position => {
--    longitude,
--    latitude,
--    address
--  },
--  end_position => {
--    longitude,
--    latitude,
--    address
--  },
--  ride_estimated_time,
--  ride_estimated_length
DROP TABLE  IF EXISTS `rc_rides`;
CREATE TABLE IF NOT EXISTS `rc_rides`(
	`ride_id` INT NOT NULL AUTO_INCREMENT,
	`ride_status` VARCHAR(20) NULL DEFAULT 'tmp',
	`car_id` VARCHAR(255) NOT NULL,
	`car_level` VARCHAR(20) NOT NULL,
	`start_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	`subscription_code` VARCHAR(20) NULL,
	`ride_estimated_time` FLOAT NULL,
	`ride_estimated_length` FLOAT NULL,
	`start_position_id` INT NULL,
	`end_position_id` INT NULL,
	`current_position_id` INT NULL,
	PRIMARY KEY(`ride_id`)

) ENGINE = InnoDB;

--
-- Structure table `rc_users`
--	user_id,
-- 	user_type,
-- 	user_priority,
-- 	user_device => {
-- 		os_name,
-- 		os_version,
-- 		notification_id
-- 	} 
DROP TABLE IF EXISTS `rc_users`;
CREATE TABLE IF NOT EXISTS `rc_users`(
	`user_id` VARCHAR(255) NOT NULL,
	`user_type` VARCHAR(20) NULL,
	`user_status` VARCHAR(20) NULL DEFAULT 'available',
	`user_priority` VARCHAR(20) NULL,
	`subscription_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	`subscription_code` VARCHAR(20) NULL,
	`device_id` INT NOT NULL,
	`session_id` VARCHAR(64) NULL,
	`credit_id` INT  NULL,
	`account_mvt_id` INT NULL,
	PRIMARY KEY(`user_id`)

) ENGINE = InnoDB;

--
-- Structure table `rc_positions`
-- 
-- longitude,
-- latitude,
-- address
-- ETA
-- http://www.cs.duke.edu/csl/docs/mysql-refman/spatial-extensions.html#creating-spatial-columns
-- http://howto-use-mysql-spatial-ext.blogspot.com/
DROP TABLE IF EXISTS `rc_positions`;
CREATE TABLE IF NOT EXISTS `rc_positions`(
	`position_id` INT NOT NULL AUTO_INCREMENT,
	`positionable_type` VARCHAR(20) NULL,
	`positionable_id` VARCHAR(255) NULL,
	`created_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` TIMESTAMP NULL ,
	`longitude` DECIMAL(18,12),
	`latitude` DECIMAL(18,12),
	`address` TEXT,
	`eta` INT(10),
	PRIMARY KEY(`position_id`)
) ENGINE = InnoDB;

--
-- Structure table `rc_devices`
-- 
-- os_name,
-- os_version,
-- notification_id
DROP TABLE IF EXISTS `rc_devices`;
CREATE TABLE IF NOT EXISTS `rc_devices`(
	`device_id` INT NOT NULL AUTO_INCREMENT,
	`os_name` VARCHAR(20) NOT NULL,
	`os_version` VARCHAR(20) NOT NULL,
	`notification_id` VARCHAR(20),
	PRIMARY KEY(`device_id`)
) ENGINE = InnoDB;

--
-- Structure table `rc_cars`
-- 
-- car => {
-- 	car_id,
-- 	car_description,
-- 	car_photos,
-- 	car_position => {
-- 		longitude,
-- 		latitude,
-- 		ETA
-- 	},
-- 	car_status,
-- 	car_level,
-- 	car_price,
-- 	car_estimated_price,
--  driver => {
-- 		driver_id,
-- 		driver_name
-- }
-- }
DROP TABLE IF EXISTS `rc_cars`;
CREATE TABLE IF NOT EXISTS `rc_cars`(
  `car_id` VARCHAR(64) NOT NULL,
	`driver_identity` VARCHAR(64) NOT NULL,
	`driver_name` VARCHAR(20),
	`position_id` INT NULL,
	`car_name` VARCHAR(20) NUll,
	`car_description` TEXT NULL,
	`car_photos` VARCHAR(55) NOT NULL,
	`car_status` VARCHAR(20) NULL DEFAULT 'available',
	`car_level` VARCHAR(20) NULL,
	`car_price` VARCHAR(20) NULL,
	`car_estimated_price` VARCHAR(20) NULL,
	PRIMARY KEY(`car_id`)
) ENGINE = InnoDB;

--
-- Structure table `rc_drivers`
--
DROP TABLE IF EXISTS `rc_drivers`;
CREATE TABLE IF NOT EXISTS `rc_drivers`(
	`driver_id` INT AUTO_INCREMENT,
	`driver_firstname` VARCHAR(20) ,
	`driver_lastname` VARCHAR(20) ,
	`driver_identity` VARCHAR(20) NULL,
	`driver_phone` VARCHAR(20) ,
	`driver_email` VARCHAR(255) ,
	`code_pin` VARCHAR(255) ,
	`driver_photos` VARCHAR(255) ,
	`session_id` VARCHAR(64),
	PRIMARY KEY(`driver_id`, `driver_identity`)
) ENGINE = InnoDB;

--
-- Structure table 'rc_companies'
-- company:{
--        company_id,
--        company_address,
--        company_address2,
--        subscription_code,
--        company_code_postal,
--        company_ville,
--        company_pays,
--        company_siret,
--        company_tva
--    }

DROP TABLE IF EXISTS `rc_companies`;
CREATE TABLE IF NOT EXISTS `rc_companies`(
  `company_id` INT NOT NULL AUTO_INCREMENT,
  `company_name` VARCHAR(55) NULL,
	`company_address` TEXT,
	`company_address2` TEXT,
	`subscription_code` VARCHAR(20) NULL,
	`company_code_postal` VARCHAR(10) NULL,
	`company_ville` VARCHAR(55) NULL,
	`company_pays` VARCHAR(55) NULL,
	`company_siret` INT NULL,
	`company_tva` INT NULL,	
	PRIMARY KEY(`company_id`)

) ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `rc_credits`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rc_credits`;
CREATE TABLE IF NOT EXISTS `rc_credits` (
  `credit_id` INT NOT NULL AUTO_INCREMENT,
  `credit_amount` FLOAT NULL DEFAULT 0,
  PRIMARY KEY (`credit_id`) 
) ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `rc_credit_movements`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rc_account_movements`;
CREATE TABLE IF NOT EXISTS `rc_account_movements` (
  `account_mvt_id` INT NOT NULL AUTO_INCREMENT,
  `account_mvt_debit` FLOAT NULL ,
  `account_mvt_credit` FLOAT NULL ,
  `account_mvt_op_id` INT NULL,
  `account_mvt_op_type` VARCHAR(10) NULL,
  PRIMARY KEY (`account_mvt_id`)
 ) ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `rc_gratuities`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rc_gratuities` ;
CREATE  TABLE IF NOT EXISTS `rc_gratuities` (
  `gratuity_id` INT NOT NULL ,
  `gratuity_amount` FLOAT NULL ,
  `driver_identity` INT NOT NULL ,
  `ride_id` INT NOT NULL ,
  PRIMARY KEY (`gratuity_id`)
)
ENGINE = InnoDB;

DROP TABLE IF EXISTS `rc_photos`;
CREATE TABLE IF NOT EXISTS `rc_photos` (
	`photos_id` INT NOT NULL AUTO_INCREMENT,
	`photos_name` VARCHAR(120) NULL,
	`photoable_id` INT NULL,
	`photoable_type` VARCHAR(20),
	PRIMARY KEY (`photos_id`)
) ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `rc_reservations`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rc_reservations` ;
CREATE  TABLE IF NOT EXISTS `rc_reservations` (
  `reservation_id` INT NOT NULL AUTO_INCREMENT,
  `reservation_status` VARCHAR(20) NULL DEFAULT 'tmp',
	`reservation_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	`start_position_id` INT NULL,
	`end_position_id` INT NULL,
	`ride_id` INT NULL,
	`driver_identity` VARCHAR(20) NULL,
  PRIMARY KEY (`reservation_id`)
)
ENGINE = InnoDB;

