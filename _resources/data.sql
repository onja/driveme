--
-- Contenu de la table `rc_drivers`
--

INSERT INTO `rc_drivers` (`driver_id`, `driver_firstname`, `driver_lastname`, `driver_identity`, `driver_phone`, `driver_email`, `driver_photos`, `code_pin`) VALUES
(1, 'THIEBAUT', 'christophe', NULL, '0621872652', 'chris.313@hotmail.fr', NULL, NULL),
(2, 'PADEL', 'Laurent', NULL, '0699536868', 'cultureconsulting@gmail.com; parisroyallimousine@gmail.com', NULL, NULL),
(3, 'MEGHELLI', 'Yacine', NULL, '0760562577', 'yacine.meghelli@gmail.com', NULL, NULL),
(4, 'AMANOUZE', 'Lahcen', NULL, '0781328227', 'infocergy@yahoo.fr', NULL, NULL),
(5, 'ZIMMERMANN', 'Pierre', NULL, '0671210763', 'pzim@free.fr', NULL, NULL),
(6, 'SOLTANI', 'Yassine', NULL, '0635580142', 'yassyne95@hotmail.fr', NULL, NULL),
(7, 'CORREIA', 'Alexandre', NULL, '0633301400', 'alexcorreia.dc@gmail.com', NULL, NULL),
(8, 'GUILLARD', 'Doniphan, François', NULL, '0612773689', 'doni-60@hotmail.fr', NULL, NULL),
(9, 'FERNANDEZ', 'Franck', NULL, '0634296475', 'franck.fernandez75014@gmail.com', NULL, NULL),
(10, 'Du JONCHAY', 'Arnaud', NULL, '0660777105', 'dejonchay@yahoo.fr', NULL, NULL),
(11, 'ROGRON', 'Florian', NULL, '0625087878', 'florian2727@hotmail.fr', NULL, NULL),
(12, 'BERMONT', 'Willy', NULL, '0669443653', 'willybermont@gmail.com', NULL, NULL),
(13, 'BENSALEM', 'Ahmed Alain', NULL, '0680133492', 'aabensalem@gmail.com', NULL, NULL),
(14, 'test', 'driveme', '123456', '0621872655', 'driveme@test.com', NULL, '1111');

--
-- Contenu de la table `rc_cars`
--

INSERT INTO `rc_cars` (`car_id`, `driver_identity`, `position_id`, `car_name`, `car_description`, `car_photos`, `car_status`, `car_level`, `car_price`, `car_estimated_price`) VALUES('AME197974CD', 123456, NULL, 'porsche cayenne', '300 ch', '', 'available', NULL, '0.5€ / min', NULL);