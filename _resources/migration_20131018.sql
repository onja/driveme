-- ********************* TABLE UPDATE 2013/10/18 **********************
-- -------- rc_drivers ---------------------------------------
ALTER TABLE rc_drivers ADD COLUMN notification_id VARCHAR(55) NULL;
-- -------- rc_users ---------------------------------------
ALTER TABLE rc_users ADD COLUMN code_pin VARCHAR(10) NULL;