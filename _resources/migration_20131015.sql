-- ************************ TABLE UPDATE 2013/10/15 ****************************

-- RC_USERS-------------------------------

DROP TABLE IF EXISTS `rc_users`;
CREATE TABLE IF NOT EXISTS `rc_users`(
	`user_id` VARCHAR(255) NOT NULL,
	`user_type` VARCHAR(20) NULL,
	`user_status` VARCHAR(20) NULL DEFAULT 'available',
	`user_priority` VARCHAR(20) NULL,
	`subscription_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	`subscription_code` VARCHAR(20) NULL,
	`device_id` INT NOT NULL,
	`session_id` VARCHAR(64) NULL,
	`credit_id` INT  NULL,
	`account_mvt_id` INT NULL,
	PRIMARY KEY(`user_id`)

) ENGINE = InnoDB;

-- RC_POSITIONS --------------------------------

DROP TABLE IF EXISTS `rc_positions`;
CREATE TABLE IF NOT EXISTS `rc_positions`(
	`position_id` INT NOT NULL AUTO_INCREMENT,
	`positionable_type` VARCHAR(20) NULL,
	`positionable_id` VARCHAR(255) NULL,
	`created_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` TIMESTAMP NULL,
	`longitude` DECIMAL(18,12),
	`latitude` DECIMAL(18,12),
	`address` TEXT,
	`eta` INT(10),
	PRIMARY KEY(`position_id`)
) ENGINE = InnoDB;


-- RC_RESERVATIONS -------------------------------------

DROP TABLE IF EXISTS `rc_reservations` ;
CREATE  TABLE IF NOT EXISTS `rc_reservations` (
  `reservation_id` INT NOT NULL AUTO_INCREMENT,
  `reservation_status` VARCHAR(20) NULL DEFAULT 'tmp',
	`reservation_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	`start_position_id` INT NULL,
	`end_position_id` INT NULL,
	`ride_id` INT NULL,
	`driver_identity` VARCHAR(20) NULL,
  PRIMARY KEY (`reservation_id`)
)
ENGINE = InnoDB;