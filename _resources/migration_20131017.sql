-- ********************* TABLE UPDATE 2013/10/17 **********************

-- --------- rc_reservations -------------------------------
ALTER TABLE rc_reservations DROP COLUMN start_position_id; 
ALTER TABLE rc_reservations DROP COLUMN end_position_id;

-- -------- rc_rides ---------------------------------------
ALTER TABLE rc_rides ADD COLUMN user_id VARCHAR(255) NULL;

UPDATE rc_rides as r SET r.user_id = (SELECT  u.user_id FROM rc_users as u WHERE u.subscription_code = r.subscription_code LIMIT 0, 1)