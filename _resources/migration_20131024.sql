ALTER TABLE rc_users DROP COLUMN credit_id;
ALTER TABLE rc_users DROP COLUMN account_mvt_id;
ALTER TABLE rc_credits ADD COLUMN user_id VARCHAR(255) NULL;
ALTER TABLE rc_account_movements ADD COLUMN credit_id INT(11) NULL;
ALTER TABLE rc_credits ADD COLUMN created_at TIMESTAMP;
ALTER TABLE rc_credits ADD COLUMN updated_at TIMESTAMP;
ALTER TABLE rc_account_movements ADD COLUMN created_at TIMESTAMP;
ALTER TABLE rc_account_movements ADD COLUMN updated_at TIMESTAMP;
