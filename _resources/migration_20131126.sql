ALTER TABLE rc_users CHANGE COLUMN user_name user_firstname VARCHAR (50);
ALTER TABLE rc_users ADD COLUMN user_lastname VARCHAR (50);
ALTER TABLE rc_users ADD COLUMN user_gender VARCHAR (10);
ALTER TABLE rc_users ADD COLUMN user_country VARCHAR (30);
ALTER TABLE rc_users ADD COLUMN user_photo VARCHAR (100);

