-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le : Lun 14 Octobre 2013 à 15:44
-- Version du serveur: 5.5.31
-- Version de PHP: 5.3.10-1ubuntu3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `driveme`
--

-- --------------------------------------------------------

--
-- Structure de la table `rc_drivers`
--
DROP TABLE IF EXISTS `rc_drivers`;
CREATE TABLE IF NOT EXISTS `rc_drivers` (
  `driver_id` int(11) NOT NULL AUTO_INCREMENT,
  `driver_name` varchar(55) NOT NULL,
  `driver_identity` varchar(20) DEFAULT NULL,
  `driver_phone` varchar(20) DEFAULT NULL,
  `driver_email` varchar(255) DEFAULT NULL,
  `driver_photos` varchar(20) DEFAULT NULL,
  `code_pin` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`driver_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Contenu de la table `rc_drivers`
--

INSERT INTO `rc_drivers` (`driver_id`, `driver_name`, `driver_identity`, `driver_phone`, `driver_email`, `driver_photos`, `code_pin`) VALUES
(1, 'christophe THIEBAUT', NULL, '0621872652', 'chris.313@hotmail.fr', NULL, NULL),
(2, 'Laurent PADEL', NULL, '0699536868', 'cultureconsulting@gmail.com; parisroyallimousine@gmail.com', NULL, NULL),
(3, 'Yacine MEGHELLI', NULL, '0760562577', 'yacine.meghelli@gmail.com', NULL, NULL),
(4, 'Lahcen AMANOUZE', NULL, '0781328227', 'infocergy@yahoo.fr', NULL, NULL),
(5, 'Pierre ZIMMERMANN', NULL, '0671210763', 'pzim@free.fr', NULL, NULL),
(6, 'Yassine SOLTANI', NULL, '0635580142', 'yassyne95@hotmail.fr', NULL, NULL),
(7, 'Alexandre CORREIA', NULL, '0633301400', 'alexcorreia.dc@gmail.com', NULL, NULL),
(8, 'Doniphan, François GUILLARD', NULL, '0612773689', 'doni-60@hotmail.fr', NULL, NULL),
(9, 'Franck FERNANDEZ', NULL, '0634296475', 'franck.fernandez75014@gmail.com', NULL, NULL),
(10, 'Arnaud Du JONCHAY', NULL, '0660777105', 'dejonchay@yahoo.fr', NULL, NULL),
(11, 'Florian ROGRON', NULL, '0625087878', 'florian2727@hotmail.fr', NULL, NULL),
(12, 'Willy BERMONT', NULL, '0669443653', 'willybermont@gmail.com', NULL, NULL),
(13, 'Ahmed Alain  BENSALEM', NULL, '0680133492', 'aabensalem@gmail.com', NULL, NULL),
(14, 'driveme test', '123456', '0621872655', 'driveme@test.com', NULL, '1111');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
