-- -----------------------------------------------------
-- Table `grids`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `grids` ;
CREATE  TABLE IF NOT EXISTS `grids` (
  `grid_id` INT NOT NULL ,
  `grid_name` VARCHAR(55) NULL ,
  PRIMARY KEY (`grid_id`)
) ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `grid_fields`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `grid_fields` ;
CREATE  TABLE IF NOT EXISTS `grid_fields` (
  `grid_field_id` INT NOT NULL ,
  `driver_type_name` VARCHAR(45) NULL ,
  `grid_id` INT NOT NULL ,
  PRIMARY KEY (`grid_field_id`)
) ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `driver_types`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `driver_types` ;
CREATE  TABLE IF NOT EXISTS `driver_types` (
  `driver_type_id` INT NOT NULL ,
  `driver_type_name` VARCHAR(45) NULL ,
  PRIMARY KEY (`driver_type_id`) 
) ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `grid_values`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `grid_values` ;
CREATE  TABLE IF NOT EXISTS `grid_values` (
  `grid_value_id` INT NOT NULL ,
  `grid_value_value` VARCHAR(45) NULL ,
  `grid_field_id` INT NOT NULL ,
  `driver_type_id` INT NOT NULL,
  PRIMARY KEY (`grid_value_id`)
) ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `grid_options`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `grid_options` ;
CREATE  TABLE IF NOT EXISTS `grid_options` (
  `grid_option_id` INT NOT NULL ,
  `grid_option_name` VARCHAR(45) NULL ,
  PRIMARY KEY (`grid_option_id`) 
) ENGINE = InnoDB;

INSERT INTO `grids` VALUES ('', 'Normale');
INSERT INTO `grids` VALUES ('', 'course au forfait nuit');
INSERT INTO `grids` VALUES ('', 'course au forfait jour');
INSERT INTO `grids` VALUES ('', 'transfert aeroport');
INSERT INTO `grids` VALUES ('', 'forfait temps');
INSERT INTO `grids` VALUES ('', 'Parrainage');
INSERT INTO `grids` VALUES ('', 'parrainage entreprise');
INSERT INTO `grids` VALUES ('', 'abonement mois');

INSERT INTO `grid_fields` VALUES ('', 'jour 6H30/21H30', 1);
INSERT INTO `grid_fields` VALUES ('', 'nuit 21H30/6H30', 1);
INSERT INTO `grid_fields` VALUES ('', '0 A 2KM PARIS', 2);
INSERT INTO `grid_fields` VALUES ('', '2KM A 5 KM PARIS', 2);
INSERT INTO `grid_fields` VALUES ('', '5KM A  10KM PARIS', 2);
INSERT INTO `grid_fields` VALUES ('', 'PARIS centre/banlieue -10km', 2);
INSERT INTO `grid_fields` VALUES ('', 'PARIS centre/banlieue-15KM', 2);
INSERT INTO `grid_fields` VALUES ('', 'PARIS centre/banlieue-30KM', 2);
INSERT INTO `grid_fields` VALUES ('', '1 A 3KM PARIS', 3);
INSERT INTO `grid_fields` VALUES ('', '2KM A 5 KM PARIS', 3);
INSERT INTO `grid_fields` VALUES ('', '5KM A  10KM PARIS', 3);
INSERT INTO `grid_fields` VALUES ('', 'PARIS centre/banlieue -10km', 3);
INSERT INTO `grid_fields` VALUES ('', 'PARIS centre/banlieue-15KM', 3);
INSERT INTO `grid_fields` VALUES ('', 'PARIS centre/banlieue-30KM', 3);
INSERT INTO `grid_fields` VALUES ('', 'paris/roissy', 4);
INSERT INTO `grid_fields` VALUES ('', 'roissy/paris', 4);
INSERT INTO `grid_fields` VALUES ('', 'ORLY/PARIS', 4);
INSERT INTO `grid_fields` VALUES ('', 'PARIS/ORLY', 4);
INSERT INTO `grid_fields` VALUES ('', 'ACHETE', 5);
INSERT INTO `grid_fields` VALUES ('', 'CREDITE', 5);
INSERT INTO `grid_fields` VALUES ('', 'Montant', 5);
INSERT INTO `grid_fields` VALUES ('', '1ère course validée', 6);
INSERT INTO `grid_fields` VALUES ('', '10ème', 6);
INSERT INTO `grid_fields` VALUES ('', '100ème', 6);
INSERT INTO `grid_fields` VALUES ('', '1000ème', 6);
INSERT INTO `grid_fields` VALUES ('', 'forfait 500 min', 7);
INSERT INTO `grid_fields` VALUES ('', 'forfait 1000 min', 7);
INSERT INTO `grid_fields` VALUES ('', 'forfait 3000 min', 7);
INSERT INTO `grid_fields` VALUES ('', 'forfait 50 min', 8);
INSERT INTO `grid_fields` VALUES ('', 'forfait 100 min', 8);
INSERT INTO `grid_fields` VALUES ('', 'forfait 200 min', 8);