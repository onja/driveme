<?php
require 'webview.php';
session_start();

//------------ 
if (empty($_GET['session_id'])) {
    header('Location: page404.php');
    exit();
}

$oUser = User::find_by_session_id($_GET['session_id']);
$oUserCredit = Credit::find_by_user_id($oUser->user_id);
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Profil</title>
        <link rel="stylesheet" href="../css/libs/style.css" type="text/css" />
    </head>
    <body>
        <div data-role="page" class="type-home">
            <div data-role="header"> 

                <div>
                    <h1 style="display: inline; float: left;">Infos Profil</h1> 

                </div>
                <?php if ($oUser) { ?>               

                    <div class="clearfix"></div>
                    <div>
                        <span id = "image_container">
                            <img src= "../html_images/avatar.png">
                            <img style = "margin-left: 15px;" src= "../html_images/gender.png">
                        </span>	 	
                        <span id = "cb_container">
                            <span class="myHeader">Crédit minute: </span></br>
                            <span>
                                <?php
                                echo $oUserCredit->credit_amount;
                                ?> min. 
                            </span>
                        </span>
                    </div>
                    <div class="clearfix"></div>
                </div> 
                <div data-role="content">
                    <ul data-role="listview" data-inset="true" data-theme="c" data-dividertheme="a">
                        <li>
                            <h2 class="myHeader">Nom: </h2>
                            <p >
                                <span>
                                    <?php
                                    echo $oUser->user_name;
                                    ?>
                                </span>
                            </p>
                        </li>
                        <li>
                            <h2 class="myHeader">Tél: </h2>
                            <p >
                                <span>
                                    <?php
                                    echo $oUser->user_phone;
                                    ?>
                                </span>
                            </p>
                        </li>
                        <li>
                            <h2 class="myHeader">Email: </h2>
                            <p >
                                <span>
                                    <?php
                                    echo $oUser->user_email;
                                    ?>
                                </span>
                            </p>
                        </li>
                        <li>
                            <h2 class="myHeader">Compte Société: </h2>
                            <p >
                                <span>
                                    <?php
                                    echo $oUser->subscription_code;
                                    ?>
                                </span>
                            </p>
                        </li>
                    <?php } else { ?>
                        <div class="clearfix"></div>
                        <div>
                            <span id = "image_container">
                                <img src= "../html_images/avatar.png">
                                <img style = "margin-left: 15px;" src= "../html_images/gender.png">
                            </span>	 	
                            <span id = "cb_container">
                                <span class="myHeader">Crédit minute: </span></br>
                                <span>

                                </span>
                            </span>
                        </div>
                        <div class="clearfix"></div>
                </div> 

                <div data-role="content">
                    <ul data-role="listview" data-inset="true" data-theme="c" data-dividertheme="a">
                        <li>
                            <h2 class="myHeader">Nom: </h2>
                            <p >
                                <span>
                                </span>
                            </p>
                        </li>
                        <li>
                            <h2 class="myHeader">Tél: </h2>
                            <p >
                                <span>
                                </span>
                            </p>
                        </li>
                        <li>
                            <h2 class="myHeader">Email: </h2>
                            <p >
                                <span>

                                </span>
                            </p>
                        </li>
                        <li>
                            <h2 class="myHeader">Compte Société: </h2>
                            <p >
                                <span>
                                </span>
                            </p>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <div id="footer">
                <h2 class="myHeader">Partager </h2>
                <p >
                    <span>Facebook </span> <br/>
                    <span>Tweeter </span> <br/>
                    <span>Email </span> <br/>
                </p>
                <div>


                </div>
    </body>
</html>
