<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Cards</title>
        <link rel="stylesheet" href="../css/libs/style.css" type="text/css" />
    </head>
    <body>
        <div data-role="page" class="type-home">
            <div data-role="header">

                <h1>Liste cartes crédit</h1> 
            </div> 
            <div data-role="content">
                <ul data-role="listview" data-inset="true" data-theme="c" data-dividertheme="a">
                    <li>
                        <a href="#"> 
                            <h2 class="myHeader">Carte #01 </h2>
                            <p >
                                <span>Perso 1 </span>
                                <span><img src=""></span>						
                            </p>
                        </a></li>
                    <li>
                        <a href="#"> 
                            <h2 class="myHeader"> Carte #02 </h2>
                            <p >
                                <span>Perso 2 </span>
                                <span><img src=""></span>						
                            </p>
                        </a></li>
                    <li>
                        <a href="#"> 
                            <h2 class="myHeader"> Carte #03 </h2>
                            <p >
                                <span>Société </span>
                                <span><img src=""></span>						
                            </p>

                </ul>
            </div>
        </div>
    </body>
</html>
