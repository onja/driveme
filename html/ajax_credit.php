<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Achat de minutes</title>
        <link rel="stylesheet" href="../css/libs/webview.css" type="text/css" />
        <script type="text/javascript" src="../js/libs/jquery-2.0.1.min.js"></script>
    </head>
    <body>
        <div data-role="page" class="type-home">
            <div data-role="header" class="header"> 
                <h1 class="title">Achat de minutes</h1>
            </div> 
            <div data-role="content" class="content">
                <form id = "crdForm" action ="" method = "post" >
                    <center>
                        <div style="margin-left: auto; margin-right: auto;width: 320px; text-align: left;">
                            <span style="display:inline;">	
                                <input onclick="check_value(this)" type="radio" class="crd_radio" id="crd1" name="crd_amount" style="padding-top:15px;" value ="30_28.00"><label for ="crd1" class ="sp-button button-grey">+ 30 min.</label><span style="color:#fff; font-size:20px">28.00€ TTC</span><br/><br/>
                            </span>
                            <span style="display:inline;">		
                                <input onclick="check_value(this)" type="radio" class="crd_radio" id="crd2" name="crd_amount" style="padding-top:15px;" value="60_57.40"><label for ="crd2" class ="sp-button button-grey">+ 60 min.</label><span style="color:#fff; font-size:20px">57.40€ TTC</span><br/><br/>
                            </span>
                            <span style="display:inline;">	
                                <input onclick="check_value(this)" type="radio" class="crd_radio" id="crd3" name="crd_amount" style="padding-top:15px;" value="100_96.00"><label for ="crd3" class ="sp-button button-grey">+ 100 min.</label><span style="color:#fff; font-size:20px">96.00€ TTC</span><br/><br/>
                            </span>
                        </div>
                        <div id="info-box">
                            <p>Total achat (dont 20% offerts) : 72 min.</p>
                            <h1 class="total_amount" style="font-size: 30px;font-family: Helvetica, Arial, sans-serif;">57,40 €</h1>
                            <p>Votre crédit minute: 128 min.</p>
                        </div>
                        <button type="submit" class ="button-red">Valider</button><br/>

                    </center>
                </form>
            </div>
            <!--<div data-role="footer" class="footer">
            </div>-->
        </div>
    </body>
</html>
