<?php
// On récupère la date au format ISO-8601
$dateTime = date("c");
// On crée la chaîne à hacher sans URLencodage

/* $tParams = array(
  'PBX_SITE' => '1999888',
  'PBX_RANG' => '85',
  'PBX_IDENTIFIANT' => '110647233',
  'PBX_TOTAL' => '1000',
  'PBX_DEVISE' => '978',
  'PBX_CMD' => 'CMD driveme',
  'PBX_PORTEUR' => 'onja.rails@gmail.com',
  'PBX_RETOUR' => 'Mt:M;Ref:R;Auto:A;Erreur:E',
  'PBX_HASH' => 'SHA512',
  'PBX_TIME' => date('Y-m-d h:i:s'),
  'PBX_HMAC' => '0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF'
  ); */

$msg = "PBX_SITE=1999888" .
        "&PBX_RANG=85" .
        "&PBX_IDENTIFIANT=110647233" .
        "&PBX_TOTAL=1000" .
        "&PBX_DEVISE=978" .
        "&PBX_CMD=CMD_DRIVEME001" .
        "&PBX_PORTEUR=onja@asareo.fr" .
        "&PBX_RETOUR=Mt:M;Ref:R;Auto:A;Erreur:E" .
        "&PBX_HASH=SHA512" .
        "&PBX_TIME=" . date('Y-m-d h:i:s');

$keyTest = '0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF';
// Si la clé est en ASCII, On la transforme en binaire
$binKey = pack("H*", $keyTest);

//print_r(hash_algos());
$hmac = strtoupper(hash_hmac('sha512', $msg, $binKey));
// La chaîne sera envoyée en majuscules, d'où l'utilisation de strtoupper()
// On crée le formulaire à envoyer à Paybox System
// ATTENTION : l'ordre des champs est extrêmement important, il doit
// correspondre exactement à l'ordre des champs dans la chaîne hachée
?>
<form method="POST" action="https://preprod-tpeweb.paybox.com/cgi/ChoixPaiementMobile.cgi">
    <input type="hidden" name="PBX_SITE" value="1999888">
    <input type="hidden" name="PBX_RANG" value="85">
    <input type="hidden" name="PBX_IDENTIFIANT" value="110647233">
    <input type="hidden" name="PBX_TOTAL" value="1000">
    <input type="hidden" name="PBX_DEVISE" value="978">
    <input type="hidden" name="PBX_CMD" value="CMD_DRIVEME001">
    <input type="hidden" name="PBX_PORTEUR" value="onja@asareo.fr">
    <input type="hidden" name="PBX_RETOUR" value="Mt:M;Ref:R;Auto:A;Erreur:E">
    <input type="hidden" name="PBX_HASH" value="SHA512">
    <input type="hidden" name="PBX_TIME" value="<? echo date('Y-m-d h:i:s'); ?>">
    <input type="hidden" name="PBX_HMAC" value="<? echo $hmac; ?>">
    <input type="submit" value="Valider">
</form>
