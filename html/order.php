<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');
require 'webview.php';

if(empty($_GET['target_url']) || empty($_GET['session_id'])){
  header('Location: page404.php');
}

$oUser = User::find_by_session_id($_GET['session_id']);
$zActionUrl = $_GET['target_url'];     

?>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Commande</title>
        <link rel="stylesheet" href="../css/libs/webview.css" type="text/css" />
    </head>
    <body>
        <div data-role="page" class="type-home">
            <div data-role="header" class="header"> 
                <h1 class="title">Commande</h1>
            </div> 
            <div data-role="content" class="content">
                <form id = "commandForm" action ="<?php echo $zActionUrl; ?>" method = "post" >
                    <input type="hidden" name="payment[payment_type]" id="payment_payment_type" value="credit-card">
                    <p>
                        <input id="card_title" name="card[title]" class="text" placeholder="Titre de la carte"/>
                    </p>
                    <p>
                        <label for="porteur">N° de la Carte Bleue :</label>
                        <input id="card_porteur" name="card[porteur]" class="text" placeholder="XXX XXX XXX 536"/>
                    </p>
                    <p style="float:right;">
                        <label for="cvv">Code de sécurité :</label>
                        <input id="card_cvv" name="card[cvv]" class ="right-text" placeholder="XXX" />
                    </p><br/>
                    <p>
                        <label for="owner">Titulaire :</label>
                        <input id="card_owner" name="card[owner]" class="text" placeholder="Jean Jacques Goldman"/>
                    </p>
                    <p style="float:right;">
                        <label for="dateval">Expire le :</label>
                        <input id="card_dateval" name="card[dateval]" class ="right-text" placeholder="XX/XX"/>
                    </p><br/><br/>

                    <p>
                        <input id="card_save_card" name="card[save_card]" type="checkbox" value="1" />
                        <label for="save_card">Memoriser ma carte</label>
                    </p>
                    <p>
                        <input id="acceptCgv" name="acceptCgv" type="checkbox"  />
                        <label for="acceptCgv">J'accepte les CGV</label>
                    </p>
                    <p>
                    <center>
                        <button type="submit" class ="button-red">Valider</button><br/>
                        <p style="font-size:11px">Vous serez prélevé à la fin de la course</p>
                    </center>
                    </p>

                </form>
            </div>
            <!--		<div data-role="footer" class="footer">
                            </div>-->
        </div>
    </body>
</html>
