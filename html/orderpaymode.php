<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
    require 'webview.php';
    if (!$_GET['session_id']) {
        header('Location: page404.php');
        exit();
    }
    if (!isset($_GET["ride_id"])) {
        header('Location: declined.php');
        exit();
    }
    //post paymode
    if (isset($_POST["payment_mode"])) {
        $selected = $_POST["payment_mode"];
        $oUser = User::find_by_session_id($_GET['session_id']);
        $target_url = urlencode('/payment/ride/check/' . $_GET['ride_id'] . '/' . $_GET['session_id']);
        if ($selected == "crd_minute") {
         //debit credit minute
            try {
                if (!$oUser->credit) header('Location: declined.php');
                $oRide = Ride::find($_GET['ride_id']);
                $fCredit = $oUser->credit->debite(floatval($oRide->ride_estimated_time));
                header('Location: approved.php');
            } catch (Exception $e) {
                header('Location: declined.php');
            }
        } elseif ($selected == "blue_card") {
            $hasCard = Card::find_by_user_id($oUser->user_id);
            if (is_object($hasCard)) {
                header('Location: orderpaytype.php?target_url=' . $target_url . '&session_id=' . $_GET['session_id']);
            } else {
                header('Location: order.php?target_url=' . $target_url . '&session_id=' . $_GET['session_id']);
            }
        }
    }
?>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Commande</title>
        <link rel="stylesheet" href="../css/libs/webview.css" type="text/css" />
    </head>
    <body>
        <div data-role="page" class="type-home">
            <div data-role="header" class="header"> 
                <h1 class="title">Commande</h1>
            </div> 
            <div data-role="content" class="content">
                <h2 style=" padding-top:15px;text-align:center; color: white;text-shadow: 0px -1px 0px #000;font-weight: normal;font-size: 18px;font-family: Helvetica, Arial, sans-serif;">
                    Mode paiement
                </h2>
                <form id = "commandForm" action ="" method = "post" >
                    <center>	
                        <input type="radio" id="crd_minute" name="payment_mode" value="crd_minute">
                        <label for="crd_minute" style="padding-top:15px;" class ="p-button button-grey">Crédit minute (56min.) </label><br/>
                        <input type="checkbox" name="company_account">
                        <label style="color:#fff; font: 0.75em 'Trebuchet MS',Arial, Helvetica;"  for="companyaccount">Compte Société</label><br/>
                        <p style="color: #0080ff; font-size:15px">Acheter des minutes</p><br/><br/>
                        <input type="radio" id="blue_card" name="payment_mode" value="blue_card">
                        <label for="blue_card" style="padding-top:15px;" class ="p-button button-grey">Carte Bleue </label><br/><br/><br/><br/><br/>
                        <button type="submit" class ="button-red">Commander</button><br/>
                        <p style="color:#fff; font-size:11px">Vous serez prélevé à la fin de la course</p>
                    </center>					
                </form>
            </div>
            <!--<div data-role="footer" class="footer">
            </div>-->
        </div>
    </body>
</html>
