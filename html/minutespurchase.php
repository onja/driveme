<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Achat de minutes</title>
        <link rel="stylesheet" href="../css/libs/webview.css" type="text/css" />
    </head>
    <body>
        <div data-role="page" class="type-home">
            <div data-role="header" class="header"> 
                <h1 class="title">Achat de minutes</h1>
            </div> 
            <div data-role="content" class="content">
                <h2 class ="content-title">Montant total: 57,40 €</h2>
                <hr/>
                <h2 style="margin-left:10px; color: white;text-shadow: 0px -1px 0px #000;font-weight: normal;font-size: 18px;font-family: Helvetica, Arial, sans-serif;">
                    Payer avec
                </h2>
                <form id = "commandForm" action ="" method = "post" >
                    <input type="radio" id="check1" name="checks" value="all" checked>
                    <label for="check1" class="list">Carte Perso</label>
                    <input type="radio" id="check2" name="checks"value="false">
                    <label for="check2" class="list">Carte PRO</label>
                    <input type="radio" id="check3" name="checks" value="true">
                    <label for="check3" class="list">Carte Maison</label>
                    <p>
                    <center>
                        <p style="color: #0080ff; font-size:15px">Ajouter carte</p><br/><br/><br/>
                        <button type="submit" class ="button-red">Valider</button><br/>

                    </center>
                    </p>

                </form>
            </div>
            <!--<div data-role="footer" class="footer">
            </div>-->
        </div>
    </body>
</html>
