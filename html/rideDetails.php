<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Detail course</title>
        <link rel="stylesheet" href="../css/libs/style.css" type="text/css" />
    </head>
    <body>
        <div data-role="page" class="type-home">
            <div data-role="header">

                <h1>Détail trajet</h1> 
            </div> 
            <div data-role="content">
                <ul data-role="listview" data-inset="true" data-theme="c" data-dividertheme="a">
                    <li>
                        <h2 class="myHeader" style="display:inline;"><span >Le:</span> <span style="float:right; margin-right:15px;">Par: Alfred</span></h2>
                        <p >
                            <span>Lundi 17 Sept 2013 </span>
                            <span><img src=""></span>						
                        </p>
                        <h2 style ="font-size: 12px;top: 5%; text-align:center;"> Porche Cayenne S Diesel 302 ch</h2>
                    </li>
                    <li>

                        <h2 class="myHeader" style="display:inline;"><span >De:</span> <span style="float:right; margin-right:15px;">Départ: 17h 24</span></h2>
                        <p >
                            <span>17, rue broca 75004 Paris </span>
                            <span><img src=""></span>						
                        </p>
                    </li>
                    <li>
                        <h2 class="myHeader" style="display:inline;"><span >A:</span> <span style="float:right; margin-right:15px;">Arrivée: 17h 33</span></h2>
                        <p >
                            <span>5, rue Dante 75005 Paris </span>
                            <span><img src=""></span>						
                        </p>
                    </li>

                </ul>
                <div id="footer">
                    <h2 style="display:inline;"><span class="myHeader" >Trajet de:</span> <span style="float:right; margin-right:15px; font-size:17px;">4.5km - 9min.</span></h2>
                    <h2 style="text-align:center"> 7,20 €</h2>
                    <div>

                    </div>
                </div>
    </body>
</html>
