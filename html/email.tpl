<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Facture</title>
        <link rel="stylesheet" href="../css/libs/style.css" type="text/css" />
    </head>
    <body>
      <div marginwidth="0" marginheight="0">                    
          <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#f2f3f5">
            <tbody>
            	<tr>
            		<td valign="top">
                  <table width="650" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#F9F9F9">
                  	<tbody>
                  		<tr>
                  			<td bgcolor="#F9F9F9">
                          <table width="650" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#F9F9F9">
                          	<tbody>
                          		<tr>
                          			<td bgcolor="#F9F9F9">
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#f2f3f5">
                                  	<tbody>
                                  		<tr>
                                  			<td valign="top" align="center" bgcolor="#f2f3f5">
                                  				<table width="630" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#f2f3f5" style="border-collapse:collapse">
                                            <tbody>
                                            	<tr>
                                            		<!--td valign="top" align="center" bgcolor="#f2f3f5" style="font-family:Helvetica,Arial,sans-serif;font-size:11px;color:#868a8d;padding:20px 0 20px 0;line-height:20px;font-weight:200;text-align:center;vertical-align:top">Des problèmes pour lire ce message? <a href="http://www.drive.gt/user/download/1233/?type=reservation" style="color:#2cc5cb" target="_blank">Cliquez-ici pour le télécharger en ligne</a></td-->
                                              </tr>
                                            </tbody>
                                          </table>
                                        </td>
                                      </tr>
                                      <tr>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <table width="100%" align="center" height="93" border="0" cellspacing="0" cellpadding="0" bgcolor="#211f29" style="border-collapse:collapse">
                                  	<tbody>
                                  		<tr>
                                  			<td height="93" align="left" valign="top" width="650" style="border-collapse:collapse" bgcolor="#211f29">
                                  				<table width="650" align="center" height="93" border="0" cellspacing="0" cellpadding="0" bgcolor="#211f29" style="border-collapse:collapse">
	                                          <tbody>
	                                          	<tr>
	                                          		<td height="93" align="left" valign="top" width="630" style="border-collapse:collapse" bgcolor="#211f29">
	                                          			<table border="0" cellspacing="0" cellpadding="0" align="left" width="560" style="border-collapse:collapse" bgcolor="#F9F9F9">
	                                          				<tbody>
	                                          					<tr>
	                                          						<td style="border-collapse:collapse" bgcolor="#211f29" border="0"><a href="#141a7c8d254ce6d0_1415bc1c80797135_"><img alt="Driveme PARIS" width="215" height="93"></a></td>
	                                          					</tr>
	                                                  </tbody>
	                                               	</table>
	                                                <table border="0" cellspacing="0" cellpadding="0" width="70" style="border-collapse:collapse" bgcolor="#F9F9F9">
	                                                	<tbody>
	                                                		<tr>
	                                                  		<td height="30" colspan="2" style="border-collapse:collapse" bgcolor="#211f29"></td>
	                                                  	</tr>
	                                                  	<tr>
	                                                  		<td width="40" height="30" align="left" style="border-collapse:collapse" bgcolor="#211f29">
	                                                  			<a href="https://www.facebook.com/driveparis" target="_blank"><img alt="Facebook" width="30" height="30" border="0"></a>
	                                                  		</td>
	                                                  		<td width="30" height="30" align="left" style="border-collapse:collapse" bgcolor="#211f29"><a href="https://twitter.com/Drive_Paris" target="_blank">
	                                                  			<img alt="Twitter" width="30" height="30" border="0"></a>
	                                                  		</td>
	                                                  	</tr>
	                                                  	<tr>
	                                                  		<td height="20" colspan="5" style="border-collapse:collapse" bgcolor="#211f29"></td>
	                                                    </tr>
	                                                  </tbody>
	                                                </table>
	                                              </td>
	                                            </tr>
	                                          </tbody>
                                        	</table>
                                      	</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <table width="630" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#F9F9F9">
                                  	<tbody>
                                  		<tr>
                                  			<td height="30" align="left" valign="middle">
                                  			</td>
                                  		</tr>
                                  	</tbody>
                                  </table>
                                  <table width="630" border="0" cellspacing="0" cellpadding="0" align="center">
                                  	<tbody>
                                  		<tr>
                                  			<td height="15" bgcolor="#F9F9F9"></td>
																			</tr>
																			<tr>
																				<td valign="top" align="left" width="50%" style="font-family:Helvetica,Arial,sans-serif;color:#211f29;font-size:32px;font-weight:200;text-align:left;vertical-align:top;letter-spacing:-0.04em">Votre <span class="il">facture</span> 
																				</td>
																				<td valign="top" align="right" width="50%" style="font-family:Helvetica,Arial,sans-serif;color:#bebfc1;font-size:22px;font-weight:200;text-align:right;vertical-align:top;letter-spacing:-0.04em">#RG14B1380223742</td>
																			</tr>
																			<tr>
																				<td height="15" bgcolor="#F9F9F9" colspan="2"></td>
																			</tr>
                                    </tbody>
                                  </table>
                                  <table width="630" cellspacing="0" cellpadding="0" border="0" bgcolor="#F9F9F9" align="center">
                                  	<tr>
                                  		<td>
                                  			<table width="280" cellspacing="0" cellpadding="0" border="0" bgcolor="#F9F9F9" align="right">
                                  				<tbody>
                                  					<tr>
                                  						<td width="280" valign="top" align="right"><a href="#141a7c8d254ce6d0_1415bc1c80797135_"><img width="268" height="268" border="0" style="display:block;border:none;outline:none;text-decoration:none;background-color:#f2f3f5;padding:6px" alt="ImageMap"></a></td>
                                  					</tr>
                                  				</tbody>
                                  			</table>
                                  			<table width="320" cellspacing="0" cellpadding="0" border="0" bgcolor="#F9F9F9" align="left">
                                  				<tbody>
                                  					<tr>
                                  						<td width="320" valign="top" align="left" style="font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:200;color:#808080;line-height:24px;text-align:left;vertical-align:top">
                                  							<p><span class="il">Facturé</span> à : <b style="color:#211f29">{$oUser.user_name} - <a href="mailto:gregoire@fournel.net" target="_blank">{$oUser.user_email}</a></b></p>
                                  							<p>Date de la commande : <b style="color:#211f29">{$oRide.ride_real_time|date_format: "%d/%m/%Y"} à {$oRide.ride_real_time|date_format: "%H:%M"}</b></p>
                                  							<p>Adresse de prise en charge : <b style="color:#211f29">{$oStartPosition.address}</b></p>
                                  							<p>Destination : <b style="color:#211f29">{$oEndPosition.address}</b></p>
                                  						</td>
                                  					</tr>
                                  				</tbody>
                                  			</table>
                                  		</td>
                                  	</tr>
                                  </tbody>
                                	</table>
	                                <table width="630" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#F9F9F9">
	                                	<tbody>
	                                		<tr>
	                                			<td height="20" align="left" valign="middle"></td>
	                                		</tr>
	                                	</tbody>
	                                </table>
	                                <table width="630" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#F9F9F9">
	                                	<tbody>
	                                		<tr>
	                                			<td height="30" align="left" valign="middle"><img width="630" height="30" alt="rule"></td>
	                                		</tr>
	                                	</tbody>
	                                </table>
	                                <table width="630" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#F9F9F9">
	                                	<tbody>
	                                		<tr>
	                                			<td height="20" align="left" valign="middle"></td>
	                                		</tr>
	                                	</tbody>
	                                </table>
	                                <table width="630" border="0" cellspacing="0" cellpadding="0" align="center">
	                                	<tbody>
	                                		<tr>
	                                			<td width="100%" valign="top" align="left" style="font-family:Helvetica,Arial,sans-serif;font-size:11px;color:#808080;line-height:34px;font-weight:200;text-align:left;vertical-align:top"></td>
	                                		</tr>
	                                		<tr>
	                                			<td width="50%" valign="top" align="left" style="font-family:Helvetica,Arial,sans-serif;font-size:14px;color:#808080;line-height:34px;font-weight:200;text-align:left;vertical-align:top">Tarif de la course</td>
	                                			<td width="50%" valign="top" align="right" style="font-family:Helvetica,Arial,sans-serif;font-size:18px;color:#211f29;line-height:34px;font-weight:200;text-align:right;vertical-align:top"><b>{$oRide.ride_total_price|number_format:2}</b></td>
	                                		</tr>
	                                		<tr>
	                                			<td width="50%" valign="top" align="left" style="font-family:Helvetica,Arial,sans-serif;font-size:14px;color:#808080;line-height:24px;font-weight:200;text-align:left;vertical-align:top">TVA 7%</td>
	                                			<td width="50%" valign="top" align="right" style="font-family:Helvetica,Arial,sans-serif;font-size:18px;color:#211f29;line-height:24px;font-weight:200;text-align:right;vertical-align:top"><b>0.98€</b></td>
	                                		</tr>
	                                		<tr>
	                                			<td height="20" bgcolor="#F9F9F9" colspan="2"></td>
	                                		</tr>
	                                		<tr>
	                                			<td width="50%" valign="middle" align="left" style="font-family:Helvetica,Arial,sans-serif;font-size:14px;color:#808080;line-height:24px;font-weight:200;text-align:left;vertical-align:middle"><b style="font-size:20px">Total TTC</b><br></td>
	                                			<td width="50%" valign="top" align="right" style="font-family:Helvetica,Arial,sans-serif;font-size:55px;color:#2cc5cb;line-height:68px;font-weight:200;text-align:right;vertical-align:top;letter-spacing:-0.04em">15€</td>
	                                		</tr>
	                                		<tr>
	                                     <td width="50%" valign="middle" align="left" style="font-family:Helvetica,Arial,sans-serif;font-size:14px;color:#808080;line-height:24px;font-weight:200;text-align:left;vertical-align:middle"></td>
	                                    </tr>
	                                  </tbody>
	                                </table>
	                                <table width="630" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#F9F9F9">
	                                	<tbody>
	                                		<tr>
	                                			<td height="40" align="left" valign="middle"></td>
	                                		</tr>
	                                	</tbody>
	                                </table>
	                                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#FFFFFF">
	                                	<tbody>
	                                		<tr>
	                                			<td valign="top" align="center" bgcolor="#FFFFFF">
	                                				<table width="630" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#42484D" style="border-collapse:collapse">
	                                					<tbody>
	                                						<tr>
	                                							<td align="center" bgcolor="#FFFFFF" style="padding-top:20px"><img alt="bottom_logo" width="31" height="32"></td>
	                                						</tr>
	                                						<tr>
	                                							<td valign="top" align="center" bgcolor="#FFFFFF" style="font-family:Helvetica,Arial,sans-serif;font-size:11px;color:#cccccc;padding:20px 0;line-height:20px;font-weight:200;text-align:center;vertical-align:top">Driveme PARIS<br></td>
	                                						</tr>
	                                					</tbody>
	                                				</table>
	                                			</td>
	                                		</tr>
	                                		<tr>
	                                		</tr>
	                                	</tbody>
	                                </table>
                              	</td>
                            	</tr>
                          	</tbody>
                        	</table>
                      	</td>
                    	</tr>
	                    <tr>
	                    	<td bgcolor="#f2f3f5" height="40"></td>
	                    </tr>
                  	</tbody>
                	</table>
              	</td>
            	</tr>
          	</tbody>
        	</table>
        <div class="yj6qo"></div>
        <div class="adL"></div>
      </div>
	</body>
</html>