<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');
require 'webview.php';

if (!$_GET['session_id']) {
    header('Location: page404.php');
}

$zActionUrl = $_GET['target_url'];
$oUser = User::find_by_session_id($_GET['session_id']);
$toCards = $oUser->cards;
?>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Commande</title>
        <link rel="stylesheet" href="../css/libs/webview.css" type="text/css" />
    </head>
    <body>
        <div data-role="page" class="type-home">
            <div data-role="header" class="header"> 
                <h1 class="title">Commande</h1>
            </div> 
            <div data-role="content" class="content">
                <h2 style="margin-left:10px; color: white;text-shadow: 0px -1px 0px #000;font-weight: normal;font-size: 18px;font-family: Helvetica, Arial, sans-serif;">
                    Payer avec
                </h2>
                <form id = "commandForm" action ="<?php echo $zActionUrl; ?>" method = "post" >
                    <input type="hidden" name="payment[payment_type]" id="payment_payment_type" value="credit-card">

                    <?php foreach ($toCards as $key => $oCard) { ?>

                        <input type="radio" id="check<?php echo $key; ?>" name='card[card_id]' value="<?php echo $oCard->card_id; ?>" checked>
                        <label for="check<?php echo $key; ?>" class="list">check<?php echo $oCard->title; ?></label>

                    <?php } ?>

                    <p>
                    <center>
                        <a href="/html/order.php?target_url=<?php echo urlencode($_GET['target_url']); ?>&session_id=<?php echo $_GET['session_id']; ?>" style="color: #0080ff; font-size:11px">Ajouter carte</a><br/><br/><br/>
                        <button type="submit" class ="button-red">Valider</button><br/>

                    </center>
                    </p>

                </form>
            </div>
            <!--<div data-role="footer" class="footer">
            </div>-->
        </div>
    </body>
</html>
