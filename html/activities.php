<?php
require 'webview.php';
error_reporting(E_ALL);
ini_set('display_errors', 'On');
session_start();

if (empty($_GET['session_id'])) {
    header('Location: page404.php');
    exit();
}

$toHistoryRide = RideHistory::find('all', array('joins' => 'INNER JOIN rc_rides as r ON r.ride_id = rc_ride_histories.ride_id
														   INNER JOIN rc_users as u ON u.user_id = r.user_id ',
            'conditions' => array('rc_ride_histories.ride_status = ? AND u.session_id = ? ORDER BY rc_ride_histories.updated_at DESC', 'finished', $_GET['session_id'])));

//$session_id = "nwxRLZP1Wk3jH1QaBnxt9avqgQrtp9lM4728dETV74U0GVU2ARfJutyFBE8gNGe";

/* try{
  $toHistoryRide = RideHistory::find('all',array('joins' => 'INNER JOIN rc_rides as r ON r.ride_id = rc_ride_histories.ride_id
  INNER JOIN rc_users as u ON u.user_id = r.user_id ',
  'conditions' => array('rc_ride_histories.ride_status = ? AND u.session_id = ? ORDER BY rc_ride_histories.updated_at DESC','finished',$session_id)));
  }catch(Exception $e){
  echo "No ride found";
  }
 */
//print_r($toHistoryRide);die;
function count_minutes($ride_id) {
    $oHistoryRideStart = RideHistory::find('first', array('conditions' => array('ride_id =? AND ride_status =?', $ride_id, 'ride')));
    $oHistoryRideFinished = RideHistory::find('first', array('conditions' => array('ride_id =? AND ride_status =?', $ride_id, 'finished')));
    //print_r($oHistoryRideStart);die;
    $duration = date_diff($oHistoryRideStart->updated_at, $oHistoryRideFinished->updated_at);
    return $duration->format('-%i');
}
?>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Vos activités</title>
        <link rel="stylesheet" href="../css/libs/style.css" type="text/css" />
    </head>
    <body>
        <div data-role="page" class="type-home">
            <div id="header"> 

                <h1>Historique</h1> 
            </div> 
            <div data-role="content">
<?php if ($toHistoryRide) { ?>
                    <ul data-role="listview" data-inset="true" data-theme="c" data-dividertheme="a">

                    <?php
                    foreach ($toHistoryRide as $oHistoryRide) {
                        //print_r(count_minutes($oHistoryRide->ride_id));die;
                        echo ('
						<li>
							<a href="#">
								<h2 class="myHeader">' . $oHistoryRide->updated_at . '</h2>
								<p style = "display: inline; ">
									<span class ="left">Course #' . $oHistoryRide->ride->ride_id . '</span>
									<span class="center"><img src = "/html/images/company.png"/></span>						
									<span class="middle">' . count_minutes($oHistoryRide->ride_id) . ' min.</span>
									<div id="arrow-right"></div>
								</p>
							</a></li>');
                    }
                    ?>

                        <!--				<li>
                                                                <a href="#"> 
                                                                        <h2 class="myHeader"> 12 juin 2013 </h2>
                                                                        <p style = "display: inline;">
                                                                                <span class ="left">Course #423 </span>
                                                                                <span class="center"><img src = "../html_images"></span>						
                                                                                <span class="middle">-24 min</span>	
                                                                                <span id="arrow-right"></span>					
                                                                        </p>
                                                                        </a></li>
                                                        <li>
                                                                <a href="#"> 
                                                                        <h2 class="myHeader"> 09 mai 2013 </h2>
                                                                        <p style = "display: inline;">
                                                                                <span class ="left">Course #344 </span>
                                                                                <span class="center"><img src = "../html_images/company.png"></span>						
                                                                                <span class="middle">-5 min</span>
                                                                                <span id="arrow-right"></span>						
                                                                        </p>
                                                                        </a></li>
                                                        <li>
                                                                <a href="#"> 
                                                                        <h2 class="myHeader"> 24 mars 2013 </h2>
                                                                        <p style = "display: inline;">
                                                                                <span class ="left">Achat minutes </span>
                                                                                <span class="center"><img src = "../html_images"></span>						
                                                                                <span class="middle">72 min</span>	
                                                                                <span id="arrow-right"></span>					
                                                                        </p>
                                                                        </a></li>
                                                        <li>
                                                                <a href="#"> 
                                                                        <h2 class="myHeader"> 24 juillet 2012 </h2>
                                                                        <p style = "display: inline;">
                                                                                <span class ="left">Course #251 </span>
                                                                                <span class="center"><img src = "../html_images"></span>					
                                                                                <span class="middle">-9 min</span>	
                                                                                <span id="arrow-right"></span>					
                                                                        </p>
                                                                        </a></li> -->

                    </ul>
<?php } else { ?>

                    <ul data-role="listview" data-inset="true" data-theme="c" data-dividertheme="a">

                        <li>
                            <a href="#">
                                <p style = "display: inline; ">
                                    <span class ="left"></span>
                                    <span class="center"><img src = ""/></span>						
                                    <span id="arrow-right"></span>
                                </p>
                            </a></li>

                    </ul>
<?php } ?>
                <div style="display:inline;">
                    <span class="myHeader" style=" float:left;"><img src="../html_images/company.png"/> Compte Société </span>
                    <span style="float:right; display:inline; margin-right:10px"> Voir les <span style="color:#14B5E6; "><a href="cgu.php">CGV</a></span> </span>
                </div>
            </div>

        </div>
    </body>
</html>
