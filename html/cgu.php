<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CGV</title>
        <link rel="stylesheet" href="../css/libs/style.css" type="text/css" />
    </head>
    <body>
        <div data-role="page" class="type-home">
            <div data-role="header"> 
                <h1>Conditions Générales de Vente</h1> 
            </div> 
            <div data-role="content">
                <ul data-role="listview" data-inset="true" data-theme="c" data-dividertheme="a">
                    <li>

                        <h2 class="myHeader">Article 1 : Définitions </h2>
                        <p class="myParagraph">
                            <b>Abonnement: </b>
                            désigne selon le choix du Client, un Abonnement avec un engagement de 2 ans, 1 an ou mensuel.
                            Au cours d'un Abonnement, le Client bénéficie des Offres et des Services auxquels il a souscrits, 
                            en contre partie du paiement du prix aux dits Offres et Services. Un Client dispose d'autant d'Abonnements 
                            qu'il crée de Boutique(s) et/ou Vitrine(s) (soit un Abonnement par Boutique ou Vitrine)

                        </p>
                    </li>
                    <li>

                        <h2 class="myHeader">Article 2 : Définitions </h2>
                        <p class="myParagraph">
                            <b>Abonnement: </b>
                            désigne selon le choix du Client, un Abonnement avec un engagement de 2 ans, 1 an ou mensuel.
                            Au cours d'un Abonnement, le Client bénéficie des Offres et des Services auxquels il a souscrits, 
                            en contre partie du paiement du prix aux dits Offres et Services. Un Client dispose d'autant d'Abonnements 
                            qu'il crée de Boutique(s) et/ou Vitrine(s) (soit un Abonnement par Boutique ou Vitrine)
                        </p>
                    </li>

                </ul>
            </div>
        </div>
    </body>
</html>
