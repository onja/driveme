<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Achat de minutes</title>
        <link rel="stylesheet" href="../css/libs/webview.css" type="text/css" />
    </head>
    <body>
        <div data-role="page" class="type-home">
            <div data-role="header" class="header"> 
                <h1 class="title">Achat de minutes</h1>
            </div> 
            <div data-role="content" class="content">
                <form id = "commandForm" action ="" method = "post" >
                    <p>
                        <input id="cardTitle" name="cardTitle" class="text" placeholder="Titre de la carte"/>
                    </p>
                    <p>
                        <label for="cardBlueNumber">N° de la Carte Bleue :</label>
                        <input id="cardBlueNumber" name="cardBlueNumber" class="text" placeholder="XXX XXX XXX 536"/>
                    </p>
                    <p style="float:right;">
                        <label for="securityCode">Code de sécurité :</label>
                        <input id="securityCode" name="securityCode" class ="right-text" placeholder="XXX" />
                    </p><br/>
                    <p>
                        <label for="holder">Titulaire :</label>
                        <input id="holder" name="holder" class="text" placeholder="Jean Jacques Goldman"/>
                    </p>
                    <p style="float:right;">
                        <label for="expireDate">Expire le :</label>
                        <input id="expireDate" name="expireDate" class ="right-text" placeholder="XX/XX"/>
                    </p><br/><br/>

                    <p>
                        <input id="cardMemorise" name="cardMemorise" type="checkbox" />
                        <label for="cardMemorise">Memoriser ma carte</label>
                    </p>
                    <p>
                        <input id="acceptCgv" name="expireDate" type="checkbox"  />
                        <label for="acceptCgv">J'accepte les CGV</label>
                    </p>
                    <p>
                    <center>
                        <button type="submit" class ="button button-red">Valider</button><br/>
                        <p style="font-size:11px">Vous serez prélevé à la fin de la course</p>
                    </center>
                    </p>

                </form>
            </div>
            <!--		<div data-role="footer" class="footer">
                            </div>-->
        </div>
    </body>
</html>
