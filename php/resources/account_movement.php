<?php

/**
 * 
 * @uri /account_movement/:session_id
 * @uri /account_movement/:user_id/:session_id
 */
class AccountMovementResource extends Tonic\Resource {

    /**
     * @method GET
     * @provides application/json
     */
    function find($user_id = '', $session_id = "") {
        $code = Tonic\Response::OK;
        $outputObject = array();
        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        if (empty($session_id)) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing session:session_id'
            );
            $code = Tonic\Response::CONFLICT;
            return new Tonic\Response($code, json_encode($error));
        }

        if (empty($user_id)) {
            $error = array(
                'error_code' => '-2',
                'error_message' => 'Method not allowed'
            );
            $code = Tonic\Response::CONFLICT;
            return new Tonic\Response($code, json_encode($error));
        }


        if ($code == Tonic\Response::OK) {
            if (!$this->check_authentication()) {
                $error = array(
                    'error_code' => '-3',
                    'error_message' => 'Authentication failed'
                );
                $code = Tonic\Response::CONFLICT;
                return new Tonic\Response($code, json_encode($error));
            } else {

                try {
                    $oUser = User::find($user_id);
                    $oAccountMovement = AccountMovement::find($oUser->account_mvt_id); //get account_mvt by id                      
                    $outputObject['user']['user_id'] = $oUser->user_id;
                    $outputObject['user']['account_mvt']['account_mvt_op_id'] = $oAccountMovement->account_mvt_op_id;
                    $outputObject['user']['account_mvt']['account_mvt_debit'] = $oAccountMovement->account_mvt_debit;
                    $outputObject['user']['account_mvt']['account_mvt_credit'] = $oAccountMovement->account_mvt_debit;
                    $outputObject['user']['account_mvt']['account_mvt_op_type'] = $oAccountMovement->account_mvt_op_type;
                } catch (Exception $e) {
                    $error = array(
                        'error_code' => '-1',
                        'error_message' => 'Not found'
                    );
                    $code = Tonic\Response::NOTFOUND;
                }
            }
        }

        $outputObject['error'] = $error;

        $jsonBody = json_encode($outputObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method POST
     * @provides application/json
     */
    function create($session_id = "") {

        $outputObject = array();

        $code = Tonic\Response::OK;

        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        if (empty($session_id) || !$this->check_authentication()) {
            $code = Tonic\Response::CONFLICT;

            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing session:session_id ',
            );

            return new Tonic\Response($code, json_encode($error));
        }

        if (!$this->check_authentication()) {
            $code = Tonic\Response::CONFLICT;

            $error = array(
                'error_code' => '-3',
                'error_message' => 'Authentication failed ',
            );

            return new Tonic\Response($code, json_encode($error));
        }

        $data = json_decode($this->request->data, true);
        $tCodeError = AccountMovementResource::checkObjectParams($data);
        if ($tCodeError["code"] != Tonic\Response::OK) {
            return new Tonic\Response($tCodeError["code"], json_encode($tCodeError["error"]));
        }
        $oUser = User::find_by_session_id($session_id);
        $oAccountMovement = new AccountMovement($data['account_mvt']);
        $oCredit = Credit::find($oUser->credit_id);
        $new_credit_amount = $oCredit->credit_amount;
        if ($oAccountMovement->account_mvt_debit != 0)
            $new_credit_amount = $oCredit->credit_amount - $oAccountMovement->account_mvt_debit;
        if ($oAccountMovement->account_mvt_credit != 0)
            $new_credit_amount = $oCredit->credit_amount + $oAccountMovement->account_mvt_credit;
        if ($new_credit_amount >= 0) {
            if (!$oAccountMovement->save()) {
                $tCodeError = AccountMovementResource::getRecordErrors($oAccountMovement);
                $error = $tCodeError["error"];
                $code = $tCodeError["code"];
            } else {


                if (!$oUser->update_attributes(array('account_mvt_id' => $oAccountMovement->account_mvt_id))) {
                    $tCodeError = AccountResource::getRecordErrors($oAccountMovement);
                    $error = $tCodeError["error"];
                    $code = $tCodeError["code"];
                } else {
                    $oCredit->update_attributes(array('credit_amount' => $new_credit_amount));
                }
            }
        } else {
            $error = array(
                'error_code' => 'DEFINE ERROR CODE',
                'error_message' => 'Insufficient balance ',
            );
        }

        $outputObject['error'] = $error;
        $jsonBody = json_encode($outputObject);

        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method PUT
     * @provides application/json
     */
    function methodNotAllowed() {

        $code = Tonic\Response::METHODNOTALLOWED;
        $outputObject = array();
        $error = array(
            'error_code' => '-2',
            'error_message' => 'Method not allowed',
        );
        $outputObject['error'] = $error;
        $jsonBody = json_encode($outputObject);
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * get ActiveRecord validation error on fields
     */
    private static function getRecordErrors($_oBject) {

        $error = array(
            "error_code" => "",
            "error_message" => ""
        );

        switch (get_class($_oBject)) {

            case 'AccountMovement':
                if ($_oBject->errors->on('account_mvt_op_id')) {
                    if (empty($_oBject->account_mvt_level)) {
                        $error = array(
                            "error_code" => "-22",
                            "error_message" => "Missing account_mvt:account_mvt_op_id"
                        );
                    } else {
                        $error = array(
                            "error_code" => "-24",
                            "error_message" => "Wrong value for account_mvt:account_mvt_op_id"
                        );
                    }
                }

                break;
        }

        return array("code" => Tonic\Response::CONFLICT, "error" => $error);
    }

    /**
     * check all object params of each HTTP request method
     * must content account_mvt params
     * $data request params
     */
    public static function checkObjectParams($data) {

        $error = array(
            'error_code' => '',
            'error_message' => ''
        );
        $code = Tonic\Response::OK;

        if (empty($data["account_mvt"])) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing account_mvt params'
            );
            $code = Tonic\Response::CONFLICT;
        }

        return array("code" => $code, "error" => $error);
    }

    private function check_authentication() {

        try {
            $session_id = $this->request->params['session_id'];
            $oUser = User::find_by_session_id($session_id);
            if (!is_object($oUser))
                return false;
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

}
