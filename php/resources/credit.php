<?php

/**
 * @uri /credit/:session_id
 * @uri /credit/:user_id/:session_id
 */
class CreditResource extends Tonic\Resource {

    /**
     * @method GET
     * @provides application/json
     */
    function find($user_id = '', $session_id = "") {
        $code = Tonic\Response::OK;
        $outputObject = array();
        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        if (empty($session_id)) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing session:session_id'
            );
            $code = Tonic\Response::CONFLICT;
            return new Tonic\Response($code, json_encode($error));
        }

        if (empty($user_id)) {
            $error = array(
                'error_code' => '-2',
                'error_message' => 'Method not allowed'
            );
            $code = Tonic\Response::CONFLICT;
            return new Tonic\Response($code, json_encode($error));
        }

        if ($code == Tonic\Response::OK) {
            if (!$this->check_authentication()) {
                $error = array(
                    'error_code' => '-3',
                    'error_message' => 'Authentication failed'
                );
                $code = Tonic\Response::CONFLICT;
            } else {
                try {
                    $oUser = User::find($user_id);
                    $oCredit = $oUser->credit;
                    $outputObject['credit']['credit_amount'] = $oUser->credit->credit_amount;
                } catch (Exception $e) {
                    $error = array(
                        'error_code' => '-1',
                        'error_message' => 'Not found'
                    );
                    $code = Tonic\Response::NOTFOUND;
                }
            }
        }

        $outputObject['error'] = $error;

        $jsonBody = json_encode($outputObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method POST
     * @provides application/json
     */
    function create($session_id = "") {
        $outputObject = array();
        $code = Tonic\Response::OK;
        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        if (empty($session_id)) {
            $code = Tonic\Response::CONFLICT;
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing session_id'
            );

            return new Tonic\Response($code, json_encode($error));
        }

        if ($code == Tonic\Response::OK) {
            if (!$this->check_authentication()) {
                $code = Tonic\Response::CONFLICT;
                $error = array(
                    'error_code' => '-3',
                    'error_message' => 'Authentication failed'
                );
            }
        }

        $data = json_decode($this->request->data, true);
        $tCodeError = CreditResource::checkObjectParams($data);

        if ($tCodeError["code"] != Tonic\Response::OK) {
            $code = $tCodeError['code'];
            $error = $tCodeError['error'];
        }

        if ($code == Tonic\Response::OK) {
            $oUser = User::find_by_session_id($session_id);
            if ($oUser->credit) {
                $oUser->credit->credite($data['credit']['credit_amount']);
                $outputObject['credit'] = array('account_balance' => $oUser->credit->credit_amount);
            } else {
                $oCredit = new Credit($data['credit']);
                $oCredit->user_id = $oUser->user_id;
                if (!$oCredit->save()) {
                    $tCodeError = CreditResource::getRecordErrors($oCredit);
                    $error = $tCodeError["error"];
                    $code = $tCodeError["code"];
                } else {
                    $outputObject['credit'] = array('account_balance' => $oUser->credit->credit_amount);
                }
            }
        }

        $outputObject['error'] = $error;
        $jsonBody = json_encode($outputObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method PUT, DELETE
     * @provides application/json
     */
    function methodNotAllowed() {

        $code = Tonic\Response::METHODNOTALLOWED;
        $outputObject = array();
        $error = array(
            'error_code' => '-2',
            'error_message' => 'Method not allowed',
        );
        $outputObject['error'] = $error;
        $jsonBody = json_encode($outputObject);
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * get ActiveRecord validation error on fields
     */
    private static function getRecordErrors($_oBject) {

        $error = array(
            "error_code" => "",
            "error_message" => ""
        );

        switch (get_class($_oBject)) {

            case 'Credit':
                if ($_oBject->errors->on('credit_amount')) {
                    if (empty($_oBject->credit_amount)) {
                        $error = array(
                            "error_code" => "-22",
                            "error_message" => "Missing credit:credit_amount"
                        );
                    } else {
                        $error = array(
                            "error_code" => "-24",
                            "error_message" => "Wrong value for credit:credit_amount"
                        );
                    }
                }

                break;
        }

        return array("code" => Tonic\Response::CONFLICT, "error" => $error);
    }

    /**
     * check all object params of each HTTP request method
     * must content credit params
     * $data request params
     */
    public static function checkObjectParams($data) {

        $error = array(
            'error_code' => '',
            'error_message' => ''
        );
        $code = Tonic\Response::OK;

        if (empty($data["credit"])) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing credit params'
            );
            $code = Tonic\Response::CONFLICT;
        }


        if (empty($data["credit"]["credit_amount"])) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'credit_amount'
            );
            $code = Tonic\Response::CONFLICT;
        }

        return array("code" => $code, "error" => $error);
    }

    private function check_authentication() {

        try {

            $session_id = $this->request->params['session_id'];
            $oUser = User::find_by_session_id($session_id);
            if (!is_object($oUser))
                return false;
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

}
