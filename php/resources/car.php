<?php

/**
 * @uri /car
 * @uri /car/:session_id
 * @uri /car/:car_id/:session_id
 * @uri /driver/car/:car_id/:session_id
 */
class CarResource extends Tonic\Resource {

    /**
     * @method GET
     * @provides application/json
     */
    function find($car_id = '', $session_id = "") {
        $code = Tonic\Response::OK;
        $outputObject = array();
        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        if (empty($session_id)) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing session:session_id'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if (empty($car_id)) {
            $error = array(
                'error_code' => '-2',
                'error_message' => 'Method not allowed'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if ($code == Tonic\Response::OK) {
            if (!$this->check_authentication()) {
                $error = array(
                    'error_code' => '-3',
                    'error_message' => 'Authentication failed'
                );
                $code = Tonic\Response::CONFLICT;
            }
        }

        if ($code == Tonic\Response::OK) {
            try {
                $oCar = Car::find($car_id); //get car by id                      
                $outputObject['car'] = $oCar->attributes();
                $car_position = $oCar->position->attributes();
                $outputObject['car']['car_position'] = $car_position;
                $outputObject['car']['driver_firstname'] = $oCar->driver->driver_firstname;
                $outputObject['car']['driver_lastname'] = $oCar->driver->driver_lastname;

                unset($outputObject['car']['position_id']);
                unset($outputObject['car']['driver_id']);
                array_shift($outputObject['car']['car_position']);
                unset($outputObject['car']['car_position']['positionable_type']);
                unset($outputObject['car']['car_position']['positionable_id']);
            } catch (Exception $e) {
                $error = array(
                    'error_code' => '-1',
                    'error_message' => 'Not found'
                );
                $code = Tonic\Response::NOTFOUND;
            }
        }

        $outputObject['error'] = $error;

        $jsonBody = json_encode($outputObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method POST
     * @provides application/json
     */
    function create($session_id = "") {

        /*      $oPayment = new Payment();

          print_r($oPayment);die; */
        $outputObject = array();

        $code = Tonic\Response::OK;

        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        if (empty($session_id)) {
            $code = Tonic\Response::CONFLICT;

            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing session:session_id'
            );

            return new Tonic\Response(Tonic\Response::OK, json_encode(array('error' => $error)));
        }

        if (!$this->check_authentication()) {
            $code = Tonic\Response::CONFLICT;

            $error = array(
                'error_code' => '-3',
                'error_message' => 'Authentication failed'
            );

            return new Tonic\Response(Tonic\Response::OK, json_encode(array('error' => $error)));
        }

        $data = json_decode($this->request->data, true);
        $tCodeError = CarResource::checkObjectParams($data);
        if ($tCodeError["code"] != Tonic\Response::OK) {
            return new Tonic\Response(Tonic\Response::OK, json_encode(array('error' => $tCodeError["error"])));
        }

        try {
            $exist = Car::find($data["car"]["car_id"]);
        } catch (Exception $e) {
            $error = array(
                'error_code' => 'DEFINE ERROR CODE',
                'error_message' => 'User already exists');
        }

        if (!empty($exist)) {
            $error = array(
                'error_code' => 'DEFINE ERROR CODE',
                'error_message' => 'Car already exists',
            );
        } else {
            $oDriver = new Driver($data["car"]["driver"]);
            $oPosition = new Position($data["car"]["car_position"]);
            unset($data["car"]["driver"]);
            unset($data["car"]["car_position"]);
            $oCar = new Car($data['car']);
            $oCar->driver->driver_firstname = $oDriver->driver_firstname;
            $oCar->driver->driver_lastname = $oDriver->driver_lastname;

            if ($oDriver->is_valid() && $oPosition->is_valid()) {
                if (!$oCar->save()) {
                    $tCodeError = CarResource::getRecordErrors($oCar);
                    $error = $tCodeError["error"];
                    $code = $tCodeError["code"];
                } else {
                    $oCar->create_driver($oDriver->attributes());
                    $oCar->create_position($oPosition->attributes());
                    $oCar->driver_identity = $oCar->driver->driver_identity;
                    $oCar->position_id = $oCar->position->position_id;
                    $oCar->save();
                }
            } else {
                /* if(!$oDriver->save()){            
                  $tCodeError = CarResource::getRecordErrors($oDriver);
                  $error = $tCodeError["error"];
                  $code = $tCodeError["code"];
                  } */
                if ($code == Tonic\Response::OK && !$oPosition->save()) {
                    $tCodeError = CarResource::getRecordErrors($oPosition);
                    $error = $tCodeError["error"];
                    $code = $tCodeError["code"];
                }
            }
        }



        $outputObject['error'] = $error;
        $jsonBody = json_encode($outputObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method PUT
     * @provides application/json
     */
    function update($car_id = "", $session_id = "") {

        $outputObject = array();

        $code = Tonic\Response::OK;

        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        $data = json_decode($this->request->data, true);

        if (empty($session_id)) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing session:session_id'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if (empty($car_id)) {
            $error = array(
                'error_code' => '-2',
                'error_message' => 'Method not allowed'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if ($code == Tonic\Response::OK) {
            if (!$this->check_authentication()) {
                $error = array(
                    'error_code' => '-3',
                    'error_message' => 'Authentication failed'
                );
                $code = Tonic\Response::CONFLICT;
            }
        }

        if ($code == Tonic\Response::OK) {

            $tCodeError = CarResource::checkObjectParams($data);
            if ($tCodeError["code"] != Tonic\Response::OK) {
                return new Tonic\Response(Tonic\Response::OK, json_encode(array('error' => $tCodeError["error"])));
            }

            try {
                $oCar = Car::find($car_id); //get car by id    
                if (isset($data['car']['car_position'])) {
                    $tPositionParam = $data['car']['car_position'];
                    unset($data['car']['car_position']);
                }

                if (!$oCar->update_attributes($data['car'])) {
                    $tCodeError = CarResource::getRecordErrors($oCar);
                    $error = $tCodeError["error"];
                    $code = $tCodeError["code"];
                }

                //update position if param position exist
                if (isset($tPositionParam)) {
                    if (is_object($oCar->position)) {
                        if (!$oCar->position->update_attributes($tPositionParam)) {
                            $tCodeError = CarResource::getRecordErrors($oCar->position);
                            $error = $tCodeError["error"];
                            $code = $tCodeError["code"];
                        }

                    } else {
                        $oCar->create_position(array_merge($tPositionParam, array('positionable_type' => 'Car', 'positionable_id' => $oCar->id)));
                        $oCar->update_attribute('position_id', $oCar->position->position_id);
                    }
                }

                if($oCar->current_active_ride){
                    $outputObject['ride']['ride_status'] = $oCar->current_active_ride->ride_status;                    
                }

                $outputObject['car']['car_status'] = $oCar->car_status;



            } catch (Exception $e) {
                $error = array(
                    'error_code' => '-1',
                    'error_message' => 'Not found'
                );
                $code = Tonic\Response::NOTFOUND;
            }
        }

        $outputObject['error'] = $error;

        $jsonBody = json_encode($outputObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * get ActiveRecord validation error on fields
     */
    private static function getRecordErrors($_oBject) {

        $error = array(
            "error_code" => "",
            "error_message" => ""
        );

        switch (get_class($_oBject)) {
            case 'Driver':
                if ($_oBject->errors->on('driver_firstname')) {
                    $error = array(
                        "error_code" => "-25",
                        "error_message" => "Missing car:driver:driver_firstname"
                    );
                }

                break;

                if ($_oBject->errors->on('driver_lastname')) {
                    $error = array(
                        "error_code" => "-25",
                        "error_message" => "Missing car:driver:driver_lastname"
                    );
                }

                break;

            case 'Position':
                if ($_oBject->errors->on('longitude')) {
                    $error = array(
                        "error_code" => "-13",
                        "error_message" => "Missing car:car_position:longitude"
                    );
                }
                if ($_oBject->errors->on('latitude')) {
                    $error = array(
                        "error_code" => "-14",
                        "error_message" => "Missing car:car_position:latitude"
                    );
                }

                break;

            case 'Car':
                if ($_oBject->errors->on('car_level')) {
                    if (empty($_oBject->car_level)) {
                        $error = array(
                            "error_code" => "-22",
                            "error_message" => "Missing car:car_level"
                        );
                    } else {
                        $error = array(
                            "error_code" => "-24",
                            "error_message" => "Wrong value for car:car_level"
                        );
                    }
                }

                break;
        }

        return array("code" => Tonic\Response::CONFLICT, "error" => $error);
    }

    /**
     * check all object params of each HTTP request method
     * must content car, driver and position params
     * $data request params
     */
    public static function checkObjectParams($data) {

        $error = array(
            'error_code' => '',
            'error_message' => ''
        );
        $code = Tonic\Response::OK;

        if (empty($data["car"])) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing car params'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if (empty($data["car"]["car_position"])) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing car:car_position params'
            );
            $code = Tonic\Response::CONFLICT;
        }


        return array("code" => $code, "error" => $error);
    }

    private function check_authentication() {

        try {
            $session_id = $this->request->params['session_id'];
            if (preg_match('/driver/', substr($this->request->uri, 0, 7))) {
                $oAuth = Driver::find_by_session_id($session_id);
                if (is_object($oAuth)) {
                    $oAuth->persist_authentication();
                }
            } else {
                $oAuth = User::find_by_session_id($session_id);
            }

            if (!is_object($oAuth))
                return false;
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

}
