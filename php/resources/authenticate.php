<?php

/**
 * @uri /authenticate
 * @uri /logout/:session_id
 */
class Authenticate extends Tonic\Resource {

    /**
     * @method GET
     * @provides application/json
     */
    function methodNotAllowed() {
        $code = Tonic\Response::METHODNOTALLOWED;

        $jsonBody = json_encode(array(
            'error' => array(
                'error_code' => '-2',
                'error_message' => 'Method not allowed',
            )
        ));

        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method POST
     * @provides application/json
     */
    function doAuthenticate() {

        $code = Tonic\Response::OK;
        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        $data = json_decode($this->request->data, true);

        if (empty($data['user']['user_id'])) {

            $error = array(
                'error_code' => '-10',
                'error_message' => 'Missing user:user_id',
            );
            $code = Tonic\Response::CONFLICT;
        } elseif (empty($data['user']['user_type'])) {

            $error = array(
                'error_code' => '-11',
                'error_message' => 'Missing user:user_type',
            );
            $code = Tonic\Response::CONFLICT;
            /* }elseif(empty($data['user']['subscription_code'])) {

              $error = array(
              'error_code'    => '-18',
              'error_message' => 'Missing user:subscription_code',
              );
              $code = Tonic\Response::CONFLICT;

             */
        } elseif (empty($data['user']['user_device']['os_name'])) {

            $error = array(
                'error_code' => '-12',
                'error_message' => 'Missing user:user_device:os_name',
            );
            $code = Tonic\Response::CONFLICT;
        } elseif (empty($data['user']['user_device']['os_version'])) {

            $error = array(
                'error_code' => '-13',
                'error_message' => 'Missing user:user_device:os_version',
            );
            $code = Tonic\Response::CONFLICT;
        } elseif (empty($data['user']['user_device']['notification_id'])) {

            $error = array(
                'error_code' => '-14',
                'error_message' => 'Missing user:user_device:notification_id',
            );
            $code = Tonic\Response::CONFLICT;
        } elseif (!preg_match('/^(passenger|driver|regulator)$/', $data['user']['user_type'])) {

            $error = array(
                'error_code' => '-15',
                'error_message' => 'Wrong value for user:user_type',
            );
            $code = Tonic\Response::CONFLICT;
        } elseif (!empty($data['user']['user_priority']) && !preg_match('/^(high|normal|low)$/', $data['user']['user_priority'])) {

            $error = array(
                'error_code' => '-16',
                'error_message' => 'Wrong value for user:user_priority',
            );
            $code = Tonic\Response::CONFLICT;
        } elseif (!preg_match('/^(iOS|Android|low)$/', $data['user']['user_device']['os_name'])) {

            $error = array(
                'error_code' => '-17',
                'error_message' => 'Wrong value for user:user_device:os_name',
            );
            $code = Tonic\Response::CONFLICT;
        }

        if ($code == Tonic\Response::OK) {
            $user = User::find('first', array(
                        'joins' => array('device'),
                        'conditions' => array(
                            'user_id = ? AND user_type = ? AND os_name = ?', $data['user']['user_id'], $data['user']['user_type'], $data['user']['user_device']['os_name'])));

            if (isset($user)) {
                $session_id = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuxyvwzABCDEFGHIJKLMNOPQRSTUXYVWZ", 5)), 0, 63);
                if(!isset($data['user']['user_priority'])) {
                    $data['user']['user_priority'] = 'normal';
                }
                $outputObject['session'] = array(
                    'session_id' => $session_id,
                    'session_priority' => $data['user']['user_priority'],
                );
                $user->update_attribute('session_id', $outputObject['session']['session_id']);
                $user->device->update_attributes(
                        array(
                            'notification_id' => $data['user']['user_device']['notification_id'],
                            'os_version' => $data['user']['user_device']['os_version']
                        )
                );
            } else {
                $error = array(
                    'error_code' => '-1',
                    'error_message' => 'Not found',
                );
                $code = Tonic\Response::NOTFOUND;
            }
        }

        $outputObject['error'] = $error;

        $jsonBody = json_encode($outputObject);

        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method DELETE
     * @provides application/json
     */
    public function logout($session_id = "") {
        $code = Tonic\Response::OK;
        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );
        $outputObject = array();

        if (empty($session_id)) {
            $code = Tonic\Response::CONFLICT;
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing param session_id',
            );
        }

        if ($code == Tonic\Response::OK) {
            if ($oUser = User::find_by_session_id($session_id))
                $oUser->deconnect();
        }

        $outputObject['error'] = $error;

        $jsonBody = json_encode($outputObject);

        return new Tonic\Response($code, $jsonBody);
    }

}
