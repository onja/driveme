<?php

/**
 * @uri /driver/authenticate
 * @uri /driver/logout
 * @uri /driver/logout/:session_id
 */
class DriverAuthenticateResource extends Tonic\Resource {

    /**
     * @method GET
     * @provides application/json
     */
    function methodNotAllowed() {
        $code = Tonic\Response::OK;

        $jsonBody = json_encode(array(
            'error' => array(
                'error_code' => '-2',
                'error_message' => 'Method not allowed',
            )
        ));

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method POST
     * @provides application/json
     */
    function doAuthenticate() {

        $code = Tonic\Response::OK;
        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        $data = json_decode($this->request->data, true);

        if (empty($data['driver']['driver_identity'])) {

            $error = array(
                'error_code' => '-50',
                'error_message' => 'Missing driver:driver_identity',
            );
        }

        if (empty($data['driver']['code_pin'])) {

            $error = array(
                'error_code' => '-51',
                'error_message' => 'Missing driver:code_pin',
            );
        }

        if (empty($data['driver']['car_id'])) {

            $error = array(
                'error_code' => '-52',
                'error_message' => 'Missing driver:car:car_id',
            );
        }
        
        if (empty($data['driver']['notification_id'])) {

            $error = array(
                'error_code' => '-53',
                'error_message' => 'Missing driver:notification_id',
            );
        }

        if ($error['error_code'] == 0) {
            $driver = Driver::find('first', array(
                        'select' => "*",
                        'conditions' => array(
                            'driver_identity = ? AND code_pin = ?', $data['driver']['driver_identity'], $data['driver']['code_pin'])
                            )
            );

            $car = Car::find('first', array(
                        'select' => "*",
                        'conditions' => array(
                            'car_id = ?', $data['driver']['car_id'])
                            )
            );

            if (!is_object($driver) || !is_object($car)) {
                $error = array(
                    'error_code' => '-3',
                    'error_message' => 'Authentication failed',
                );
            } else {
                $session_id = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuxyvwzABCDEFGHIJKLMNOPQRSTUXYVWZ", 5)), 0, 63);
                $outputObject['session'] = array(
                    'session_id' => $session_id
                );

                $car->update_attributes(
                        array(
                            'driver_identity' => $data['driver']['driver_identity'],
                            'car_status' => 'not-available',
                            'connection_status' => 'available',
                            'last_request' => date('c')
                        )
                );

                $tDriverParams = array('session_id' => $outputObject['session']['session_id']);
                if (isset($data['driver']['notification_id'])) {
                    $tDriverParams = array_merge($tDriverParams, array('notification_id' => $data['driver']['notification_id']));
                }
                $driver->update_attributes($tDriverParams);
                //push to driver if avalilable reservation exists
                $driver->push_reservation();
            }
        }

        $outputObject['error'] = $error;

        $jsonBody = json_encode($outputObject);

        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method DELETE
     * @provides application/json
     */
    public function logout($session_id = "") {

        $code = Tonic\Response::OK;
        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );
        $outputObject = array();

        if (empty($session_id)) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing param session_id',
            );
        }

        if ($error['error_code'] == 0) {
            if ($oDriver = Driver::find_by_session_id($session_id)) {
                $oDriver->deconnect();
            } else {
                $error = array(
                    'error_code' => '-1',
                    'error_message' => 'Driver with this session not found',
                );
            }
        }

        $outputObject['error'] = $error;

        $jsonBody = json_encode($outputObject);

        return new Tonic\Response($code, $jsonBody);
    }

}
