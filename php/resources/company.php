<?php

/**
 * @uri /company
 * @uri /company/:key_id
 * @uri /company/:company_id/:session_id
 */
class CompanyResource extends Tonic\Resource {

    /**
     * @method GET
     * @provides application/json
     */
    function find($company_id = "", $session_id = "") {

        $code = Tonic\Response::OK;
        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );
        $outPutObject = array();
        if (empty($session_id)) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing session:session_id'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if (empty($company_id)) {
            $error = array(
                'error_code' => '-2',
                'error_message' => 'Method not allowed'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if ($code == Tonic\Response::OK) {
            if (!$this->check_authentication()) {
                $error = array(
                    'error_code' => '-3',
                    'error_message' => 'Authentication failed'
                );
                $code = Tonic\Response::CONFLICT;
            }
        }

        if ($code == Tonic\Response::OK) {
            try {
                $oCompany = Company::find($company_id);
            } catch (Exception $e) {
                $code = Tonic\Response::CONFLICT;
                $error = array(
                    'error_code' => '-1',
                    'error_message' => 'Not found'
                );
            }
        }

        if ($code == Tonic\Response::OK) {
            $tCompany = $oCompany->attributes();
            array_shift($tCompany);
            $outPutObject['company'] = $tCompany;
        }

        $outPutObject['error'] = $error;
        $jsonBody = json_encode($outPutObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method POST
     * @provides application/json
     */
    function create() {

        $outputObject = array();

        $code = Tonic\Response::OK;

        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        $data = json_decode($this->request->data, true);

        $tCodeError = CompanyResource::checkObjectParams($data);
        if ($tCodeError["code"] != Tonic\Response::OK) {
            return new Tonic\Response($tCodeError["code"], json_encode($tCodeError["error"]));
        }
        $oCompany = new Company($data['company']);
        if (!$oCompany->save()) {
            $tCodeError = CompanyResource::getRecordErrors($oCompany);
            $error = $tCodeError["error"];
            $code = $tCodeError["code"];
        }



        $outputObject['error'] = $error;
        $jsonBody = json_encode($outputObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method PUT
     * @provides application/json
     */
    function update($company_id = "", $session_id = "") {

        $outputObject = array();
        $code = Tonic\Response::OK;
        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        $data = json_decode($this->request->data, true);

        $tCodeError = CompanyResource::checkObjectParams($data);

        if ($tCodeError["code"] != Tonic\Response::OK) {
            return new Tonic\Response($tCodeError["code"], json_encode($tCodeError["error"]));
        }

        if (empty($session_id)) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing session:session_id'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if (empty($company_id)) {
            $error = array(
                'error_code' => '-2',
                'error_message' => 'Method not allowed'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if ($code == Tonic\Response::OK) {
            if (!$this->check_authentication()) {
                $error = array(
                    'error_code' => '-3',
                    'error_message' => 'Authentication failed'
                );
                $code = Tonic\Response::CONFLICT;
            }
        }
        if ($code == Tonic\Response::OK) {

            try {
                $oCompany = Company::find($company_id);

                if (!$oCompany->update_attributes($data["company"])) {
                    $tCodeError = CompanyResource::getRecordErrors($oCompany);
                    $error = $tCodeError["error"];
                    $code = $tCodeError["code"];
                }
            } catch (Exception $e) {
                $error = array(
                    'error_code' => '-1',
                    'error_message' => 'Not found'
                );
                $code = Tonic\Response::NOTFOUND;
            }
        }

        $outputObject['error'] = $error;
        $jsonBody = json_encode($outputObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * get ActiveRecord validation error on fields
     */
    private static function getRecordErrors($_oBject) {
        $error = array(
            "error_code" => "",
            "error_message" => ""
        );

        switch (get_class($_oBject)) {
            case 'Company':
                if ($mess = $_oBject->errors->on('company_name')) {
                    $error = array(
                        "error_code" => "-11",
                        "error_message" => is_array($mess) ? $mess[0] : $mess
                    );
                }

                break;
        }

        return array("code" => Tonic\Response::CONFLICT, "error" => $error);
    }

    /**
     * check all object params of each HTTP request method
     * must content company and device params
     * $data request params
     */
    public static function checkObjectParams($data) {

        $error = array(
            'error_code' => '',
            'error_message' => ''
        );
        $code = Tonic\Response::OK;

        if (empty($data["company"])) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing company params'
            );
            $code = Tonic\Response::CONFLICT;
        }


        return array("code" => $code, "error" => $error);
    }

    private function check_authentication() {
        try {
            $session_id = $this->request->params['session_id'];
            $oUser = User::find_by_session_id($session_id);
            if (!is_object($oUser))
                return false;
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

}
