<?php

/**
 * @uri /payment/credit
 * @uri /payment/credit/:session_id
 */
class PaymentCreditResource extends Tonic\Resource {

    /**
     * @method POST
     * @provides application/json
     */
    function send($session_id = "") {
        session_start();
        error_reporting(E_ALL);
        error_reporting(E_ALL);
        ini_set('display_errors', 'On');

        $code = Tonic\Response::OK;
        $outputObject = array();
        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        if (empty($session_id)) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing param session_id'
            );
            $code = Tonic\Response::CONFLICT;
        }


        if(Tonic\Response::OK && empty($_SESSION['crd_amount'])){
          $session_id = $this->request->params['session_id'];
          header('Location: /html/choosecredit.php?session_id=' . $session_id);
          exit();
        }

        /*if code OK*/
        if($code == Tonic\Response::OK){
          $oUser = $this->check_authentication();
          if($oUser === false){
              $error = array(
                'error_code'    => '-3',
                'error_message' => 'Authentication failed'
              );
              $code = Tonic\Response::CONFLICT;   
          }else{
            
            /*exception*/
            //parse post param into array
            parse_str($this->request->data, $data);
            //$data = json_decode($this->request->data, true);
            $tCreditValues = explode('_', $_SESSION['crd_amount']);
            $data['payment']['amount'] = floatval($tCreditValues[1]);
            $data['credit']['credit_amount'] = floatval($tCreditValues[0]);
            unset($_SESSION['crd_amount']);
            
            try {
              $bCreditOK = false;             
              //if pay with stored card
              if(!empty($data['card']['card_id'])){
                $oCard = Card::find($data['card']['card_id']);
                $porteur = $oCard->number;
                $cvv = $oCard->cvv;
                $dateval = $oCard->expired_date;
              }else {
                $porteur = $data['card']['porteur'];
                $cvv = $data['card']['cvv'];
                $dateval = preg_replace('/\//', '', $data['card']['dateval']);
                
                //save card
                if(!empty($data['card']['save_card'])){
                  Card::create(array(
                      'user_id' => $oUser->user_id,
                      'title' => $data['card']['title'],
                      'number' => $porteur,
                      'cvv' => $cvv,
                      'owner' => $data['card']['owner'],
                      'expired_date' => $dateval
                    ));
                }
              }                

              $tParams = array(
                  'porteur' => $porteur,
                  'dateval' => $dateval,
                  'cvv' => $cvv,
                  'reference' => 'DM' . time(),
                  'num_question' => time(),
                  'amount' => $data['payment']['amount']
                );

              $tPayboxVars = Payment::authorize($tParams);  
              //store paybox data
              if(floatval($tPayboxVars['CODEREPONSE']) == 0){
                $tPayboxAttrs = array(
                  'user_id' => $oUser->user_id,
                  'porteur' => $tParams['porteur'],
                  'dateval' => $tParams['dateval'],
                  'cvv' => $tParams['cvv'],
                  'num_question' => $tPayboxVars['NUMQUESTION'],
                  'num_trans' => $tPayboxVars['NUMTRANS'],
                  'num_appel' => $tPayboxVars['NUMAPPEL'],
                  'reference' => $tParams['reference'],
                  'code_response' => $tPayboxVars['CODEREPONSE'],
                  'authorisation' => $tPayboxVars['AUTORISATION'],
                  'amount' => $data['payment']['amount'],
                  'paybox_status' => 'pending',
                  'comment' => $tPayboxVars['COMMENTAIRE']
                );
                $oPayboxCredit = new PayboxData($tPayboxAttrs);
                $oPayboxCredit->save();
                //PayboxData::create($tPayboxAttrs);
                $tSendParams = array_merge($tParams, array(
                    'num_trans' => $tPayboxVars['NUMTRANS'],
                    'num_appel' => $tPayboxVars['NUMAPPEL']
                  ));
                $tPayboxSend = Payment::send($tSendParams); 

                if(intval($tPayboxSend['CODEREPONSE']) == 0){
                  if ($oUser->credit) {
                    $oUser->credit->credite($data['credit']['credit_amount']);
                    
                  } else {
                    $oCredit = new Credit($data['credit']);
                    $oCredit->user_id = $oUser->user_id;
                    $oCredit->save();
                  }
                  
                  $bCreditOK = true ;

                }
                
              }

              if($bCreditOK){               
                header('Location: /html/approved.php');
              } else {
                header('Location: /html/declined.php');                
              }
            } catch (Exception $e) {
                header('Location: /html/declined.php');
            }
            /* fin exception */
          }
            /* fin if user false */
        }

        /* fin if code ok */

       exit();
    }

    /**
     * @method GET
     * @method PUT
     * @method DELETE
     * @provides application/json
     */
    function methodNotAllowed() {

        $code = Tonic\Response::METHODNOTALLOWED;
        $outputObject = array();
        $error = array(
            'error_code' => '-2',
            'error_message' => 'Method not allowed',
        );
        $outputObject['error'] = $error;
        $jsonBody = json_encode($outputObject);
        return new Tonic\Response($code, $jsonBody);
    }

    private function check_authentication() {
        try {
            $session_id = $this->request->params['session_id'];
            $oUser = User::find_by_session_id($session_id);
            if (!is_object($oUser))
                return false;
        } catch (Exception $e) {
            return false;
        }

        return $oUser;
    }

}
