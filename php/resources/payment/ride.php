<?php

/**
 * @uri /payment/ride/check
 * @uri /payment/ride/check/:key_id
 * @uri /payment/ride/check/:ride_id/:session_id
 */
class PaymentRideResource extends Tonic\Resource {

    /**
     * @method POST     
     */
    function check_authorization($ride_id = "", $session_id = "") {

        error_reporting(E_ALL);
        error_reporting(E_ALL);
        ini_set('display_errors', 'On');

        $code = Tonic\Response::OK;
        $outputObject = array();
        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        if (empty($session_id)) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing param session_id'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if (empty($ride_id)) {
            $error = array(
                'error_code' => '-61',
                'error_message' => 'Missing param ride_id'
            );
            $code = Tonic\Response::CONFLICT;
        }

        /*if code OK*/
        if($code == Tonic\Response::OK){
          $oUser = $this->check_authentication();
          if($oUser === false){
              $error = array(
                'error_code'    => '-3',
                'error_message' => 'Authentication failed'
              );
              $code = Tonic\Response::CONFLICT;   
          }else{
            
            /*exception*/
            //parse post param into array
            parse_str($this->request->data, $data);
            
            try {

              //$ride_id = $data['ride_id'];
              $oRide = Ride::find ($ride_id);       
              $oUser = $oRide->user;
              $oCredit = $oUser->credit;
              $bCreditOK = false;
              switch ($data['payment']['payment_type']) {
                case 'credit-minute': 
                  $bCreditOK = $oUser->credit ? ($oUser->credit->credit_amount > $oRide->ride_estimated_time) ? true : false : false ;
                  break;
                
                case 'credit-company':
                  $bCreditOK = true ;
                  break;

                case 'credit-card': 
                  //if pay with stored card
                  if(!empty($data['card']['card_id'])){
                    $oCard = Card::find($data['card']['card_id']);
                    $porteur = $oCard->number;
                    $cvv = $oCard->cvv;
                    $dateval = $oCard->expired_date;
                  }else {
                    $porteur = $data['card']['porteur'];
                    $cvv = $data['card']['cvv'];
                    $dateval = preg_replace('/\//', '', $data['card']['dateval']);
                    
                    //save card
                    if(!empty($data['card']['save_card'])){
                      Card::create(array(
                          'user_id' => $oUser->user_id,
                          'title' => $data['card']['title'],
                          'number' => $porteur,
                          'cvv' => $cvv,
                          'owner' => $data['card']['owner'],
                          'expired_date' => $dateval
                        ));
                    }
                  }                

                  $tParams = array(
                      'porteur' => $porteur,
                      'dateval' => $dateval,
                      'cvv' => $cvv,
                      'reference' => 'DM' . time(),
                      'num_question' => time(),
                      'amount' => floatval($oRide->ride_total_price)
                    );
                  
                  $tPayboxVars = Payment::authorize($tParams);  
                  //store paybox data
                  if(floatval($tPayboxVars['CODEREPONSE']) == 0){
                    $tPayboxAttrs = array(
                      'user_id' => $oUser->user_id,
                      'porteur' => $tParams['porteur'],
                      'dateval' => $tParams['dateval'],
                      'cvv' => $tParams['cvv'],
                      'num_question' => $tPayboxVars['NUMQUESTION'],
                      'num_trans' => $tPayboxVars['NUMTRANS'],
                      'num_appel' => $tPayboxVars['NUMAPPEL'],
                      'reference' => $tParams['reference'],
                      'code_response' => $tPayboxVars['CODEREPONSE'],
                      'authorisation' => $tPayboxVars['AUTORISATION'],
                      'amount' => $oRide->ride_total_price,
                      'paybox_status' => 'pending',
                      'comment' => $tPayboxVars['COMMENTAIRE']
                    );
                    PayboxData::create($tPayboxAttrs);
                    $bCreditOK = true ;
                    break;
                  }
                  
              }

              if($bCreditOK){
                //send push to driver
                Utils::push('USER', $oUser->user_id, urlencode('PUSH USER MESSAGE'));
                header('Location: /html/approved.php');                
              } else {
                header('Location: /html/declined.php');
              }
            } catch (Exception $e) {
              header('Location: /html/declined.php');
            }
            /* fin exception */
          }
            /* fin if user false */
        }

        exit();
    }

    /**
     * @method GET
     * @method PUT
     * @method DELETE
     * @provides application/json
     */
    function methodNotAllowed() {

        $code = Tonic\Response::METHODNOTALLOWED;
        $outputObject = array();
        $error = array(
            'error_code' => '-2',
            'error_message' => 'Method not allowed',
        );
        $outputObject['error'] = $error;
        $jsonBody = json_encode($outputObject);
        return new Tonic\Response($code, $jsonBody);
    }

    private function check_authentication() {
        try {
            $session_id = $this->request->params['session_id'];
            $oUser = User::find_by_session_id($session_id);
            if (!is_object($oUser))
                return false;
        } catch (Exception $e) {
            return false;
        }

        return $oUser;
    }

}
