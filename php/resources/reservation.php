<?php

/**
 * @uri /reservation 
 * @uri /reservation/:session_id
 * @uri /reservation/:reservation_id/:session_id
 */

class ReservationResource extends Tonic\Resource {

    /**
     * @method GET
     * @provides application/json
     */
    function find($reservation_id = "", $session_id = "") {

        $outPutObject = array();
        $error = array(
            'error_code' => '0',
            'error_message' => ''
        );
        $code = Tonic\Response::OK;

        if (empty($session_id)) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing session:session_id'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if ($code == Tonic\Response::OK && empty($reservation_id)) {
            $error = array(
                'error_code' => '-2',
                'error_message' => 'Method not allowed'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if ($code == Tonic\Response::OK) {
            $oAuth = $this->check_authentication();
            if ($oAuth === false) {
                $error = array(
                    'error_code' => '-3',
                    'error_message' => 'Authentication failed'
                );
                $code = Tonic\Response::CONFLICT;
            }
        }

        if ($code == Tonic\Response::OK) {
            try {
                $oReservation = reservation::find($reservation_id);
            } catch (Exception $e) {
                
            }
            $outPutObject["reservation"] = $oReservation;
        }

        $outPutObject["error"] = $error;
        $jsonBody = json_encode($outPutObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method POST
     * @provides application/json
     */
    function create($session_id = "") {                
        $outputObject = array();
        $code = Tonic\Response::OK;
        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        $data = json_decode($this->request->data, true);

        if (empty($session_id)) {
            //print_r("ici");die;
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing session_id'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if ($code == Tonic\Response::OK) {
            $oAuth = $this->check_authentication();
            if($oAuth === false){
                $error = array(
                    'error_code' => '-3',
                    'error_message' => 'Authentication failed'
                );
                $code = Tonic\Response::CONFLICT;
            }
        }

        if ($code == Tonic\Response::OK) {
            $tCodeError = ReservationResource::checkObjectParams($data);
            if ($tCodeError["code"] != Tonic\Response::OK) {
                return new Tonic\Response($tCodeError["code"], json_encode($tCodeError["error"]));
            }

            //check if driver exist and available
            if( !empty($data['reservation']['driver_identity']) ){
                $oDriver = Driver::find_by_driver_identity($data['reservation']['driver_identity']);   
                if(!$oDriver){
                    $error = array(
                        'error_code' => '-94',
                        'error_message' => 'Driver not exist'
                    );
                    return new Tonic\Response(Tonic\Response::CONFLICT, json_encode($error));       
                }elseif (!$oDriver->available_on($data['reservation']['reservation_date'], $data['reservation']['reservation_time'])) {
                    $error = array(
                        'error_code' => '-94',
                        'error_message' => 'Driver not available'
                    );
                    return new Tonic\Response(Tonic\Response::CONFLICT, json_encode($error));
                } 
            }
            
            $oStartPosition = new Position($data['reservation']['start_position']);
            $oEndPosition = new Position($data['reservation']['end_position']);
            
            unset($data['reservation']['start_position']);
            unset($data['reservation']['end_position']);
            //print_r($data['reservation']);die;

            
            $oReservation = new Reservation($data['reservation']);
            $oReservation->reservation_status = 'pending';
            $oReservation->user_id = $oAuth->user_id;

            if (!$oReservation->save()) {
                $tCodeError = ReservationResource::getRecordErrors($oReservation);
                $error = $tCodeError["error"];
                $code = $tCodeError["code"];
            } else {
                $oReservation->create_start_position($oStartPosition->attributes());
                $oReservation->create_end_position($oEndPosition->attributes());
                $oReservation->start_position_id = $oReservation->start_position->position_id;
                $oReservation->end_position_id = $oReservation->end_position->position_id;
                $oReservation->set_estimated_data();                
                $oReservation->save();
                $oReservation->reload();

                $outputObject['reservation'] = $oReservation->fetchApiObject();
            }
            
            
        }

        $outputObject['error'] = $error;
        $jsonBody = json_encode($outputObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method PUT
     * @provides application/json
     */
    function update($reservation_id = "", $session_id = "") {

        $outputObject = array();

        $code = Tonic\Response::OK;

        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        $data = json_decode($this->request->data, true);

        if (empty($session_id)) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing session:session_id'
            );
            $code = Tonic\Response::CONFLICT;            
        }

        if ($code == Tonic\Response::OK && empty($reservation_id)) {
            $error = array(
                'error_code' => '-96',
                'error_message' => 'Missing reservation_id'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if ($code == Tonic\Response::OK && empty($session_id)) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing session_id'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if ($code == Tonic\Response::OK) {
            $oAuth = $this->check_authentication();
            if($oAuth === false){
                $error = array(
                    'error_code' => '-3',
                    'error_message' => 'Authentication failed'
                );
                $code = Tonic\Response::CONFLICT;    
            }
            
        }

        if ($code == Tonic\Response::OK) {
            try {
                $oReservation = Reservation::find($reservation_id);
                if (!$oReservation->update_attributes($data['reservation'])) {
                    $tCodeError = $ReservationResource::getRecordErrors($oReservation);
                    $error = $tCodeError["error"];
                    $code = $tCodeError["code"];
                }
            } catch (Exception $e) {
                $error = array(
                    'error_code' => '-1',
                    'error_message' => 'Not found'
                );
                $code = Tonic\Response::NOTFOUND;
            }
        }

        $outputObject['error'] = $error;
        $jsonBody = json_encode($outputObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method DELETE
     * @provides application/json
     */
    function methodNotAllowed() {

        $code = Tonic\Response::METHODNOTALLOWED;
        $outputObject = array();
        $error = array(
            'error_code' => '-2',
            'error_message' => 'Method not allowed',
        );
        $outputObject['error'] = $error;
        $jsonBody = json_encode($outputObject);
        return new Tonic\Response($code, $jsonBody);
    }

    private static function getRecordErrors($_object) {
        
    }

    /**
     * check all object params of each HTTP request method
     * must content reservation, start and end position params
     * $data request params
     */
    public static function checkObjectParams($data) {

        $error = array(
            'error_code' => '',
            'error_message' => ''
        );
        $code = Tonic\Response::OK;

        if (!isset($data["reservation"]["start_position"]["address"])) {
            $error = array(
                'error_code' => '-90',
                'error_message' => 'Missing reservation:start_position:address params'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if (!isset($data["reservation"]["end_position"]["address"])) {
            $error = array(
                'error_code' => '-91',
                'error_message' => 'Missing reservation:end_position:address params'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if (!isset($data["reservation"]["reservation_date"])) {
            $error = array(
                'error_code' => '-92',
                'error_message' => 'Missing reservation:reservation_date params'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if (!isset($data["reservation"]["reservation_time"])) {
            $error = array(
                'error_code' => '-93',
                'error_message' => 'Missing reservation:reservation_time params'
            );
            $code = Tonic\Response::CONFLICT;
        }

        return array("code" => $code, "error" => $error);
    }

    private function check_authentication() {

        try {
            $session_id = $this->request->params['session_id'];
            if (preg_match('/driver/', substr($this->request->uri, 0, 7))) {
                $oAuth = Driver::find_by_session_id($session_id);
                if (is_object($oAuth)) {
                    $oAuth->persist_authentication();
                }
            } else {
                $oAuth = User::find_by_session_id($session_id);
            }

            if (!is_object($oAuth))
                return false;
        } catch (Exception $e) {
            return false;
        }
        return $oAuth;
    }

}
