<?php

/**
 * @uri /payment/check/:session_id
 */
class PaymentResource extends Tonic\Resource {

    /**
     * @method POST
     * @provides application/json
     */
    function check_authorization($session_id = "") {

        error_reporting(E_ALL);
        error_reporting(E_ALL);
        ini_set('display_errors', 'On');

        $code = Tonic\Response::OK;
        $outputObject = array();
        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        if (empty($session_id)) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing session:session_id'
            );
            $code = Tonic\Response::CONFLICT;
        }

        /*if code OK*/
        if($code == Tonic\Response::OK){
          $oUser = $this->check_authentication();
          if($oUser === false){
              $error = array(
                'error_code'    => '-3',
                'error_message' => 'Authentication failed'
              );
              $code = Tonic\Response::CONFLICT;   
          }else{
            
            /*exception*/
            $data = json_decode($this->request->data, true);
            
            try {

              $ride_id = $data['ride']['ride_id'];
              $oRide = Ride::find ($ride_id);
              $oUser = $oRide->user;
              $oCredit = $oUser->credit;

              $bCreditOK = false;
              switch ($data['payment']['payment_type']) {
                case 'credit-minute': 
                  $bCreditOK = $oUser->credit ? ($oUser->credit->credit_amount > $oRide->ride_estimated_time) ? true : false : false ;
                  break;
                
                case 'credit-company':
                  $bCreditOK = true ;
                  break;

                case 'credit-card': 
                  //if pay with stored card
                  if(!empty($data['card']['card_id'])){
                    $oCard = Card::find($data['card']['card_id']);
                    $porteur = $oCard->number;
                    $cvv = $oCard->cvv;
                    $dateval = $oCard->expired_date;
                  }else {
                    $porteur = $data['card']['porteur'];
                    $cvv = $data['card']['cvv'];
                    $dateval = $data['card']['dateval'];
                    //save card
                    if(empty($data['card']['save_on'])){
                      Card::create(array(
                          'user_id' => $oUser->user_id,
                          'title' => $data['card']['title'],
                          'number' => $porteur,
                          'cvv' => $cvv,
                          'owner' => $data['card']['owner'],
                          'expired_date' => $dateval
                        ));
                    }
                  }                

                  $tParams = array(
                      'porteur' => $porteur,
                      'dateval' => $dateval,
                      'cvv' => $cvv,
                      'reference' => 'DM' . time(),
                      'num_question' => time(),
                      'montant' => floatval($oRide->ride_total_price)
                    );
                  
                  $tPayboxVars = Payment::authorize($tParams);  
                  $tPayboxParams = array();

                  foreach ($tPayboxVars as $key => $keyValue) {
                    $tKeyValue = explode('=', $keyValue);
                    $tPayboxParams[$tKeyValue[0]] = $tKeyValue[1];
                  }

                  //store paybox data
                  if(floatval($tPayboxParams['CODEREPONSE']) == 0){
                    $tPayboxAttrs = array(
                      'user_id' => $oUser->user_id,
                      'porteur' => $tParams['porteur'],
                      'dateval' => $tParams['dateval'],
                      'cvv' => $tParams['cvv'],
                      'num_question' => $tPayboxParams['NUMQUESTION'],
                      'num_trans' => $tPayboxParams['NUMTRANS'],
                      'num_appel' => $tPayboxParams['NUMAPPEL'],
                      'reference' => $tParams['reference'],
                      'code_response' => $tPayboxParams['CODEREPONSE'],
                      'authorisation' => $tPayboxParams['AUTORISATION'],
                      'amount' => $oRide->ride_total_price,
                      'paybox_status' => 'pending',
                      'comment' => $tPayboxParams['COMMENTAIRE']
                    );
                    PayboxData::create($tPayboxAttrs);
                    $bCreditOK = true ;
                    break;
                  }
                  
              }

              if($bCreditOK){
                $outputObject['payment'] = array(
                  'payment_status' => 'OK',
                  'payment_message' => 'Carte Bleu Validée'
                );       

              } else {
                $error = array(
                    'error_code' => '-61',
                    'error_message' => 'Veuillez Acheter des credits'
                );
              }
            } catch (Exception $e) {
                $error = array(
                    'error_code' => '-1',
                    'error_message' => 'Not found'
                );
                $code = Tonic\Response::NOTFOUND;
            }
            /* fin exception */
          }
            /* fin if user false */
        }

        /* fin if code ok */

        $outputObject['error'] = $error;

        $jsonBody = json_encode($outputObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method GET
     * @method PUT
     * @method DELETE
     * @provides application/json
     */
    function methodNotAllowed() {

        $code = Tonic\Response::METHODNOTALLOWED;
        $outputObject = array();
        $error = array(
            'error_code' => '-2',
            'error_message' => 'Method not allowed',
        );
        $outputObject['error'] = $error;
        $jsonBody = json_encode($outputObject);
        return new Tonic\Response($code, $jsonBody);
    }

    private function check_authentication() {
        try {
            $session_id = $this->request->params['session_id'];
            $oUser = User::find_by_session_id($session_id);
            if (!is_object($oUser))
                return false;
        } catch (Exception $e) {
            return false;
        }

        return $oUser;
    }

}
