<?php

error_reporting(E_ALL);
ini_set('display_errors', 'On');

/**
 * @uri /regulator/ride/:ride_id/:session_id
 */
class RegulatorRideResource extends Tonic\Resource {

    /**
     * @method GET
     * @provides application/json
     */
    function find($ride_id = "", $session_id = "") {

        $outputObject = array();
        $error = array(
            'error_code' => '0',
            'error_message' => ''
        );
        $code = Tonic\Response::OK;

        if (empty($session_id)) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing session:session_id'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if (empty($ride_id)) {
            $error = array(
                'error_code' => '-2',
                'error_message' => 'Method not allowed'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if ($code == Tonic\Response::OK) {
            $oAuth = $this->check_authentication();
            if ($oAuth === false) {
                $error = array(
                    'error_code' => '-3',
                    'error_message' => 'Authentication failed'
                );
                $code = Tonic\Response::CONFLICT;
            }
        }

        if ($code == Tonic\Response::OK) {
            try {
                $oRide = Ride::find($ride_id);
            } catch (Exception $e) {
                $error = array(
                    'error_code' => '-1',
                    'error_message' => 'Not found'
                );
                $code = Tonic\Response::NOTFOUND;
            }
            $outputObject['ride'] = array(
                "ride_id" => $oRide->ride_id,
                "ride_status" => $oRide->ride_status,
                "start_position" => array("longitude" => $oRide->start_position->longitude,
                    "latitude" => $oRide->start_position->latitude,
                    "address" => $oRide->start_position->address),
                "end_position" => array("longitude" => $oRide->end_position->longitude,
                    "latitude" => $oRide->end_position->latitude,
                    "address" => $oRide->end_position->address),
                "ride_estimated_time" => $oRide->ride_estimated_time,
                "ride_estimated_length" => $oRide->ride_estimated_length,
                "ride_pack_price" => $oRide->ride_pack_price,
                "ride_minute_price" => $oRide->ride_minute_price,
                "ride_total_price" => $oRide->ride_total_price
            );

            if ($oRide->start_date) {
                $outputObject['ride']['start_date'] = $oRide->start_date->format('d/m/Y h\H m\m\i\n');
            }

            $outputObject['user'] = array(
                'user_id' => $oRide->user->user_id,
                'code_pin' => $oRide->user->code_pin,
                'user_priority' => $oRide->user->user_priority,
                'user_firstname' => $oRide->user->user_firstname,
                'user_lastname' => $oRide->user->user_lastname,
                'user_phone' => $oRide->user->user_phone,
                'user_email' => $oRide->user->user_email,
                'subscription_code' => $oRide->user->subscription_code,
                'user_device' => array(
                    'os_name' => $oRide->user->device->os_name,
                    'os_version' => $oRide->user->device->os_version,
                    'notification_id' => $oRide->user->device->notification_id
                )
            );

            $outputObject['car'] = array(
                "car_name" => $oRide->car->car_name,
                "car_id" => $oRide->car->car_id,
                "car_description" => $oRide->car->car_description,
                "car_estimated_price" => $oRide->car->car_estimated_price,
                "eta" => $oRide->car->position->eta);
            $outputObject['driver'] = array(
                "driver_firstname" => $oRide->car->driver->driver_firstname,
                "driver_lastname" => $oRide->car->driver->driver_lastname,
                "driver_photos" => $oRide->car->driver->driver_photos);
        }

        $outputObject["error"] = $error;
        $jsonBody = json_encode($outputObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method POST
     * @provides application/json
     */
    function create($session_id = "") {

        $outputObject = array();
        $code = Tonic\Response::OK;
        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        $data = json_decode($this->request->data, true);

        if (empty($session_id)) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing session_id'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if ($code == Tonic\Response::OK) {
            $oAuth = $this->check_authentication();
            if ($oAuth === false) {
                $error = array(
                    'error_code' => '-3',
                    'error_message' => 'Authentication failed'
                );
                $code = Tonic\Response::CONFLICT;
            }
        }

        $tCodeError = RegulatorRideResource::checkObjectParams($data);
        if ($tCodeError["code"] != Tonic\Response::OK) {
            return new Tonic\Response(Tonic\Response::OK, json_encode($tCodeError["error"]));
        }

        if ($code == Tonic\Response::OK) {
            //create ride position
            $oStartPosition = new Position($data['ride']['start_position']);
            $oEndPosition = new Position($data['ride']['end_position']);
            unset($data['ride']['start_position']);
            unset($data['ride']['end_position']);
            if (empty($oStartPosition->address)) {
                $error = array(
                    'error_code' => '-32',
                    'error_message' => 'Missing ride:start_position:address'
                );
                $code = Tonic\Response::CONFLICT;
            }

            if (empty($oEndPosition->address)) {
                $error = array(
                    'error_code' => '-35',
                    'error_message' => 'Missing ride:end_position:address'
                );
                $code = Tonic\Response::CONFLICT;
            }

            if ($code == Tonic\Response::OK) {
                if ($oStartPosition->is_valid() && $oEndPosition->is_valid()) {
                    //print_r($data['ride']);die;
                    $oRide = new Ride($data['ride']);
                    if (!$oRide->save()) {
                        $tCodeError = RideResource::getRecordErrors($oRide);
                        $error = $tCodeError["error"];
                        $code = $tCodeError["code"];
                    } else {
                        $oUser = User::find_by_session_id($session_id);
                        $oRide->create_start_position($oStartPosition->attributes());
                        $oRide->create_end_position($oEndPosition->attributes());
                        $oRide->start_position_id = $oRide->start_position->position_id;
                        $oRide->end_position_id = $oRide->end_position->position_id;
                        $oRide->set_estimated_data();
                        $oRide->user_id = $oUser->user_id;
                        $oRide->save();
                        $oRide->reload();
                        $oRide->set_car_status();
                    }
                }

                $oRide->reload();

                $outputObject['ride'] = array(
                    "ride_id" => $oRide->ride_id,
                    "ride_status" => $oRide->ride_status,
                    "start_date" => $oRide->start_date,
                    "start_position" => array("longitude" => $oRide->start_position->longitude,
                        "latitude" => $oRide->start_position->latitude,
                        "address" => $oRide->start_position->address),
                    "end_position" => array("longitude" => $oRide->end_position->longitude,
                        "latitude" => $oRide->end_position->latitude,
                        "address" => $oRide->end_position->address),
                    "ride_estimated_time" => $oRide->ride_estimated_time,
                    "ride_estimated_length" => $oRide->ride_estimated_length,
                    "ride_pack_price" => $oRide->ride_pack_price,
                    "ride_minute_price" => $oRide->ride_minute_price,
                    "ride_total_price" => $oRide->ride_total_price
                );
                $outputObject['car'] = array("car_name" => $oRide->car->car_name,
                    "car_description" => $oRide->car->car_description,
                    "car_estimated_price" => $oRide->car->car_estimated_price,
                    "eta" => $oRide->car->position->eta);
                $outputObject['driver'] = array("driver_firstname" => $oRide->car->driver->driver_firstname,
                    "driver_lastname" => $oRide->car->driver->driver_lastname,
                    "driver_photos" => $oRide->car->driver->driver_photos);
            } else {
                if (!$oStartPosition->save()) {
                    $tCodeError = RideResource::getRecordErrors($oStartPosition);
                    $error = $tCodeError["error"];
                    $code = $tCodeError["code"];
                }
                if (!$oEndPosition->save()) {
                    $tCodeError = RideResource::getRecordErrors($oEndPosition);
                    $error = $tCodeError["error"];
                    $code = $tCodeError["code"];
                }
            }
        }
        $outputObject['error'] = $error;
        $jsonBody = json_encode($outputObject);

        $code = Tonic\Response::OK;

        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method PUT
     * @provides application/json
     */
    function update($ride_id = "", $session_id = "") {

        error_reporting(E_ALL);
        ini_set('display_errors', 'On');

        $outputObject = array();

        $code = Tonic\Response::OK;

        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        $data = json_decode($this->request->data, true);

        //$oCar = Car::find($car_id); //get car by id        
        if (empty($session_id)) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing session:session_id'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if (empty($ride_id)) {
            $error = array(
                'error_code' => '-2',
                'error_message' => 'Method not allowed'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if ($code == Tonic\Response::OK) {
            $oAuth = $this->check_authentication();
            if ($oAuth === false) {
                $error = array(
                    'error_code' => '-3',
                    'error_message' => 'Authentication failed'
                );
                $code = Tonic\Response::CONFLICT;
            } else {
                try {
                    $oRide = Ride::find($ride_id);
                } catch (Exception $e) {
                    $error = array(
                        'error_code' => '-1',
                        'error_message' => 'Not found'
                    );
                    $code = Tonic\Response::NOTFOUND;
                }

                if (!$oRide->update_attributes($data['ride'])) {
                    $tCodeError = $RideResource::getRecordErrors($oRide);
                    $error = $tCodeError["error"];
                    $code = $tCodeError["code"];
                } else {
                    if (isset($data['ride']['ride_status']) && !empty($data['ride']['ride_status'])) {
                        $oRide->set_car_status();
                    }
                    if ($oRide->ride_status == 'refused') {
                        $oRide->reallocate();
                    }

                    if ($oRide->ride_status == 'finished') {
                        $oSmarty = new Smarty();
                        $oSmarty->assign('oRide', $oRide->attributes());
                        $oSmarty->assign('oUser', $oRide->user->attributes());
                        $oSmarty->assign('oStartPosition', $oRide->start_position->attributes());
                        $oSmarty->assign('oEndPosition', $oRide->end_position->attributes());
                        $bodyTpl = $oSmarty->fetch(APPLICATION_PATH . '/html/email.tpl');
                        $oEmail = new Email();
                        $oEmail->send('Subject', $oRide->user->user_email, $oRide->user->user_name, DRIVEME_EMAIL, 'driveme', '', $bodyTpl, $zMessage = 'Message', '', '');
                    }
                }
            }
        }

        if ($code == Tonic\Response::OK) {
            $outputObject['ride'] = array('ride_id' => $oRide->ride_id, 'ride_status' => $oRide->ride_status);
        } else {
            $outputObject['ride'] = array('ride_id' => '', 'ride_status' => '');
        }

        $outputObject['error'] = $error;
        $jsonBody = json_encode($outputObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method DELETE
     * @provides application/json
     */
    function destroy($ride_id = "", $session_id = "") {
        $outputObject = array();

        $code = Tonic\Response::OK;

        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        if (empty($session_id)) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing session:session_id'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if (empty($ride_id)) {
            $error = array(
                'error_code' => '-2',
                'error_message' => 'Method not allowed'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if ($code == Tonic\Response::OK && !$this->check_authentication()) {
            $code = Tonic\Response::CONFLICT;
            $error = array(
                'error_code' => '-3',
                'error_message' => 'Authentication failed'
            );
        }

        if ($code == Tonic\Response::OK) {
            try {
                $oRide = Ride::find($ride_id);
                $oRide->delete();
            } catch (Exception $e) {
                $error = array(
                    'error_code' => '-1',
                    'error_message' => 'Not found'
                );
                $code = Tonic\Response::NOTFOUND;
            }
        }

        $outputObject["error"] = $error;
        $jsonBody = json_encode($outputObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    private static function getRecordErrors($_oRide) {
        switch (get_class($_oRide)) {
            case 'Ride':
                if ($_oRide->errors->on('car_id')) {
                    $error = array(
                        "error_code" => "-40",
                        "error_message" => "Missing ride:car_id"
                    );
                }
                if ($_oRide->errors->on('subscription_code')) {
                    $error = array(
                        "error_code" => "-42",
                        "error_message" => "Missing ride:subscription_code"
                    );
                }

                break;

            case 'Position':
                if ($_oBject->errors->on('longitude')) {
                    $error = array(
                        "error_code" => "-30",
                        "error_message" => "Missing ride:car_position:longitude"
                    );
                }
                if ($_oBject->errors->on('latitude')) {
                    $error = array(
                        "error_code" => "-31",
                        "error_message" => "Missing ride:car_position:latitude"
                    );
                }

                break;
        }

        return array("code" => Tonic\Response::CONFLICT, "error" => $error);
    }

    /**
     * check all object params of each HTTP request method
     * must content ride, start and end position params
     * $data request params
     */
    public static function checkObjectParams($data) {

        $error = array(
            'error_code' => '',
            'error_message' => ''
        );
        $code = Tonic\Response::OK;

        if (!isset($data["ride"])) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing ride params'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if (!isset($data["ride"]["start_position"])) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing ride:start_position params'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if (!isset($data["ride"]["end_position"])) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing ride:end_position params'
            );
            $code = Tonic\Response::CONFLICT;
        }

        return array("code" => $code, "error" => $error);
    }

    private function check_authentication() {

        try {
            $session_id = $this->request->params['session_id'];
            if (preg_match('/driver/', substr($this->request->uri, 0, 7))) {
                $oAuth = Driver::find_by_session_id($session_id);
                if (is_object($oAuth)) {
                    $oAuth->persist_authentication();
                }
            } else {
                $oAuth = User::find_by_session_id($session_id);
            }

            if (!is_object($oAuth))
                return false;
        } catch (Exception $e) {
            return false;
        }

        return $oAuth;
    }

}
