<?php

/**
 * @uri /regulator/driver
 * @uri /regulator/driver/:session_id
 * @uri /regulator/driver/:driver_id/:session_id
 */
class RegulatorDriverResource extends Tonic\Resource {

    /**
     * @method GET
     * @provides application/json
     */
    function find($driver_id = '', $session_id = "") {
        $code = Tonic\Response::OK;
        $outputObject = array();
        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        if (empty($session_id)) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing session:session_id'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if (empty($driver_id)) {
            $error = array(
                'error_code' => '-2',
                'error_message' => 'Method not allowed'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if ($code == Tonic\Response::OK) {
            if (!$this->check_authentication()) {
                $error = array(
                    'error_code' => '-3',
                    'error_message' => 'Authentication failed'
                );
                $code = Tonic\Response::CONFLICT;
            }
        }

        if ($code == Tonic\Response::OK) {
            try {
                $oDriver = Driver::find($driver_id); //get driver by id                      
                $outputObject['driver'] = $oDriver->attributes();
            } catch (Exception $e) {
                $error = array(
                    'error_code' => '-1',
                    'error_message' => 'Not found'
                );
                $code = Tonic\Response::NOTFOUND;
            }
        }

        $outputObject['error'] = $error;

        $jsonBody = json_encode($outputObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method POST
     * @provides application/json
     */
    function create($session_id = "") {



        /*      $oPayment = new Payment();

          print_r($oPayment);die; */
        $outputObject = array();

        $code = Tonic\Response::OK;

        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        if (empty($session_id)) {
            $code = Tonic\Response::CONFLICT;

            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing session:session_id'
            );

            return new Tonic\Response($code, json_encode($error));
        }

        if (!$this->check_authentication()) {
            $code = Tonic\Response::CONFLICT;

            $error = array(
                'error_code' => '-3',
                'error_message' => 'Authentication failed'
            );

            return new Tonic\Response($code, json_encode($error));
        }

        $data = json_decode($this->request->data, true);
        $tCodeError = RegulatorDriverResource::checkObjectParams($data);
        if ($tCodeError["code"] != Tonic\Response::OK) {
            return new Tonic\Response($tCodeError["code"], json_encode($tCodeError["error"]));
        }

        $oDriver = new Driver($data["driver"]);

        if (!$oDriver->save()) {
            $tCodeError = RegulatorDriverResource::getRecordErrors($oDriver);
            $error = $tCodeError["error"];
            $code = $tCodeError["code"];
        }

        $outputObject['error'] = $error;
        $jsonBody = json_encode($outputObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method PUT
     * @provides application/json
     */
    function update($driver_id = "", $session_id = "") {

        $outputObject = array();

        $code = Tonic\Response::OK;

        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        $data = json_decode($this->request->data, true);

        if (empty($session_id)) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing session:session_id'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if (empty($driver_id)) {
            $error = array(
                'error_code' => '-2',
                'error_message' => 'Method not allowed'
            );
            $code = Tonic\Response::CONFLICT;
        }


        if ($code == Tonic\Response::OK) {
            if (!$this->check_authentication()) {
                $error = array(
                    'error_code' => '-3',
                    'error_message' => 'Authentication failed'
                );
                $code = Tonic\Response::CONFLICT;
            }
        }

        if ($code == Tonic\Response::OK) {

            try {
                $oDriver = Driver::find($driver_id); //get driver by id    

                if (!$oDriver->update_attributes($data['driver'])) {
                    $tCodeError = RegulatorDriverResource::getRecordErrors($oDriver);
                    $error = $tCodeError["error"];
                    $code = $tCodeError["code"];
                }
            } catch (Exception $e) {
                $error = array(
                    'error_code' => '-1',
                    'error_message' => 'Not found'
                );
                $code = Tonic\Response::NOTFOUND;
            }
        }

        $outputObject['error'] = $error;

        $jsonBody = json_encode($outputObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * get ActiveRecord validation error on fields
     */
    private static function getRecordErrors($_oBject) {

        $error = array(
            "error_code" => "",
            "error_message" => ""
        );

        switch (get_class($_oBject)) {
            case 'Driver':
                if ($_oBject->errors->on('driver_identity')) {
                    $error = array(
                        "error_code" => "-25",
                        "error_message" => "Missing driver:driver:driver_identity"
                    );
                }

                if ($_oBject->errors->on('code_pin')) {
                    $error = array(
                        "error_code" => "-25",
                        "error_message" => "Missing driver:driver:code_pin"
                    );
                }

                break;

            case 'Device':
                if ($_oBject->errors->on('os_name')) {
                    $error = array(
                        "error_code" => "-13",
                        "error_message" => "Wrong device:os_name"
                    );
                }

                break;
        }

        return array("code" => Tonic\Response::CONFLICT, "error" => $error);
    }

    /**
     * check all object params of each HTTP request method
     * must content driver, driver and position params
     * $data request params
     */
    public static function checkObjectParams($data) {

        $error = array(
            'error_code' => '',
            'error_message' => ''
        );
        $code = Tonic\Response::OK;

        if (empty($data["driver"])) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing driver params'
            );
            $code = Tonic\Response::CONFLICT;
        }


//        if (empty($data["driver"]["device"])) {
//            $error = array(
//                'error_code' => '-9',
//                'error_message' => 'Missing driver:device params'
//            );
//            $code = Tonic\Response::CONFLICT;
//        }

        return array("code" => $code, "error" => $error);
    }

    private function check_authentication() {

        try {
            $session_id = $this->request->params['session_id'];
            $oAuth = User::find_by_session_id($session_id);
            if (!is_object($oAuth))
                return false;
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

}
