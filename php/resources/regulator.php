<?php

/**
 * @uri /regulator/authenticate
 * @uri /regulator/logout
 * @uri /regulator/logout/:session_id
 */
class RegulatorResource extends Tonic\Resource {

    /**
     * @method GET
     * @provides application/json
     */
    function methodNotAllowed() {
        $code = Tonic\Response::METHODNOTALLOWED;

        $jsonBody = json_encode(array(
            'error' => array(
                'error_code' => '-2',
                'error_message' => 'Method not allowed',
            )
        ));

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method POST
     * @provides application/json
     */
    function doAuthenticate() {

        $code = Tonic\Response::OK;
        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        $data = json_decode($this->request->data, true);

        if (empty($data['regulator']['user_id'])) {

            $error = array(
                'error_code' => '-50',
                'error_message' => 'Missing Identifiant',
            );
            $code = Tonic\Response::CONFLICT;
        }

        if (empty($data['regulator']['code_pin'])) {

            $error = array(
                'error_code' => '-51',
                'error_message' => 'Missing code_pin',
            );
            $code = Tonic\Response::CONFLICT;
        }

        if (empty($data['regulator']['user_type'])) {

            $error = array(
                'error_code' => '-51',
                'error_message' => 'Missing regulator:user_type',
            );
            $code = Tonic\Response::CONFLICT;
        }

        if ($code == Tonic\Response::OK) {
            $regulator = User::find('first', array(
                        'select' => "*",
                        'conditions' => array(
                            'user_id = ? AND code_pin = ? AND user_type = ?', $data['regulator']['user_id'], $data['regulator']['code_pin'], 'regulator')
                            )
            );


            if (!is_object($regulator)) {
                $error = array(
                    'error_code' => '-3',
                    'error_message' => 'Authentication failed',
                );
                $code = Tonic\Response::CONFLICT;
            } else {
                $session_id = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuxyvwzABCDEFGHIJKLMNOPQRSTUXYVWZ", 5)), 0, 63);
                $regulator->update_attribute('session_id', $session_id);
                $outputObject['session'] = array(
                    'session_id' => $session_id
                );
            }
        }

        $outputObject['error'] = $error;

        $jsonBody = json_encode($outputObject);

        //it's for debug env
        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method DELETE
     * @provides application/json
     */
    public function logout($session_id = "") {

        $code = Tonic\Response::OK;
        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );
        $outputObject = array();

        if (empty($session_id)) {
            $code = Tonic\Response::CONFLICT;
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing param session_id',
            );
        }

        if ($code == Tonic\Response::OK) {
            if ($oRegul = User::find_by_session_id($session_id))
                $oRegul->deconnect();
        }

        $outputObject['error'] = $error;

        $jsonBody = json_encode($outputObject);

        return new Tonic\Response($code, $jsonBody);
    }

}
