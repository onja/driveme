<?php

/**
 * @uri /user
 * @uri /user/:key_id
 * @uri /user/:user_id/:session_id
 */
class UserResource extends Tonic\Resource {

    /**
     * @method GET
     * @provides application/json
     */
    function find($user_id = "", $session_id = "") {

        $code = Tonic\Response::OK;
        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );
        $outPutObject = array();

        if (empty($user_id)) {
            $error = array(
                'error_code' => '-10',
                'error_message' => 'Missing param user_id'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if (empty($session_id)) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing param session_id'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if ($code == Tonic\Response::OK) {
            $oUser = $this->check_authentication();
            if ($oUser === false) {
                $code = Tonic\Response::CONFLICT;
                $error = array(
                    'error_code' => '-3',
                    'error_message' => 'Authentication failed'
                );
            }
        }

        if ($code == Tonic\Response::OK) {
            try {
                $oUser = User::find($user_id);
                $outPutObject['user'] = array(
                    'user_id' => $oUser->user_id,
                    'code_pin' => $oUser->code_pin,
                    'user_priority' => $oUser->user_priority,
                    'user_name' => $oUser->user_name,
                    'user_phone' => $oUser->user_phone,
                    'user_email' => $oUser->user_email,
                    'subscription_code' => $oUser->subscription_code,
                    'user_device' => array(
                        'os_name' => $oUser->device->os_name,
                        'os_version' => $oUser->device->os_version,
                        'notification_id' => $oUser->device->notification_id
                    )
                );
            } catch (Exception $e) {
                $code = Tonic\Response::CONFLICT;
                $error = array(
                    'error_code' => '-1',
                    'error_message' => 'Not found'
                );
            }
        }

        $outPutObject['error'] = $error;
        $jsonBody = json_encode($outPutObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method POST
     * @provides application/json
     */
    function create() {

        $outputObject = array();

        $code = Tonic\Response::OK;

        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        $data = json_decode($this->request->data, true);

        $tCodeError = UserResource::checkObjectParams($data);
        if ($tCodeError["code"] != Tonic\Response::OK) {
            return new Tonic\Response($tCodeError["code"], json_encode($tCodeError["error"]));
        }
        
        if (!isset($data["user"]["user_id"])) {
            $error = array(
                'error_code' => '-10',
                'error_message' => "The field 'user_id' of the object 'user' is missing, and this field is mandatory"
            );
            $code = Tonic\Response::CONFLICT;
        } else {
        
            try {

                $exist = User::find($data["user"]["user_id"]);
                if (is_object($exist)) {
                    $error = array(
                        'error_code' => '-5',
                        'error_message' => 'User already exists',
                    );
                    $code = Tonic\Response::CONFLICT;
                }
            } catch (Exception $e) {
                $oDevice = new Device($data["user"]["user_device"]);
                //$oCredit = new Credit($data['user']['user_credit']);
                unset($data["user"]["user_device"]);
                //unset($data["user"]["user_credit"]);
                $oUser = new User($data['user']);

                $device_conn = Device::connection();
                //$credit_conn = Credit::connection();
                $device_conn->transaction();
                //$credit_conn->transaction();

                if (!$oDevice->save()) {
                    $tCodeError = UserResource::getRecordErrors($oDevice);
                    $error = $tCodeError["error"];
                    $code = $tCodeError["code"];
                } else {
                    $oUser->device_id = $oDevice->device_id;
                    if (!$oUser->save()) {
                        $tCodeError = UserResource::getRecordErrors($oUser);
                        $error = $tCodeError["error"];
                        $code = $tCodeError["code"];
                        $device_conn->rollback();
                    } else {
                        $device_conn->commit();
                        $outputObject['session'] = array(
                            'session_id' => $oUser->session_id,
                            'session_priority' => $oUser->user_priority
                        );
                    }
                }
            }           
        }
        $outputObject['error'] = $error;
        $jsonBody = json_encode($outputObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method PUT
     * @provides application/json
     */
    function update($user_id = "", $session_id = "") {
        $outputObject = array();
        $code = Tonic\Response::OK;
        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        $data = json_decode($this->request->data, true);

        if (empty($user_id)) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing session:session_id'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if (empty($session_id)) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing session:session_id'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if ($code == Tonic\Response::OK) {
            $oUser = $this->check_authentication();
            if ($oUser === false) {
                $error = array(
                    'error_code' => '-3',
                    'error_message' => 'Authentication failed'
                );
                $code = Tonic\Response::CONFLICT;
            }
        }

        if ($code == Tonic\Response::OK) {
            try {
                $oUser = User::find($user_id);

                if (isset($data['user']['user_device'])) {
                    $tDeviceParam = $data['user']['user_device'];
                    unset($data['user']['user_device']);
                }


                if (!$oUser->update_attributes($data["user"])) {
                    $tCodeError = UserResource::getRecordErrors($oUser);
                    $error = $tCodeError["error"];
                    $code = $tCodeError["code"];
                }

                if (isset($tDeviceParam)) {
                    if (!$oUser->device->update_attributes($tDeviceParam)) {
                        $tCodeError = UserResource::getRecordErrors($oUser->device);
                        $error = $tCodeError["error"];
                        $code = $tCodeError["code"];
                    }
                }
            } catch (Exception $e) {
                $error = array(
                    'error_code' => '-1',
                    'error_message' => 'Not found: ' . $e->getTraceAsString()
                );
                $code = Tonic\Response::NOTFOUND;
            }
        }

        $outputObject['error'] = $error;
        $jsonBody = json_encode($outputObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * get ActiveRecord validation error on fields
     */
    private static function getRecordErrors($_oBject) {
        $error = array(
            "error_code" => "",
            "error_message" => ""
        );

        switch (get_class($_oBject)) {
            case 'User':
                if ($mess = $_oBject->errors->on('user_type')) {
                    $error = array(
                        "error_code" => "-11",
                        "error_message" => is_array($mess) ? $mess[0] : $mess
                    );
                }
                if ($mess = $_oBject->errors->on('user_priority')) {
                    $error = array(
                        "error_code" => "-12",
                        "error_message" => is_array($mess) ? $mess[0] : $mess
                    );
                }
                break;

            case 'Device':
                if ($mess = $_oBject->errors->on('os_name')) {
                    $error = array(
                        "error_code" => "-13",
                        "error_message" => is_array($mess) ? $mess[0] : $mess
                    );
                }
                if ($mess = $_oBject->errors->on('os_version')) {
                    $error = array(
                        "error_code" => "-14",
                        "error_message" => $mess
                    );
                }
                break;
        }

        return array("code" => Tonic\Response::CONFLICT, "error" => $error);
    }

    /**
     * check all object params of each HTTP request method
     * must content user and device params
     * $data request params
     */
    public static function checkObjectParams($data) {

        $error = array(
            'error_code' => '0',
            'error_message' => ''
        );

        $code = Tonic\Response::OK;

        if (empty($data["user"])) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing user params'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if (empty($data["user"]["user_device"])) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing user:user_device params'
            );
            $code = Tonic\Response::CONFLICT;
        }

        return array("code" => $code, "error" => $error);
    }

    private function check_authentication() {
        try {
            $session_id = $this->request->params['session_id'];
            $oUser = User::find_by_session_id($session_id);
            if (!is_object($oUser))
                return false;
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

}
