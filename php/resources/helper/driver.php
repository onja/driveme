<?php

/**
 * @uri /regulator/helper/drivers/:session_id
 */
class DriverHelper extends Tonic\Resource {

    /**
     * @method PUT
     * @method POST
     * @method DELETE
     * @provides application/json
     */
    function methodNotAllowed() {

        $code = Tonic\Response::METHODNOTALLOWED;
        $outputObject = array();
        $error = array(
            'error_code' => '-2',
            'error_message' => 'Method not allowed',
        );
        $outputObject['error'] = $error;
        $jsonBody = json_encode($outputObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method GET
     * @provides application/json
     */
    function find($session_id = "") {

        $code = Tonic\Response::OK;
        $outputObject = array();
        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        $data = json_decode($this->request->data, true);

        if (empty($session_id)) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing session_id'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if (!$this->check_authentication()) {
            $error = array(
                'error_code' => '-3',
                'error_message' => 'Authentication failed'
            );
            $code = Tonic\Response::CONFLICT;
        }

        if ($code == Tonic\Response::OK) {
            $toDriverRecord = Driver::find("all");
            $toDriver = array();
            foreach ($toDriverRecord as $key => $oDriverRecord) {
                $toDriver[] = $oDriverRecord->attributes();
                $outputObject['drivers'] = $toDriver;
            }
        }

        $outputObject['error'] = $error;

        $jsonBody = json_encode($outputObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    private function check_authentication() {
        try {
            $session_id = $this->request->params['session_id'];
            $oUser = User::find_by_session_id($session_id);
            if (!is_object($oUser))
                return false;
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

}
