<?php

/**
 * @uri /driver/helper/reservations/:session_id
 */
class DriverReservationHelper extends Tonic\Resource {

    /**
     * @method PUT
     * @method POST
     * @method DELETE
     * @provides application/json
     */
    function methodNotAllowed() {

        $code = Tonic\Response::METHODNOTALLOWED;
        $outputObject = array();
        $error = array(
            'error_code' => '-2',
            'error_message' => 'Method not allowed',
        );
        $outputObject['error'] = $error;
        $jsonBody = json_encode($outputObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method GET
     * @provides application/json
     */
    function find($session_id = "") {

        $code = Tonic\Response::OK;
        $outputObject = array();
        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        $data = json_decode($this->request->data, true);

        if (empty($session_id)) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing session_id'
            );
            $code = Tonic\Response::CONFLICT;
        }

         if( $code == Tonic\Response::OK ){
            $oDriver = $this->check_authentication() ;
            if($oDriver === false){
                $error = array(
                    'error_code' => '-3',
                    'error_message' => 'Authentication failed'
                );
                $code = Tonic\Response::CONFLICT;
            }

        }

        if ($code == Tonic\Response::OK) {
            $toReservationRecord = Reservation::find("all", array(
                            'conditions' => array(
                                'driver_identity = ? AND reservation_status = ?', $oDriver->driver_identity, 'pending')
                    
                ));
            $toReservation = array();
            foreach ($toReservationRecord as $key => $oDriver) {
                $toReservation[] = $oDriver->fetchApiObject();
                $outputObject['reservations'] = $toReservation;
            }
        }

        $outputObject['error'] = $error;

        $jsonBody = json_encode($outputObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    private function check_authentication() {
        try {
            $session_id = $this->request->params['session_id'];
            $oDriver = Driver::find_by_session_id($session_id);
            if (!is_object($oDriver))
                return false;
        } catch (Exception $e) {
            return false;
        }

        return $oDriver;
    }

}
