<?php

/**
 * @uri /helper/cars/:session_id
 */
class CarHelper extends Tonic\Resource {

    /**
     * @method PUT
     * @method GET
     * @provides application/json
     */
    function methodNotAllowed() {

        $code = Tonic\Response::METHODNOTALLOWED;
        $outputObject = array();
        $error = array(
            'error_code' => '-2',
            'error_message' => 'Method not allowed',
        );
        $outputObject['error'] = $error;
        $jsonBody = json_encode($outputObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method POST
     * @provides application/json
     */
    function find($session_id = "") {


        $code = Tonic\Response::OK;
        $outputObject = array();
        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );
        $array_car = array();
        $data = json_decode($this->request->data, true);

        if (empty($session_id)) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing session_id'
            );
            $code = Tonic\Response::CONFLICT;
            return new Tonic\Response($code, json_encode($error));
        }

        if (!$this->check_authentication()) {
            $error = array(
                'error_code' => '-3',
                'error_message' => 'Authentication failed'
            );
            $code = Tonic\Response::OK;
            return new Tonic\Response($code, json_encode($error));
        }

        if (!preg_match('/regulator/', substr($this->request->uri, 0, 10))) {
            $limit = "LIMIT 0, 1";
        } else {
            $limit = "";
        }

        $tSqlConditions = $this->formatConditions($data);
        $conditions = $tSqlConditions['conditions'];
        
        if (isset($data['car']['start_position'])) {
            $oNearlyCar = Car::get_available_nearly($data);
            $outputObject['car'] = $oNearlyCar;
            $toActiveRecordCar = $oNearlyCar;
        } else {

            $toActiveRecordCar = Car::find_by_sql("
                    SELECT
                        c.*
                    FROM
                        rc_cars as c
                    INNER JOIN
                        rc_positions as p
                    ON
                        p.position_id =c.position_id
                    $conditions
                    $limit
                ");

            foreach ($toActiveRecordCar as $key => $oActiveRecordCar) {

                if (isset($oActiveRecordCar->driver->driver_firstname)) {
                    $driver = array(
                        "driver_firstname" => $oActiveRecordCar->driver->driver_firstname,
                        "driver_lastname" => $oActiveRecordCar->driver->driver_lastname
                    );
                } else {
                    $driver = array();
                }

                $array_car[] = array(
                    "car" => array(
                        "car_id" => $oActiveRecordCar->car_id,
                        "car_name" => $oActiveRecordCar->car_name,
                        "car_status" => $oActiveRecordCar->car_status,
                        "connection_status" => $oActiveRecordCar->connection_status,
                        "car_position" => array(
                            "longitude" => $oActiveRecordCar->position->longitude,
                            "latitude" => $oActiveRecordCar->position->latitude,
                            "ETA" => $oActiveRecordCar->position->eta
                        ),
                        "car_level" => $oActiveRecordCar->car_level,
                        "car_price" => $oActiveRecordCar->car_price,
                        "car_estimated_price" => $oActiveRecordCar->car_estimated_price,
                    ),
                    "driver" => $driver
                );
            }

            $outputObject['cars'] = $array_car;
        }


//        if (count($toActiveRecordCar) <= 0) {
//            $error = array(
//                'error_code' => '-1',
//                'error_message' => 'Not found'
//            );
//            $code = Tonic\Response::OK;
//            return new Tonic\Response($code, json_encode($error), array('Content-Type' => 'application/json; charset=UTF-8'));
//        }

        $outputObject['error'] = $error;

        $jsonBody = json_encode($outputObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody, array('Content-Type' => 'application/json; charset=UTF-8'));
    }

    private function formatConditions($data) {
        
        $zConditions = "";
        $zValues = "";
        $error = array(
            'error_code' => '0',
            'error_message' => ''
        );


        if (!isset($data["car"]["start_position"]["longitude"])) {
            $error = array(
                'error_code' => '-30',
                'error_message' => 'car:start_position:longitude'
            );
        }
        if (!isset($data["car"]["start_position"]["latitude"])) {
            $error = array(
                'error_code' => '-31',
                'error_message' => 'car:start_position:latitude'
            );
        }

        if (isset($data["car"]["car_status"])) {
            if (!preg_match('/^(all|available|not-available|ride|pick-up|pause)$/', $data['car']['car_status'])) {
                $error = array(
                    'error_code' => '-36',
                    'error_message' => 'Wrong value for car_status',
                );
                $code = Tonic\Response::CONFLICT;
            } else {

                if ($zConditions == "") {
                    if (!preg_match('/regulator/', substr($this->request->uri, 0, 10))) {
                        $zConditions .= "WHERE c.car_status = \"available\"";
                        $zValues .= "";
                    } else {
                        if ($data["car"]["car_status"] != 'all') {
                            $zConditions .= "WHERE c.car_status = '" . $data["car"]["car_status"] . "'";
                            $zValues .= "";
                        }
                    }
                } else {
                    if (!preg_match('/regulator/', substr($this->request->uri, 0, 10))) {
                        $zConditions .= " AND c.car_status = \"available\"";
                        $zValues .= "";
                    } else {
                        if ($data["car"]["car_status"] != 'all') {
                            $zConditions .= " AND c.car_status = \"" . $data["car"]["car_status"] . "\"";
                            $zValues .= "";
                        }
                    }
                }
            }
        }

        if (isset($data["car"]["car_level"])) {
            if (!preg_match('/^(all|basic|premium)$/', $data['car']['car_level'])) {
                $error = array(
                    'error_code' => '-37',
                    'error_message' => 'Wrong value for car_level',
                );
                $code = Tonic\Response::CONFLICT;
            } else {
                if ($zConditions == "") {
                    if ($data["car"]["car_level"] != 'all') {
                        $zConditions .= "WHERE c.car_level = \"" . $data["car"]["car_level"] . "\"";
                        $zValues .= "";
                    }
                } else {
                    if ($data["car"]["car_level"] != 'all') {
                        $zConditions .= " AND c.car_level = \"" . $data["car"]["car_level"] . "\"";
                        $zValues .= "";
                    }
                }
            }
        }

        if (isset($data["car"]["connection_status"])) {
            if (!preg_match('/^(available|not\-available)$/', $data['car']['connection_status'])) {
                $error = array(
                    'error_code' => '-38',
                    'error_message' => 'Wrong value for connection_status',
                );
                $code = Tonic\Response::CONFLICT;
            } else {
                if ($zConditions == "") {
                    if ($data["car"]["connection_status"] != 'all') {
                        $zConditions .= "WHERE c.connection_status = \"" . $data["car"]["connection_status"] . "\"";
                        $zValues .= "";
                    }
                } else {
                    if ($data["car"]["connection_status"] != 'all') {
                        $zConditions .= " AND c.connection_status = \"" . $data["car"]["connection_status"] . "\"";
                        $zValues .= "";
                    }
                }
            }
        } else{
            if ($zConditions == "") {

                    $zConditions .= "WHERE c.connection_status = 'available'";
                    $zValues .= "";

            } else {

                    $zConditions .= " AND c.connection_status = 'available'";
                    $zValues .= "";

            }
        }       

        return array('conditions' => $zConditions, 'values' => $zValues, 'error' => $error);
    }

    private function distance_matrix($origin_lat, $origin_lng, $origin_adr, $dest_lat, $dest_lng, $dest_adr) {
        $url = "http://maps.googleapis.com/maps/api/distancematrix/json?origins=$origin_lat,$origin_lng|$origin_adr&destinations=$dest_lat,$dest_lng|$dest_adr&mode=driving&language=fr-FR&sensor=false";
        //print_r ($url);die;
        $body = file_get_contents($url);
        $data = json_decode($body);
        $oDistanceMatrix = array_shift($data->rows);
        return $distancematrix = array_shift($oDistanceMatrix->elements);
    }

    private function check_authentication() {

        try {

            $session_id = $this->request->params['session_id'];
            //print_r($session_id);die;
            $oUser = User::find_by_session_id($session_id);
            //print_r($oUser);die;
            if (!is_object($oUser))
                return false;
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

}
