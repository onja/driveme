<?php

/**
 * @uri /regulator/helper/reservations/:session_id
 */
class RegulatorReservationHelper extends Tonic\Resource {

    /**
     * @method PUT
     * @method DELETE
     * @provides application/json
     */
    function methodNotAllowed() {

        $code = Tonic\Response::METHODNOTALLOWED;
        $outputObject = array();
        $error = array(
            'error_code' => '-2',
            'error_message' => 'Method not allowed',
        );
        $outputObject['error'] = $error;
        $jsonBody = json_encode($outputObject);
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method POST
     * @provides application/json
     */
    function find($session_id = "") {

        $code = Tonic\Response::OK;
        $outputObject = array();
        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        $data = json_decode($this->request->data, true);

        if (empty($session_id)) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing session:session_id'
            );
            $code = Tonic\Response::CONFLICT;
        }


        if ($code = Tonic\Response::OK) {
            $oAuth = $this->check_authentication();
            if ($oAuth === false) {
                $error = array(
                    'error_code' => '-9',
                    'error_message' => 'Authentication failed'
                );
                $code = Tonic\Response::CONFLICT;
            }
        }


        if ($code == Tonic\Response::OK) {
            $toReservations = Reservation::find('all');
            //print_r($toReservations);die;
            foreach($toReservations as $oReservation){
                //print_r($oReservation);die;
                $reservations[]['reservation'] = $oReservation->fetchApiObject();
            }
        }

        $outputObject['reservations'] = $reservations;
        //print_r($outputObject);die;
        $outputObject['error'] = $error;

        $jsonBody = json_encode($outputObject);

        return new Tonic\Response($code, $jsonBody);
    }

    private function check_authentication() {
        try {
            $session_id = $this->request->params['session_id'];
            $oAuth = User::find_by_session_id($session_id);
            if (!is_object($oAuth))
                return false;
        } catch (Exception $e) {
            return false;
        }

        return $oAuth;
    }

}
