<?php

/**
 * @uri /helper/rides/:session_id
 * @uri /driver/helper/rides/:session_id
 */
class RideHelper extends Tonic\Resource {

    /**
     * @method PUT
     * @method DELETE
     * @provides application/json
     */
    function methodNotAllowed() {

        $code = Tonic\Response::METHODNOTALLOWED;
        $outputObject = array();
        $error = array(
            'error_code' => '-2',
            'error_message' => 'Method not allowed',
        );
        $outputObject['error'] = $error;
        $jsonBody = json_encode($outputObject);
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method POST
     * @provides application/json
     */
    function find($session_id = "") {

        $code = Tonic\Response::OK;
        $outputObject = array();
        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        $data = json_decode($this->request->data, true);

        if (empty($session_id)) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing session:session_id'
            );
            $code = Tonic\Response::CONFLICT;
        }


        if ($code = Tonic\Response::OK) {
            $oAuth = $this->check_authentication();
            if ($oAuth === false) {
                $error = array(
                    'error_code' => '-9',
                    'error_message' => 'Authenication failed'
                );
                $code = Tonic\Response::CONFLICT;
            }
        }


        if ($code == Tonic\Response::OK) {

            if (preg_match('/driver/', substr($this->request->uri, 0, 7))) {
                $oCar = Car::find_by_driver_identity($oAuth->driver_identity);
                $myCar = $oCar->attributes();

                $toActiveRecordRides = Ride::find(
                                "all", array(
                            'joins' => 'INNER JOIN rc_cars as c ON c.car_id = rc_rides.car_id INNER JOIN rc_users as u ON u.user_id = rc_rides.user_id',
                            'conditions' => array(
                                'rc_rides.car_id = ? AND ride_status NOT IN (?)', $myCar['car_id'], array('refused', 'canceled', 'finished')
                            )
                                )
                );

                $toRides = null;
            } else {
                $toActiveRecordRides = Ride::all(
                                array(
                                    'joins' => 'INNER JOIN rc_cars as c ON c.car_id = rc_rides.car_id INNER JOIN rc_users as u ON u.user_id = rc_rides.user_id',
                                    'conditions' => ''
                                )
                );
                $toRides = array();
            }

            if (isset($toActiveRecordRides)) {

                foreach ($toActiveRecordRides as $key => $oActiveRecordRide) {

                    $array_start_position = array();
                    $array_end_position = array();
                    if ($oActiveRecordRide->start_position) {
                        $array_start_position = array('longitude' => $oActiveRecordRide->start_position->longitude, 'latitude' => $oActiveRecordRide->start_position->latitude, 'address' => $oActiveRecordRide->start_position->address);
                    }
                    if($oActiveRecordRide->end_position){
                        $array_end_position = array('longitude' => $oActiveRecordRide->end_position->longitude, 'latitude' => $oActiveRecordRide->end_position->latitude, 'address' => $oActiveRecordRide->end_position->address);    
                    }
                    
                    $array_rides = $oActiveRecordRide->attributes();
                    unset($array_rides['current_position_id'], $array_rides['start_position_id'], $array_rides['end_position_id']);

                    $oCar = $oActiveRecordRide->car;
                    $oUser = User::find_by_subscription_code($oActiveRecordRide->subscription_code);
                    if (isset($oUser)) {
                        $user = $oUser->attributes();
                    } else {
                        $user = array();
                    }
                    if (preg_match('/driver/', substr($this->request->uri, 0, 7))) {
                        $toRides = array_merge(
                                $array_rides, array('start_position' => $array_start_position), array('end_position' => $array_end_position), array('car' => array_merge($oCar->attributes(), array('position' => $oCar->position->attributes()))), array('user' => $user)
                        );
                    } else {
                        $toRides[] = array_merge(
                                $array_rides, array('start_position' => $array_start_position), array('end_position' => $array_end_position), array('car' => array_merge($oCar->attributes(), array('position' => $oCar->position->attributes()))), array('user' => $user)
                        );
                    }
                }
            }
        }
        
        if ($code == Tonic\Response::OK) {
            if (preg_match('/driver/', substr($this->request->uri, 0, 7))) {
                $outputObject['ride'] = $toRides;
            } else {
                $outputObject['rides'] = $toRides;
            }
        }

        $outputObject['error'] = $error;

        $jsonBody = json_encode($outputObject);

        return new Tonic\Response($code, $jsonBody);
    }

    private function check_authentication() {
        try {
            $session_id = $this->request->params['session_id'];
            if (preg_match('/driver/', substr($this->request->uri, 0, 7))) {
                $oAuth = Driver::find_by_session_id($session_id);
                if (is_object($oAuth)) {
                    $oAuth->persist_authentication();
                }
            } else {
                $oAuth = User::find_by_session_id($session_id);
            }

            if (!is_object($oAuth))
                return false;
        } catch (Exception $e) {
            return false;
        }

        return $oAuth;
    }

}
