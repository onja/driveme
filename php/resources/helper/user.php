<?php

/**
 * @uri /helper/users/:session_id
 */
class UserHelper extends Tonic\Resource {

    /**
     * @method PUT
     * @method GET
     * @provides application/json
     */
    function methodNotAllowed() {

        $code = Tonic\Response::METHODNOTALLOWED;
        $outputObject = array();
        $error = array(
            'error_code' => '-2',
            'error_message' => 'Method not allowed',
        );
        $outputObject['error'] = $error;
        $jsonBody = json_encode($outputObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    /**
     * @method POST
     * @provides application/json
     */
    function find($session_id = "") {

        $code = Tonic\Response::OK;
        $outputObject = array();
        $error = array(
            'error_code' => '0',
            'error_message' => '',
        );

        $data = json_decode($this->request->data, true);

        if (empty($session_id)) {
            $error = array(
                'error_code' => '-9',
                'error_message' => 'Missing session_id'
            );
            $code = Tonic\Response::CONFLICT;
        }
        //print_r($session_id);die;
        if (!$this->check_authentication()) {
            $error = array(
                'error_code' => '-3',
                'error_message' => 'Authentication failed'
            );
            $code = Tonic\Response::CONFLICT;
        } else {
            $toActiveRecordUsers = User::find(
                            "all", array('joins' => 'INNER JOIN rc_devices as d ON d.device_id = rc_users.device_id')
            );
            $toUsers = array();
            foreach ($toActiveRecordUsers as $key => $oActiveRecordUser) {
                $array_device = $oActiveRecordUser->device->attributes();
                array_shift($array_device);
                $array_users = $oActiveRecordUser->attributes();
                unset($array_users['device_id']);
                $toUsers[] = array_merge($array_users, array('user_device' => $array_device));
            }
        }

        if ($code == Tonic\Response::OK) {
            $outputObject['users'] = $toUsers;
        }

        $outputObject['error'] = $error;

        $jsonBody = json_encode($outputObject);

        $code = Tonic\Response::OK;
        return new Tonic\Response($code, $jsonBody);
    }

    private function check_authentication() {
        try {
            $session_id = $this->request->params['session_id'];
            $oUser = User::find_by_session_id($session_id);
            if (!is_object($oUser))
                return false;
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

}
