<?php
  
  class DriverPlaning extends ActiveRecord\Model { 

    static $table_prefix = DATABASE_PREFIX ;
    static $table_name  = "driver_planings" ;
    static $primary_key = "id"; 

    static $belongs_to = array(
    	array('driver', 'foreign_key' => 'driver_identity', 'class_name' => 'Driver')
    );
  }
