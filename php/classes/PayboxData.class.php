<?php

class PayboxData extends ActiveRecord\Model {

    static $table_prefix = DATABASE_PREFIX;
    static $table_name = "paybox_data";
    static $primary_key = 'id';
    static $belongs_to = array(
        array('user', 'foreign_key' => 'user_id')
    );   

}
