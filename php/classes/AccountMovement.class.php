<?php
   
  class AccountMovement extends ActiveRecord\Model { 
    static $table_prefix = DATABASE_PREFIX ;
    static $table_name  = "account_movements" ;
    static $primary_key = 'account_mvt_id' ;
    
    static $validates_presence_of = array(
      array('account_mvt_op_id', 'message' => "Missing user:account_mvt_op_id")
    );
    
       
    static $belongs_to = array(
    		array("credit", 'foreign_key' => 'credit_id', "class_name" => "Credit")
    	);
  }
