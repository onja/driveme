<?php
   
  class Credit extends ActiveRecord\Model { 
    static $table_prefix = DATABASE_PREFIX ;
    static $table_name  = "credits" ;
    static $primary_key = 'credit_id' ;

    static $belongs_to = array(
      array('user', 'foreign_key' => 'user_id', 'class_name' => 'User')
    );

    static $has_many = array(
      array('account_movements', 'class_name' => 'AccountMovement')
    );

    //debite user credits
    public function debite($iMinutes){
    	$iDebite = $this->credit_amount - $iMinutes ;
    	$this->update_attributes(array(
    		'credit_amount' => $iDebite,
    		'updated_at' => date('Y-m-d h:i:s')
    	));
    }

    //credite user credits
    public function credite($iMinutes){
    	$iCredite = $this->credit_amount + $iMinutes ;
    	$this->update_attributes(array(
    		'credit_amount' => $iCredite,
    		'updated_at' => date('Y-m-d h:i:s')
    	));
    }    
        
  }
