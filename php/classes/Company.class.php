<?php
   
  class Company extends ActiveRecord\Model { 
    static $table_prefix = DATABASE_PREFIX ;
    static $table_name  = "companies" ;
    static $primary_key = 'company_id' ;
    
    static $validates_presence_of = array(
      array('company_name', 'message' => "Missing company:company_name")
    );
      
    static $before_create = array('generate_subscription_code'); # new records only

    public function generate_subscription_code(){   
      if(!empty($this->subscription_code))
        return true ;   
      $code = "";
      while ( strlen($code) <= 9) {
        $code .= rand(0, 9);
      }
      
      $tExistingCompany = Company::find('all', array('conditions' => array('subscription_code = ?', $code)));
      
      if(count($tExistingCompany)>0)
        $this->generate_subscription_code();
        
      $this->subscription_code = $code;
      
      return $code;

    }    
    

  }
