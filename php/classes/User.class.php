<?php
   
  class User extends ActiveRecord\Model { 

    static $table_prefix = DATABASE_PREFIX ;
    static $table_name  = "users" ;
    static $primary_key = 'user_id' ;

    static $belongs_to = array(
      array('device', 'foreign_key' => 'device_id')
    );
    
    static $has_one = array(
      array('credit', 'foreign_key' => 'user_id')      
    );

    static $has_many = array(
      array('ride', 'class_name' => 'Ride'),
      array('paybox_data', 'foreign_key' => 'user_id', 'class_name' => 'PayboxData'),
      array('cards', 'foreign_key' => 'user_id', 'class_name' => 'Card')
    );
   
    static $validates_presence_of = array(
      array('user_id', 'message' => "Missing user:user_id"),
      array('user_type', 'message' => "Missing user:user_type")
    );

    static $validates_inclusion_of = array(
      array('user_type', 'in' => array('passenger', 'driver', 'regulator'), 'message' => 'Wrong value for user:user_type'),
      array('user_priority', 'in' => array('high', 'low', 'normal'), 'message' => 'Wrong value for user:user_priority'),
    );

    static $before_create = array('generate_subscription_code', 'create_session'); # new records only
    static $after_create = array('create_credit');
    
    public function generate_subscription_code(){     

      if(!empty($this->subscription_code))
        return true ;
      $code = "";
      while ( strlen($code) <= 9) {
        $code .= rand(0, 9);
      }
      $tExistingCode = User::find('all', array('conditions' => array("subscription_code = ?", $code)));
      if (count($tExistingCode)>0) {
        $this->generate_subscription_code();
      }      
      $this->subscription_code = $code;

      return $code;
    }    

    //create a new session on create
    public function create_session(){
      $session_id = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuxyvwzABCDEFGHIJKLMNOPQRSTUXYVWZ", 5)), 0, 63);
      $this->session_id = $session_id ;      
    }   
    
    public function create_credit(){ 
      Credit::create(array(
          'user_id' => $this->user_id,
          'credit_amount' => 0
        ));
    }
    
    public function user_device (){
        return $this->device;
    }

    public function deconnect (){
        return $this->update_attribute('session_id', NULL);
    }

  }
