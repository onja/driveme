<?php

/**
 * Description of Log
 * 
 * @author greggy.tel
 */
class SmartLog {
    
    public static $LEVEL_NO     = 0;
    public static $LEVEL_ERROR  = 1;
    public static $LEVEL_WARN   = 2;
    public static $LEVEL_INFO   = 3;
    public static $LEVEL_DEBUG  = 4;
    
    public static $REQUEST      = 'REQUEST';
    public static $RESPONSE       = 'RESPONSE';
    
    private $level_name_array;
    private $file_path;
    private $current_log_path;
    private $max_file_size;
    private $max_number_of_files;
    private $level;
    private $enable = 1;
    
    /**
     * Constructor
     * 
     * @param string $file_path - path of the log
     * @param int $max_file_size - maximum file size in Bytes
     * @param int $max_number_of_files - maximum number of files
     */
    public function __construct($enable, $level, $file_path, $max_file_size = 10485760, $max_number_of_files = 2) {

        $this->enable = $enable;
        $this->file_path = $file_path;
        $this->max_file_size = $max_file_size;
        $this->max_number_of_files = $max_number_of_files;
        $this->level = $level;
        
        $this->level_name_array = array(
            '',
            $this->truncate('error', 9),
            $this->truncate('warn',  9),
            $this->truncate('info',  9),
            $this->truncate('debug', 9),
        );
        
    }
    

    public function logRaw($rawData) {
        $this->_log(SmartLog::$LEVEL_NO, '', $rawData);
    }

    public function logRealRaw($rawData) {
        $this->_rawlog($rawData);
    }
    
    public function error($title, $description) {
        if($this->level >= SmartLog::$LEVEL_ERROR) {
            $this->_log(SmartLog::$LEVEL_ERROR, $title, $description);
        }
    }
    
    public function warn($title, $description) {
        if($this->level >= SmartLog::$LEVEL_WARN) {
            $this->_log(SmartLog::$LEVEL_WARN, $title, $description);
        }
    }
    
    public function info($title, $description) {
        if($this->level >= SmartLog::$LEVEL_INFO) {
            $this->_log(SmartLog::$LEVEL_INFO, $title, $description);
        }
    }
    
    public function debug($title, $description) {
        if($this->level >= SmartLog::$LEVEL_DEBUG) {
            $this->_log(SmartLog::$LEVEL_DEBUG, $title, $description);
        }
    }
    
    /**
     * log
     * 
     * @param type $description
     * @param type $level
     * @param type $title
     */
    public function _log($level, $title, $description) {        
        if($this->enable) {
            $str = '';

            $hasChanged = $this->selectLogFile();

            // Log level
            $str .= $this->level_name_array[$level];

            // Date (ex: 20130329 16:05:28.417785)
            $str .= $this->truncate($this->getTimestamp(), 30);

            // title
            if(!empty($title)) {
                $str .= $this->truncate($title, 30) . ' - ';
            } else {
                $str .= "\n";
            }

            // description
            $str .= $description;

            // reformat lines
            if(!empty($title) && strpos($str, "\n") !== FALSE) {

                $lines = explode("\n", $str);
                $formatedStr = $lines[0] . "\n";
                for($i = 1 ; $i<count($lines) ; $i++) {
                    $formatedStr .= str_pad('', 72) . $lines[$i] . "\n";
                }

                $str = $formatedStr;            
            }

            if($hasChanged) {
                $this->write($str, '', 'w');
            } else {
                $this->write($str);
            }
        }
    }
    
    public function _rawlog($str) {        
        if($this->enable) {

            $hasChanged = $this->selectLogFile();

            if($hasChanged) {
                $this->write($str, '', 'w');
            } else {
                $this->write($str);
            }
        }
    }
    
    private function getLogFile($i = -1) {
        
        if($i != -1) {
            $this->current_log_path = preg_replace('/\.log$/', '_' . $i . '.log', $this->file_path);
        } else {
        
            if(!file_exists('/tmp/' . basename($this->file_path) . '.current')) {
                $this->setLogFile(0);
            } else {
                $this->current_log_path = file_get_contents('/tmp/' . basename($this->file_path) . '.current');
            } 
        }
        return $this->current_log_path;
    }
    
    private function getLogFileNumber() {
        
        if(preg_match('/_([0-9]*).log$/', $this->getLogFile(), $matches)) {
            $number = $matches[1];
        } else {
            $number = 0;
        }
        return $number;

    }
    
    private function setLogFile($i) {
        
        $this->current_log_path = $this->getLogFile($i);
        $this->write($this->current_log_path, '/tmp/' . basename($this->file_path) . '.current', 'w', FALSE);

    }
    
    private function write($str, $file_path = '', $mode = 'a', $withCarriageReturn = TRUE) {
        if($withCarriageReturn) {
            $end = "\r\n";
        } else {
            $end = "";
        }
        if(empty($file_path)) {
            $file_path = $this->getLogFile();
        }

        if(!file_exists($file_path)) {            
            @mkdir(pathinfo($file_path, PATHINFO_DIRNAME), 0777, TRUE);
        }
        $fp = fopen($file_path, $mode);
        if($fp !== FALSE) {
            fwrite($fp, $str . $end);
            fclose($fp);
        } else {
//            error_log('############## SmartLog DEBUG : BEGIN');
//            error_log('### TRYING TO WRITE THE FOLLOWING CONTENT: ' .  $str . $end);
//            error_log(var_export(debug_backtrace(), true));
//            error_log('############## SmartLog DEBUG : END');
        }
    }
    
    /**
     * selectLogFile
     * 
     * @return boolean
     */
    private function selectLogFile() {
        
        $hasChanged = FALSE;
        $this->current_log_path = $this->getLogFile();
        $index = $this->getLogFileNumber();
        clearstatcache(TRUE, $this->current_log_path);
        
        if(file_exists($this->current_log_path) && filesize($this->current_log_path) > $this->max_file_size) {
            $hasChanged = TRUE;
            $index++;
            
            if($index>=$this->max_number_of_files) {
                $index=0;
            }
            
        }

        $this->setLogFile($index);
        
        return $hasChanged;
    }
    
    private function truncate($str, $length) {
        if(strlen($str)>$length) {
             $str = substr($str, 0, $length + 1);
        } elseif(strlen($str)<$length) {
            $str = str_pad($str, $length);
        }
        return $str;
    }
    
    private function getTimestamp(){
        return date('Y-m-d H:i:s.') . str_pad(substr((float)microtime(), 2), 6, '0', STR_PAD_LEFT);
    }

}