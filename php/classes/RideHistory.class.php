<?php
  
  class RideHistory extends ActiveRecord\Model { 

    static $table_prefix = DATABASE_PREFIX ;
    static $table_name  = "ride_histories" ;
    static $primary_key = 'id' ;   

    static $belongs_to = array(
    	array('ride', 'foreign_key' => 'ride_id', 'class_name' => 'Ride')
    );
    
  }
