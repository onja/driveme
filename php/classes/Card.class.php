<?php
  
  class Card extends ActiveRecord\Model { 

    static $table_prefix = DATABASE_PREFIX ;
    static $table_name  = "cards" ;
    static $primary_key = 'card_id' ;   

    static $belongs_to = array(
        array('user', 'foreign_key' => 'user_id', 'class_name'=>'User')
    );
   
  }
