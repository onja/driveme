<?php
  
  class Reservation extends ActiveRecord\Model {
    static $table_prefix = DATABASE_PREFIX ;
    static $table_name  = "reservations" ;
    static $primary_key = 'reservation_id' ; 
    
    static $belongs_to = array(
      array('user','foreign_key' => 'user_id'),  
      array('driver','foreign_key' => 'driver_identity'),
      array('start_position', 'foreign_key' => 'start_position_id', 'class_name' => 'Position', 'dependent' => 'delete'),
        array('end_position', 'foreign_key' => 'end_position_id', 'class_name' => 'Position', 'dependent' => 'delete'),
      array('ride','foreign_key' => 'ride_id', 'class_name' => 'Ride')
    );

    static $after_create = array('send_push_to_driver', 'set_reference');
    static $after_update = array('send_push_to_user');

    function send_push_to_driver(){
        if($this->driver)
          Utils::push('USER', $this->user->user_id, 'Reservation ');
    }

    function send_push_to_user(){
        if($this->reservation_status == 'riding')
            Utils::push('DRIVER', $this->driver->driver_identity, 'Reservation accepté');
    }

    function set_reference(){
      if(empty($this->reference))
        $this->reference = "Réservation-". date("d/m", strtotime($this->reservation_date));
    }

    public function set_estimated_data() {

        try {
            $origin_lat = $this->start_position->latitude;
            $origin_lng = $this->start_position->longitude;
            $origin_adr = preg_replace('/\s/', '+', $this->start_position->address);
            
            $dest_lat = $this->end_position->latitude;
            $dest_lng = $this->end_position->longitude;
            $dest_adr = preg_replace('/\s/', '+', $this->end_position->address);

            $oDistanceMatrix = Ride::distance_matrix($origin_lat, $origin_lng, $origin_adr, $dest_lat, $dest_lng, $dest_adr);

            $this->estimated_time = ceil($oDistanceMatrix->duration / 60);
            $this->estimated_length = $oDistanceMatrix->distance;
            $estimated_time = ceil($oDistanceMatrix->duration / 60);
            $this->save();

        } catch (Exception $e) {
            return true;
        }
    }

    function fetchApiObject(){
      return array(
          //'reference' => $this->reference,
          'reservation_id' => $this->reservation_id,
          'reservation_date' => $this->reservation_date,
          'reservation_time' => $this->reservation_status,
           'reservation_status' => $this->reservation_time,
          'user_firstname'=>$this->ride->user->user_firstname,
          'user_lastname'=>$this->ride->user->user_lastname,
          'driver' => array(
              'driver_firstname' => $this->ride->car->driver->driver_firstname,
              'driver_lastname' => $this->ride->car->driver->driver_lastname
            ),
          'ride' => array(
              'start_position' => array(
                  'longitude' => $this->ride->start_position->longitude,
                  'latitude' => $this->ride->start_position->latitude,
                  'address' => $this->ride->start_position->address
                ),
              'end_position' => array(
                  'longitude' => $this->ride->end_position->longitude,
                  'latitude' => $this->ride->end_position->latitude,
                  'address' => $this->ride->end_position->address
                ),
              'estimated_time' => $this->ride->ride_estimated_time,
              'estimated_length' => $this->ride->ride_estimated_length 
            )
        );
    }
        
  } 
