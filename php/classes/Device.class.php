<?php
   
  class Device extends ActiveRecord\Model { 

    static $table_prefix = DATABASE_PREFIX ;
    static $table_name  = "devices" ;
    static $primary_key = 'device_id' ;

    static $validates_presence_of = array(
      array('os_name', 'message' => "Missing user:user_device:os_name"),
      array('os_version', 'message' => "Missing user:user_device:os_version"),
      array('notification_id', 'message' => "Missing user:user_device:notification_id")
    );

    static $validates_inclusion_of = array(
      array('os_name', 'in' => array('iOS', 'Android', 'Androïd'), 'message' => 'Wrong value for user:user_device:os_name'),
    );

    static $has_one = array(
    		array("user", "class_name" => "User")
    	);

  }
