<?php

class Utils {

    /**
     * 	Sending push to mobile device
     * $from: source USER or DRIVER
     * token: USER or DRIVER ID
     * $msg: displaying text
     */
    public static function push($from = 'USER', $token, $msg) {
        $tParams = array(
            'apikey' => constant("PUSH_" . $from . "_KEY"),
            'method' => PUSH_METHOD,
            'token' => $token,
            'text' => $msg
        );
        return Utils::request(PUSH_URL, $tParams, 'GET');
    }

    /**
     * $_zUrl request url
     * $_ztParams array of the parameters 
     * $_zMethod RESTful method
     */
    public static function request($_zUrl, $_ztParams, $_zMethod) {

        $ch = curl_init();

        // Entête à 0 
        curl_setopt($ch, CURLOPT_HEADER, 0);

        // Autorisation des redirections en retour 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        //give url action and the method request

        switch ($_zMethod) {
            case 'POST':
                curl_setopt($ch, CURLOPT_URL, $_zUrl);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($_ztParams));
                break;
            case 'PUT':
                curl_setopt($ch, CURLOPT_URL, $_zUrl);
                curl_setopt($ch, CURLOPT_PUT, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($_ztParams));
                break;
            case 'DELETE':
                //$method = CURLOPT_PUT ;
                break;
            default:
                curl_setopt($ch, CURLOPT_URL, $_zUrl . '?' . http_build_query($_ztParams));
                break;
        }
        //Envoi du formulaire 
        $data = curl_exec($ch);

        //Fermeture de la ressource curl 
        curl_close($ch);

        //Affichage du résultat 
        return $data;
    }

    public static function array_utf8_encode_recursive($dat) {
        if (is_string($dat)) {
            return utf8_encode($dat);
        }
        if (is_object($dat)) {
            $ovs = get_object_vars($dat);
            $new = $dat;
            foreach ($ovs as $k => $v) {
                $new->$k = Utils::array_utf8_encode_recursive($new->$k);
            }
            return $new;
        }

        if (!is_array($dat))
            return $dat;
        $ret = array();
        foreach ($dat as $i => $d)
            $ret[$i] = Utils::array_utf8_encode_recursive($d);
        return $ret;
    }

    public static function array_utf8_decode_recursive($dat) {
        if (is_string($dat)) {
            return utf8_decode($dat);
        }
        if (is_object($dat)) {
            $ovs = get_object_vars($dat);
            $new = $dat;
            foreach ($ovs as $k => $v) {
                $new->$k = Utils::array_utf8_decode_recursive($new->$k);
            }
            return $new;
        }

        if (!is_array($dat))
            return $dat;
        $ret = array();
        foreach ($dat as $i => $d)
            $ret[$i] = Utils::array_utf8_decode_recursive($d);
        return $ret;
    }

    /**
    *check if a given time is between two times
    * @param $t1
    * @param $t2
    * @param $tn
    */
    function check_time($t1, $t2, $tn) {
        $t1 = +str_replace(":", "", $t1);
        $t2 = +str_replace(":", "", $t2);
        $tn = +str_replace(":", "", $tn);

        if ($t2 >= $t1) {
            return $t1 <= $tn && $tn < $t2;
        } else {
            return ! ($t2 <= $tn && $tn < $t1);
        }
    }

}