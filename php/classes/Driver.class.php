<?php
  
  class Driver extends ActiveRecord\Model { 

    static $table_prefix = DATABASE_PREFIX ;
    static $table_name  = "drivers" ;
    static $primary_key = "driver_identity"; 

    static $has_one = array(
    	array('car', 'foreign_key' => 'driver_identity')    	
    );

    static $has_many = array(
        array('driver_planings', 'foreign_key' => 'driver_identity', 'class_name' => 'DriverPlaning'),
        array('reservations', 'foreign_key' => 'driver_identity', 'class_name' => 'Reservation')
    );

    static $validates_presence_of = array(
        array('driver_identity', 'message' => "Missing driver:driver_identity"),
        array('code_pin', 'message' => "Missing driver:code_pin")
    );

    public function deconnect (){
        $this->update_attribute('session_id', NULL);
        $this->car->update_attribute('connection_status', 'not-available');
    }

    public function persist_authentication(){

        if(!is_object($this->car))
            return true;
        
        $this->car->update_attributes(
            array(
                'connection_status' => 'available',
                'last_request' => date('c')
            )
        );
    }


    /**
    * Check if driver is connected and car available
    */

    public function connected(){
        return !empty($this->session_id ) && $this->car->connection_status == 'available';
    }

    /**
    * check if driver available on given date and time
    * @param $_date reservation date
    * @param $_time reservation time
    */

    public function available_on($_date, $_time){
        $toPlanings = $this->driver_planings;
        foreach ($toPlanings as $key => $oPlaning) {
            if (Utils::check_time($oPlaning->driver_hour_min, $oPlaning->driver_hour_max, $_time)) return false ;
        }

        return true;
    }

    /**
    * get all reservations on current day
    */

    function pending_reservations(){
        return Reservation::all(array('conditions' => array('driver_identity = ? AND reservation_status = ? AND reservation_date = ?', $this->driver_identity, 'pending', date('Y-m-d'))));
    }

    /**
    * Send push to driver if available reservations exists on day
    */

    function push_reservation(){
        if ( ($iSize = count($this->pending_reservations())) > 0 ) {
            Utils::push('DRIVER', $this->driver_identity, 'Vous avez ' . $iSize . ' réservation(s) aujourd\'hui');
        }
    }
    
  }
