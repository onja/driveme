<?php

  require_once APPLICATION_PATH . '/php/libs/phpmailer/class.phpmailer.php';
  
  class Email {
    
    private $oMail;

    function __construct() {

      global $conf_email;

      $this->oMail = new PHPMailer();

      switch ($conf_email['type']) {
        case 'SENDMAIL':
          $this->oMail->IsSendmail();
          break;
        
        case 'SMTP':
          $this->oMail->IsSMTP(); // telling the class to use SMTP
          $this->oMail->Host       = $conf_email['smtp']['host'];           // SMTP server
          $this->oMail->SMTPDebug  = 2;                                    // enables SMTP debug information (for testing)
                                                                     // 1 = errors and messages
                                                                     // 2 = messages only
          $this->oMail->SMTPAuth   = $conf_email['smtp']['authenticable']; // enable SMTP authentication
          $this->oMail->SMTPSecure = $conf_email['smtp']['secure'];        // sets the prefix to the servier
          $this->oMail->Port       = $conf_email['smtp']['port'];          // set the SMTP port for the GMAIL server
          $this->oMail->Username   = $conf_email['smtp']['username'];       // GMAIL username
          $this->oMail->Password   = $conf_email['smtp']['password'];      // GMAIL password
          break;
      }

    }

    public function send($zSubject, $zFrom, $fromName, $zTo, $toName, $bcc='', $bodyTpl='', $zMessage='', $tAttachment='', $tParams='') {
      $this->oMail->SetFrom($zFrom, $fromName);
      $this->oMail->Subject    = $zSubject;
      $this->oMail->MsgHTML($bodyTpl);
      $this->oMail->AddAddress($zTo, $toName);
      if(!$this->oMail->Send()) {
        return "Mailer Error: " . $this->oMail->ErrorInfo;
      } else {
        return "Message sent!";
      }
      
    }
  }