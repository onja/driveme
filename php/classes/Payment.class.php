<?php
	
	class Payment {	

		public function __construct(){				
		}

		/**
		* Demand authorisation
		* @param @userId id of the user owner account to debit
		* @param $_tParams array of the payment params: porteur, dateval, cvv, reference, montant
	 	*/		
		public function authorize($_tParams){
			//Initialisation du curl 
			$ch = curl_init(); 

			// Définition de l'action du formulaire 
			curl_setopt( $ch, CURLOPT_URL, PBX_DIRECT_URL ); 

			// Entête à 0 
			curl_setopt( $ch, CURLOPT_HEADER, 0 ); 

			// Autorisation des redirections en retour 
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 ); 

			// Le formulaire est envoyé avec la méthode post 
			curl_setopt( $ch, CURLOPT_POST, 1 ); 

			$type= '00001'; //PBX Type of the demand
			$dateq = date("Ymdhis");

			$zParam  = "VERSION=" . PBX_DIRECT_VERSION ;
			$zParam .= "&TYPE=0001";
			$zParam .= "&SITE=" . PBX_DIRECT_SITE ;
			$zParam .= "&RANG=" . PBX_DIRECT_RANG ;
			$zParam .= "&CLE=" . PBX_DIRECT_CLE ;
			$zParam .= "&DEVISE=" . PBX_DIRECT_DEVISE ;
									
			$zParam .= "&DATEQ=" . $dateq ;
			$zParam .= "&NUMQUESTION=" . $_tParams['num_question'] ;
			$zParam .= "&PORTEUR=" . $_tParams['porteur'];
			$zParam .= "&DATEVAL=" . $_tParams['dateval'];
			$zParam .= "&CVV=" . $_tParams['cvv'];
			$zParam .= "&REFERENCE=" . $_tParams['reference'];
			$zParam .= "&MONTANT=" . $_tParams['amount'];			

			/*$zParam .= "&NUMTRANS=0003812942";
			$zParam .= "&NUMAPPEL=0006455457";*/

			//print_r($zParam);die;

			curl_setopt( $ch, CURLOPT_POSTFIELDS, $zParam ); 

			//Envoi du formulaire 
			$zData = curl_exec( $ch ); 

			//Fermeture de la ressource curl 
			curl_close( $ch ); 

			//Affichage du résultat 

			parse_str($zData, $data);

			return $data;

		}

		/**
		* Debit of the transaction demand
		* @param $numTrans NUMTRANS param returned by paybox
		* @param $numAppel NUMAPPEL param returned by paybox
		*/

		public function send($_tParams){

			//print_r($_tParams);die;

			//Initialisation du curl 
			$ch = curl_init(); 

			// Définition de l'action du formulaire 
			curl_setopt( $ch, CURLOPT_URL, PBX_DIRECT_URL ); 

			// Entête à 0 
			curl_setopt( $ch, CURLOPT_HEADER, 0 ); 

			// Autorisation des redirections en retour 
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 ); 

			// Le formulaire est envoyé avec la méthode post 
			curl_setopt( $ch, CURLOPT_POST, 1 ); 

			$type= '00002'; //PBX Type of the demand
			$dateq = date("Ymdhis");

			$zParam  = "VERSION=" . PBX_DIRECT_VERSION ;
			$zParam .= "&TYPE=0002";
			$zParam .= "&SITE=" . PBX_DIRECT_SITE ;
			$zParam .= "&RANG=" . PBX_DIRECT_RANG ;
			$zParam .= "&CLE=" . PBX_DIRECT_CLE ;
			$zParam .= "&DEVISE=" . PBX_DIRECT_DEVISE ;
									
			$zParam .= "&DATEQ=" . $dateq ;
			$zParam .= "&NUMQUESTION=" . $_tParams['num_question'] ;
			$zParam .= "&PORTEUR=" . $_tParams['porteur'];
			$zParam .= "&DATEVAL=" . $_tParams['dateval'];
			$zParam .= "&CVV=" . $_tParams['cvv'];
			$zParam .= "&REFERENCE=" . $_tParams['reference'];
			$zParam .= "&MONTANT=" . floatval($_tParams['amount']);			
			$zParam .= "&NUMTRANS=" . $_tParams['num_trans'];
			$zParam .= "&NUMAPPEL=" . $_tParams['num_appel'];

			curl_setopt( $ch, CURLOPT_POSTFIELDS, $zParam ); 

			//Envoi du formulaire 
			$zData = curl_exec( $ch ); 

			//Fermeture de la ressource curl 
			curl_close( $ch ); 

			//Affichage du résultat 
			parse_str($zData, $data);

			return $data;

		}

}