<?php

class Ride extends ActiveRecord\Model {

    static $table_prefix = DATABASE_PREFIX;
    static $table_name = "rides";
    static $primary_key = 'ride_id';
    static $belongs_to = array(
        array('start_position', 'foreign_key' => 'start_position_id', 'class_name' => 'Position', 'dependent' => 'delete'),
        array('end_position', 'foreign_key' => 'end_position_id', 'class_name' => 'Position', 'dependent' => 'delete'),
        array('current_position', 'foreign_key' => 'current_position_id', 'class_name' => 'Position', 'dependent' => 'delete'),
        array('car', 'foreign_key' => 'car_id'),
        array('user', 'foreign_key' => 'user_id')
    );
    static $has_one = array(
        array('reservation', 'foreign_key' => 'ride_id', 'class_name' => 'Ride')
    );

    static $has_many = array(
        array('ride_histories', 'foreign_key' => 'ride_id', 'class_name' => 'RideHistory', 'order' => 'updated_at ASC')
    );

    static $validates_presence_of = array(
        array('car_id', 'message' => "Missing ride:car_id")//,
            //array('car_level', 'message' => "Missing ride:car_level"),
            //array('subscription_code', 'message' => "Missing ride:subscription_code")
    );

    /*    static $validates_inclusion_of = array(
      array('car_level', 'in' => array('basic', 'premium'), 'message' => 'Wrong value for car:car_level')
      ); */
    static $after_save = array('set_positionable_start_type', 'set_positionable_end_type', 'create_history');

    static $before_create = array('set_status');

    static $before_update = array('rollback_status');

    static $before_destroy = array('set_car_available');

    public function set_positionable_start_type() {
        if (is_object($this->start_position)) {
            $this->start_position->positionable_type = "Ride";
            $this->start_position->positionable_id = $this->ride_id;
            $this->start_position->save();
        }
    }

    public function set_positionable_end_type() {
        if (is_object($this->end_position)) {
            $this->end_position->positionable_type = "Ride";
            $this->end_position->positionable_id = $this->ride_id;
            $this->end_position->save();
        }
    }

    public function set_status() {
        if(empty($this->ride_status))
        $this->ride_status = "tmp";
    }

    public function set_estimated_data() {

        try {
            $origin_lat = $this->start_position->latitude;
            $origin_lng = $this->start_position->longitude;
            $origin_adr = preg_replace('/\s/', '+', $this->start_position->address);
            
            $dest_lat = $this->end_position->latitude;
            $dest_lng = $this->end_position->longitude;
            $dest_adr = preg_replace('/\s/', '+', $this->end_position->address);

            $oDistanceMatrix = Ride::distance_matrix($origin_lat, $origin_lng, $origin_adr, $dest_lat, $dest_lng, $dest_adr);

            $this->ride_estimated_time = ceil($oDistanceMatrix->duration / 60);
            $this->ride_estimated_length = $oDistanceMatrix->distance;
            $estimated_time = ceil($oDistanceMatrix->duration / 60);
            $ride_time = $estimated_time * (1 + (20 / 100));

            $car_price = floatval(preg_replace('/,/', '.', array_shift(explode('€', $this->car->car_price)))) ;

            $this->ride_pack_price = $ride_time * $car_price ;
            $this->save();

        } catch (Exception $e) {
            return true;
        }
    }

    public function set_car_status() {

        $tStatus = array(
            'tmp' => 'reserved',
            'accepted' => 'pick-up',
            'ride-pack' => 'ride',
            'refused'  => 'available',
            'finished' => 'available',
            'canceled' => 'available'            
        );
        
        if (in_array($this->ride_status, array_keys($tStatus))) {
            $this->car->update_attribute('car_status', $tStatus[$this->ride_status]);
        }

        if($this->ride_status == 'waiting-car'){
            $token = 'test';
            $msg = 'test';
            Utils::push('USER', $token, $msg);
        }

        if($this->ride_status == 'accepted'){
            $token = 'test';
            $msg = 'test';
            Utils::push('DRIVER', $token, $msg);
        }        

        if($this->ride_status == 'ride-pack')
            $this->update_attribute('start_date', date('Y-m-d h:i:s'));

        if($this->ride_status == 'finished'){
            $this->update_attribute('ride_real_time', date('Y-m-d h:i:s'));
            //debite user driveme credits
            /*$this->user->credit->debite($this->ride_estimated_time);
            AccountMovement::create(array(
                    'credit_id' => $this->user->credit->credit_id,
                    'account_mvt_debit' => $this->ride_estimated_time,
                    'account_mvt_op_id' => $this->ride_id,
                    'account_mvt_op_type' => 'Ride',
                    'updated_at' => date('Y-m-d h:i:s')
                ));*/
        }

    }

    public function create_history() {
        RideHistory::create(
                array(
                    'ride_id' => $this->ride_id,
                    'ride_status' => $this->ride_status,
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                )
        );
    }

    /**
    * reallocate refused ride by another car
    */

    public function reallocate(){
        $oCar = $this->car;
        $oDriver = $this->car->driver;
        $oStartPosition = $this->start_position->attributes();

        $tParam = array(
            'car' => array(
                'start_position' => $oStartPosition
            )
        );

        $tzFilter = array();
        $tCarFilter = array("car_id", "NOT IN", "('". $oCar->car_id ."')");
        $tzFilter[] = $tCarFilter;
        $oNearlyCar = Car::get_available_nearly($tParam, $tzFilter, 'object');
        
        $this->update_attributes(
                array(
                    'car_id' => $oNearlyCar->car_id,
                    'ride_status' => 'waiting-car'
                )
        );

        $oNearlyCar->update_attribute('car_status', 'reserved');

    }

    //calcul distance matrix between 2 points
    public static function distance_matrix($origin_lat, $origin_lng, $origin_adr, $dest_lat, $dest_lng, $dest_adr){

      $url = "http://dev.virtualearth.net/REST/V1/Routes?wp.0=$origin_lat,$origin_lng&wp.1=$dest_lat,$dest_lng&optmz=timeWithTraffic&key=" . APP_TRAFFIC_KEY ;

      $body = file_get_contents($url);
      $data = json_decode($body);      

      $oDistanceMatrix = new StdCLass();

      if( isset($data->resourceSets[0]->resources[0]) ){
        $oDistanceMatrix->duration = $data->resourceSets[0]->resources[0]->travelDurationTraffic;
        $oDistanceMatrix->distance = $data->resourceSets[0]->resources[0]->travelDistance;  
      }      
        
      return $oDistanceMatrix;

    }

    //calcul real ride duration
    public function get_real_duration() {
        $toRide = Ride::find_by_sql("SELECT TIMESTAMPDIFF(SECOND, c.start_date, c.ride_real_time) as iduration FROM rc_rides as c WHERE c.ride_id = " . $this->ride_id);
        $oRide = array_shift($toRide);

        return ceil($oRide->iduration/60);
    }

    public function rollback_status(){
        
        $old_object = Ride::find($this->ride_id) ;
        
        $array1 = array('ride', 'pick-up', 'waiting-passenger');
        $array2 = array('available', 'not-available');

        if (in_array($old_object->ride_status, $array1) && in_array($this->ride_status, $array2)) {
            $this->ride_status = $old_object->ride_status ;
        }

    }

    //set car available if ride canceled
    public function set_car_available(){
        $this->car->update_attribute('car_status', 'available');
    }

}
