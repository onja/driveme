<?php
  
  class Position extends ActiveRecord\Model { 

    static $table_prefix = DATABASE_PREFIX ;
    static $table_name  = "positions" ;
    static $primary_key = 'position_id' ;   

    static $has_one = array(
    		array('car'),
        array("ride", 'class_name'=>'Ride')/*,
        array('reservation','class_name' => 'Reservation')*/
    );

    /*static $validates_presence_of = array(
      array('longitude', 'message' => 'Missing car:car_position:longitude'),
      array('latitude', 'message' => 'Missing car:car_position:latitude')
    );*/
    
    static $before_create = array('set_created_time');
    public function set_created_time(){
      $this->created_time = "NOW()";
    }   
   
  }
