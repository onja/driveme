<?php
  
  class Photos extends ActiveRecord\Model { 

    static $table_prefix = DATABASE_PREFIX ;
    static $table_name  = "photos" ;
    static $primary_key = 'photos_id' ;   

    static $belongs_to = array(
    	array('driver', 'foreign_key' => 'photoable_id', 'class_name' => 'Driver')
    );
    
  }