<?php

class Car extends ActiveRecord\Model {

    static $table_prefix = DATABASE_PREFIX;
    static $table_name = "cars";
    static $primary_key = 'car_id';
    
    static $belongs_to = array(
        array('driver', 'foreign_key' => 'driver_identity', 'class_name' => 'Driver'),
        array('position', 'foreign_key' => 'position_id')
    );
    
    static $has_many = array(array('rides', 'class_name' => 'Ride'));

    /**
    * Get the current ride that car has been assigned if exist
    */
    static $has_one = array(array('current_active_ride', 'conditions' => array('ride_status IN (?)', array('tmp', 'pending', 'accepted', 'ride-pack', 'ride-minute')), 'class_name' => 'Ride'));

    static $validates_presence_of = array(
        array('car_id', 'message' => "Missing car:car_id")
    );

    static $validates_inclusion_of = array(
        array('car_level', 'in' => array('basic', 'premium'), 'message' => 'Wrong value for car:car_level')
    );

    static $after_save = array('set_positionable_type');

    public function set_positionable_type() {
        if (is_object($this->position)) {
            $this->position->positionable_type = "Car";
            $this->position->positionable_id = $this->car_id;
            $this->position->save();
        }
    }

    /**
     * locate the available nearly car 
     * @param $_tParam array of car parameters: position(latitude, longitude)
     * @param $_tzFilter array of Sql filter
     */
    public static function get_available_nearly($_tParam, $_tzFilter = array(), $return = 'array') {

        $zConditions = "connection_status = 'available' AND car_status = 'available'";
        if (count($_tzFilter) > 0) {
            foreach ($_tzFilter as $key => $tFilter) {
                $zConditions .= " AND " . implode(' ', $tFilter);
            }
        }

        $toAvailableCar = Car::find('all', array(
                    'conditions' => $zConditions
                        )
        );

        $duration_min = 0;

        $carsArray = array();

        foreach ($toAvailableCar as $key => $oAvailableCar) {
            $origin_lat = $_tParam["car"]["start_position"]["latitude"];
            $origin_lng = $_tParam["car"]["start_position"]["longitude"];
            $origin_adr = empty($_tParam["car"]["start_position"]["address"]) ? "" : "|" . $_tParam["car"]["start_position"]["address"];
            $origin_adr = preg_replace('/\s/', '+', $origin_adr);

            $dest_lat = $oAvailableCar->position->latitude;
            $dest_lng = $oAvailableCar->position->longitude;
            $dest_adr = $oAvailableCar->position->address;

            $oDistanceMatrix = Car::distance_matrix($origin_lat, $origin_lng, $origin_adr, $dest_lat, $dest_lng, $dest_adr);

            $carsArray[$oDistanceMatrix->duration] = $oAvailableCar;
        }
        if (count($carsArray) > 0) {
            ksort($carsArray);
            $keys = array_keys($carsArray);
            $oNearlyCar = $carsArray[$keys[0]];
        }
        $eta = ceil($duration_min);
        $array_car = array();
        if (isset($oNearlyCar)) {
            $oNearlyCar->position->update_attribute('eta', $eta);
            $array_car = array(
                "car_id" => $oNearlyCar->car_id,
                "car_status" => $oNearlyCar->car_status,
                "car_position" => array(
                    "longitude" => $oNearlyCar->position->longitude,
                    "latitude" => $oNearlyCar->position->latitude,
                    "ETA" => $eta
                ),
                "car_level" => $oNearlyCar->car_level,
                "car_price" => $oNearlyCar->car_price,
                "driver" => array(
                    "driver_firstname" => $oNearlyCar->driver->driver_firstname,
                    "driver_lastname" => $oNearlyCar->driver->driver_lastname
                )
            );
        }
        
        if($return == 'array') {
            return $array_car;
        } else {
            return $oNearlyCar;
        }
        
    }

    //calcul distance matrix between 2 points
    public static function distance_matrix($origin_lat, $origin_lng, $origin_adr, $dest_lat, $dest_lng, $dest_adr) {

        $url = "http://dev.virtualearth.net/REST/V1/Routes?wp.0=$origin_lat,$origin_lng&wp.1=$dest_lat,$dest_lng&optmz=timeWithTraffic&key=" . APP_TRAFFIC_KEY;

        $body = file_get_contents($url);
        $data = json_decode($body);

        $oDistanceMatrix = new StdCLass();

        if (isset($data->resourceSets[0]->resources[0])) {
            $oDistanceMatrix->duration = $data->resourceSets[0]->resources[0]->travelDurationTraffic;
            $oDistanceMatrix->distance = $data->resourceSets[0]->resources[0]->travelDistance;
        }

        return $oDistanceMatrix;
    }

    /**
    * Fetch API attributes return 
    */

    function fetchApiObject(){
        return array(
                "car_id" => $this->car_id,
                "car_status" => $this->car_status,
                "car_position" => array(
                    "longitude" => $this->position->longitude,
                    "latitude" => $this->position->latitude,
                    "ETA" => $this->position->eta
                ),
                "car_level" => $this->car_level,
                "car_price" => $this->car_price,
                "driver" => array(
                    "driver_firstname" => $this->driver->driver_firstname,
                    "driver_lastname" => $this->driver->driver_lastname
                )
            );
    }   

}