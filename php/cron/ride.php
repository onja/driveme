<?php

require 'cron.php';

$toRide = Ride::find('all',
        array(
            'joins' => "LEFT JOIN rc_ride_histories as rh ON rh.ride_id = rc_rides.ride_id",
            'conditions' => array( "rc_rides.ride_status = ?", 'finished' )
        )
    );

$ztRideId = array();
$ztRideId = array_map(function($oRide) {
    return $oRide->ride_id;
}, $toRide);


//canceled each ride when status tmp duration more than 15 minutes
$oRide = Ride::connection();
$oRide->query("UPDATE rc_rides INNER JOIN rc_ride_histories as rh ON rh.ride_id = rc_rides.ride_id INNER JOIN rc_cars as c ON c.car_id = rc_rides.car_id SET rh.ride_status = 'canceled', rc_rides.ride_status = 'canceled', car_status = 'available' WHERE rc_rides.ride_status = 'tmp' AND rh.updated_at < (NOW() - INTERVAL " . RIDE_TMP_TIMEOUT . " minute)");

echo "OK";