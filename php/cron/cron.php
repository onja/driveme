<?php

    define('APPLICATION_CRON_PATH', dirname(__FILE__));
    if(!defined('APPLICATION_PATH'))
        define('APPLICATION_PATH', realpath(APPLICATION_CRON_PATH . '/../../'));
    
    //require and load activerecord lib
    require_once APPLICATION_PATH. '/php/libs/php-activerecord/ActiveRecord.php';
    require APPLICATION_PATH . '/config/database.conf.php';
    require APPLICATION_PATH . '/config/driveme.conf.php';
    ActiveRecord\Config::initialize(function($cfg)
    {
        $cfg->set_model_directory(APPLICATION_PATH . '/php/classes');
        $cfg->set_connections(array(
        'development' => 'mysql://' . DATABASE_USER . ':' . DATABASE_PASSWORD . '@' . DATABASE_HOST . '/' . DATABASE_NAME . '?charset=utf8'));
    });

    spl_autoload_register('regulator_autoload');

    function regulator_autoload($class_name)
    {
        if(file_exists(APPLICATION_PATH . '/php/classes/'.$class_name . '.class.php')) {
        require_once(APPLICATION_PATH . '/php/classes/'.$class_name . '.class.php');
      } else {
        throw new Exception("Unable to load $class_name.");
      }
    }
    

?>