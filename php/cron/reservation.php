<?php

/**
* Sending push to driver every 15 minutes before the reservation date time
*/

require 'cron.php';
error_reporting(E_ALL);
ini_set('display_errors', 'On');
$toAvailableRes = Reservation::find('all', array(
		'conditions' => array("reservation_status = ? AND DATE_FORMAT(CONCAT(reservation_date, ' ', reservation_time), '%Y-%m-%d %h:%i:%s') < NOW()", 'pending') 
	));

if(empty($toAvailableRes))
	return true ;

foreach ($toAvailableRes as $key => $oReservation) {	

	//if driver assined with reservation
	if( is_object($oReservation->driver) && $oReservation->driver->connected() ){
		Utils::push('USER', $oReservation->driver->driver_identity, 'Reservation message');
	}else{

		//if any driver assigned, broadcast push to driver available on reservation date
		$toDriver = Driver::find('all', array(
				'joins' => 'INNER JOIN rc_driver_planings as dp ON dp.driver_identity = rc_drivers.driver_identity',
				'conditions' => array( 'DATE(dp.driver_day) = ? AND TIME(dp.driver_hour_min) < ? AND TIME(dp.driver_hour_max) > ?', $oReservation->reservation_date, $oReservation->reservation_time, $oReservation->reservation_time )

			));

		foreach ($toDriver as $key => $oDriver) {
			Utils::push('USER', $oDriver->driver_identity, 'Reservation message');
		}

	}

}