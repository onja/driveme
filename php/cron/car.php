<?php

require 'cron.php';
$oCar = Car::connection();
//set car connection_status not-available if last request duration more than 2 minutes        
$oCar->query("UPDATE rc_cars as c SET c.connection_status = 'not-available' WHERE c.last_request < (NOW() - INTERVAL " . CAR_CONNECTION_PERSISTANCE_TIMEOUT . " minute) OR c.last_request IS NULL");

//delete driver car session if last request duration more than 30 minutes
$oCar->query("UPDATE rc_cars as c INNER JOIN rc_drivers as d ON d.driver_identity = c.driver_identity SET c.connection_status = 'not-available', d.session_id = NULL WHERE c.last_request < (NOW() - INTERVAL " . CAR_CONNECTION_TIME_OUT . " minute)");

echo "OK";