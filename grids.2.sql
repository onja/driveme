SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP SCHEMA IF EXISTS `mydb` ;
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`grids`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`grids` ;

CREATE  TABLE IF NOT EXISTS `mydb`.`grids` (
  `grid_id` INT NOT NULL ,
  `grid_name` VARCHAR(45) NULL ,
  PRIMARY KEY (`grid_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`grid_fields`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`grid_fields` ;

CREATE  TABLE IF NOT EXISTS `mydb`.`grid_fields` (
  `grid_field_id` INT NOT NULL ,
  `driver_type_name` VARCHAR(45) NULL ,
  `grids_grid_id` INT NOT NULL ,
  PRIMARY KEY (`grid_field_id`) ,
  INDEX `fk_grid_fields_grids` (`grids_grid_id` ASC) ,
  CONSTRAINT `fk_grid_fields_grids`
    FOREIGN KEY (`grids_grid_id` )
    REFERENCES `mydb`.`grids` (`grid_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`driver_types`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`driver_types` ;

CREATE  TABLE IF NOT EXISTS `mydb`.`driver_types` (
  `driver_type_id` INT NOT NULL ,
  `driver_type_name` VARCHAR(45) NULL ,
  PRIMARY KEY (`driver_type_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`grid_options`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`grid_options` ;

CREATE  TABLE IF NOT EXISTS `mydb`.`grid_options` (
  `grid_option_id` INT NOT NULL ,
  `grid_option_name` VARCHAR(45) NULL ,
  PRIMARY KEY (`grid_option_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`grid_values`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`grid_values` ;

CREATE  TABLE IF NOT EXISTS `mydb`.`grid_values` (
  `grid_value_id` INT NOT NULL ,
  `grid_value_value` VARCHAR(45) NULL ,
  `grid_fields_grid_field_id` INT NOT NULL ,
  `driver_types_driver_type_id` INT NOT NULL ,
  `grid_options_grid_option_id` INT NOT NULL ,
  PRIMARY KEY (`grid_value_id`) ,
  INDEX `fk_grid_values_grid_fields1` (`grid_fields_grid_field_id` ASC) ,
  INDEX `fk_grid_values_driver_types1` (`driver_types_driver_type_id` ASC) ,
  INDEX `fk_grid_values_grid_options1` (`grid_options_grid_option_id` ASC) ,
  CONSTRAINT `fk_grid_values_grid_fields1`
    FOREIGN KEY (`grid_fields_grid_field_id` )
    REFERENCES `mydb`.`grid_fields` (`grid_field_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_grid_values_driver_types1`
    FOREIGN KEY (`driver_types_driver_type_id` )
    REFERENCES `mydb`.`driver_types` (`driver_type_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_grid_values_grid_options1`
    FOREIGN KEY (`grid_options_grid_option_id` )
    REFERENCES `mydb`.`grid_options` (`grid_option_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
